# get_cli intl implement

<https://zhuanlan.zhihu.com/p/440074892>

1. write like `enUS.json`
2. run `get generate locales assets/locales`

## Write rule

enUS.json ruler is [path_domain_field]. path is min path, domain is optional, field is abbreviation of the original text.

> json keys or path don't contain spaces.

## Usage

Use it like this:

```dart
Get.rawSnackbar(
  title: LocaleKeys.snackbar_utilSuccess.tr,
  icon: const Icon(Icons.thumb_up, color: Colors.white),
  message: message,
  backgroundColor: Colors.green.shade600,
);
```

```dart
LocaleKeys.xxx.tr
```
