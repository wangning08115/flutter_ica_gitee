// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

// ignore_for_file: lines_longer_than_80_chars, constant_identifier_names
// ignore: avoid_classes_with_only_static_members
class AppTranslation {
  static Map<String, Map<String, String>> translations = {
    'zh_HK': Locales.zh_HK,
    'zh_CN': Locales.zh_CN,
    'en_US': Locales.en_US,
  };
}

class LocaleKeys {
  LocaleKeys._();
  static const message_default = 'message_default';
  static const message_msgFormRequired = 'message_msgFormRequired';
  static const message_msgFormInvalidUsername =
      'message_msgFormInvalidUsername';
  static const message_msgFormInvalidEmail = 'message_msgFormInvalidEmail';
  static const message_msgFormUsedEmail = 'message_msgFormUsedEmail';
  static const message_msgFormNotSameEmail = 'message_msgFormNotSameEmail';
  static const message_msgFormLongPassword = 'message_msgFormLongPassword';
  static const message_msgFormNotSamePassword =
      'message_msgFormNotSamePassword';
  static const message_msgFormShortPassword = 'message_msgFormShortPassword';
  static const message_msgFormSpecifyPassword =
      'message_msgFormSpecifyPassword';
  static const message_msgFormNotSupportedFormat =
      'message_msgFormNotSupportedFormat';
  static const formInfo_formClassName = 'formInfo_formClassName';
  static const formInfo_formClassNamePlaceholder =
      'formInfo_formClassNamePlaceholder';
  static const formInfo_formFirstName = 'formInfo_formFirstName';
  static const formInfo_formFirstNamePlaceholder =
      'formInfo_formFirstNamePlaceholder';
  static const formInfo_formLastName = 'formInfo_formLastName';
  static const formInfo_formLastNamePlaceholder =
      'formInfo_formLastNamePlaceholder';
  static const formInfo_formTeacherEmail = 'formInfo_formTeacherEmail';
  static const formInfo_formTeacherEmailPlaceholder =
      'formInfo_formTeacherEmailPlaceholder';
  static const formInfo_formEmail = 'formInfo_formEmail';
  static const formInfo_formEmailPlaceholder = 'formInfo_formEmailPlaceholder';
  static const formInfo_formConfirmEmail = 'formInfo_formConfirmEmail';
  static const formInfo_formConfirmEmailPlaceholder =
      'formInfo_formConfirmEmailPlaceholder';
  static const formInfo_formPassword = 'formInfo_formPassword';
  static const formInfo_formCurpassword = 'formInfo_formCurpassword';
  static const formInfo_formNewpassword = 'formInfo_formNewpassword';
  static const formInfo_formPasswordPlaceholder =
      'formInfo_formPasswordPlaceholder';
  static const formInfo_formCurpasswordPlaceholder =
      'formInfo_formCurpasswordPlaceholder';
  static const formInfo_formNewpasswordPlaceholder =
      'formInfo_formNewpasswordPlaceholder';
  static const formInfo_formConfirmPassword = 'formInfo_formConfirmPassword';
  static const formInfo_formConfirmPasswordPlaceholder =
      'formInfo_formConfirmPasswordPlaceholder';
  static const formInfo_formUserName = 'formInfo_formUserName';
  static const formInfo_formUserNamePlaceholder =
      'formInfo_formUserNamePlaceholder';
  static const formInfo_formPhone = 'formInfo_formPhone';
  static const formInfo_formPhonePlaceholder = 'formInfo_formPhonePlaceholder';
  static const formInfo_formCountry = 'formInfo_formCountry';
  static const formInfo_formCountryPlaceholder =
      'formInfo_formCountryPlaceholder';
  static const formInfo_formRegion = 'formInfo_formRegion';
  static const formInfo_formRegionPlaceholder =
      'formInfo_formRegionPlaceholder';
  static const formInfo_formZipCode = 'formInfo_formZipCode';
  static const formInfo_formZipCodePlaceholder =
      'formInfo_formZipCodePlaceholder';
  static const formInfo_formCancel = 'formInfo_formCancel';
  static const formInfo_formSubmit = 'formInfo_formSubmit';
  static const formInfo_formNext = 'formInfo_formNext';
  static const formInfo_formBack = 'formInfo_formBack';
  static const formInfo_formOk = 'formInfo_formOk';
  static const formInfo_formLoading = 'formInfo_formLoading';
  static const formInfo_tableNoContent = 'formInfo_tableNoContent';
  static const modal_modalAgree = 'modal_modalAgree';
  static const modal_modalDisagree = 'modal_modalDisagree';
  static const modal_modalSubmit = 'modal_modalSubmit';
  static const modal_modalCancel = 'modal_modalCancel';
  static const headerInfo_headerHome = 'headerInfo_headerHome';
  static const headerInfo_headerSimulations = 'headerInfo_headerSimulations';
  static const headerInfo_headerSimulationsIndividual =
      'headerInfo_headerSimulationsIndividual';
  static const headerInfo_headerSimulationsStudents =
      'headerInfo_headerSimulationsStudents';
  static const headerInfo_headerSimulationsOverview =
      'headerInfo_headerSimulationsOverview';
  static const headerInfo_headerQuestionBank = 'headerInfo_headerQuestionBank';
  static const headerInfo_headerQuestionBankIndividual =
      'headerInfo_headerQuestionBankIndividual';
  static const headerInfo_headerQuestionBankStudents =
      'headerInfo_headerQuestionBankStudents';
  static const headerInfo_headerQuestionBankOverview =
      'headerInfo_headerQuestionBankOverview';
  static const headerInfo_headerPrice = 'headerInfo_headerPrice';
  static const headerInfo_headerPriceIndividual =
      'headerInfo_headerPriceIndividual';
  static const headerInfo_headerPriceSchool = 'headerInfo_headerPriceSchool';
  static const headerInfo_headerComments = 'headerInfo_headerComments';
  static const headerInfo_headerQuestions = 'headerInfo_headerQuestions';
  static const headerInfo_headerBuy = 'headerInfo_headerBuy';
  static const headerInfo_headerBuyIndividual =
      'headerInfo_headerBuyIndividual';
  static const headerInfo_headerBuySchool = 'headerInfo_headerBuySchool';
  static const headerInfo_headerContact = 'headerInfo_headerContact';
  static const headerInfo_headerLogin = 'headerInfo_headerLogin';
  static const headerInfo_headerLogout = 'headerInfo_headerLogout';
  static const footerInfo_footerTerms = 'footerInfo_footerTerms';
  static const loginPage_loginLoginName = 'loginPage_loginLoginName';
  static const loginPage_loginLoginNamePlaceholder =
      'loginPage_loginLoginNamePlaceholder';
  static const loginPage_loginLogin = 'loginPage_loginLogin';
  static const loginPage_loginSignUp = 'loginPage_loginSignUp';
  static const loginPage_loginForgetPassword = 'loginPage_loginForgetPassword';
  static const resetPassword_resetPasswordSuccess =
      'resetPassword_resetPasswordSuccess';
  static const registerPage_registerTitle = 'registerPage_registerTitle';
  static const registerPage_registerIndividual =
      'registerPage_registerIndividual';
  static const registerPage_registerTeacher = 'registerPage_registerTeacher';
  static const registerPage_registerTeacherIndexQ1 =
      'registerPage_registerTeacherIndexQ1';
  static const registerPage_registerTeacherIndexA1 =
      'registerPage_registerTeacherIndexA1';
  static const registerPage_registerTeacherIndexA2 =
      'registerPage_registerTeacherIndexA2';
  static const registerPage_registerTeacherIndexQ2QuoteNumber =
      'registerPage_registerTeacherIndexQ2QuoteNumber';
  static const registerPage_registerTeacherIndexQ2QuoteNumberPlaceholder =
      'registerPage_registerTeacherIndexQ2QuoteNumberPlaceholder';
  static const registerPage_registerTeacherIndexQ2Email =
      'registerPage_registerTeacherIndexQ2Email';
  static const registerPage_registerTeacherIndexQ2EmailPlaceholder =
      'registerPage_registerTeacherIndexQ2EmailPlaceholder';
  static const registerPage_registerTeacherIndexQ2Submit =
      'registerPage_registerTeacherIndexQ2Submit';
  static const registerPage_registerTeacherIndexQ2Pending =
      'registerPage_registerTeacherIndexQ2Pending';
  static const registerPage_registerTeacherIndexQ2Success =
      'registerPage_registerTeacherIndexQ2Success';
  static const registerPage_registerTeacherQuoteNumber =
      'registerPage_registerTeacherQuoteNumber';
  static const registerPage_registerTeacherQuoteNumberSubtitle =
      'registerPage_registerTeacherQuoteNumberSubtitle';
  static const registerPage_registerTeacherQuoteNumberCopy =
      'registerPage_registerTeacherQuoteNumberCopy';
  static const registerPage_registerTeacherQuoteNumberCopied =
      'registerPage_registerTeacherQuoteNumberCopied';
  static const registerPage_registerTeacherQuoteTitle1 =
      'registerPage_registerTeacherQuoteTitle1';
  static const registerPage_registerTeacherQuoteTitle2 =
      'registerPage_registerTeacherQuoteTitle2';
  static const registerPage_registerTeacherQuoteTitle3 =
      'registerPage_registerTeacherQuoteTitle3';
  static const registerPage_registerTeacherQuoteNoOfStudent =
      'registerPage_registerTeacherQuoteNoOfStudent';
  static const registerPage_registerTeacherQuoteNoOfStudentSubText =
      'registerPage_registerTeacherQuoteNoOfStudentSubText';
  static const registerPage_registerTeacherQuoteDuration =
      'registerPage_registerTeacherQuoteDuration';
  static const registerPage_registerTeacherQuoteDurationOption1 =
      'registerPage_registerTeacherQuoteDurationOption1';
  static const registerPage_registerTeacherQuoteDurationOption2 =
      'registerPage_registerTeacherQuoteDurationOption2';
  static const registerPage_registerTeacherQuoteDurationOption3 =
      'registerPage_registerTeacherQuoteDurationOption3';
  static const registerPage_registerTeacherQuoteSubtotal =
      'registerPage_registerTeacherQuoteSubtotal';
  static const registerPage_registerTeacherQuoteUnitPrice =
      'registerPage_registerTeacherQuoteUnitPrice';
  static const registerPage_registerTeacherQuoteSubmit =
      'registerPage_registerTeacherQuoteSubmit';
  static const registerPage_registerTeacherQuoteBillFailed =
      'registerPage_registerTeacherQuoteBillFailed';
  static const registerPage_registerTeacherQuoteBillPending =
      'registerPage_registerTeacherQuoteBillPending';
  static const registerPage_registerTeacherQuoteBillSuccess =
      'registerPage_registerTeacherQuoteBillSuccess';
  static const registerPage_registerTeacherAccountSubmit =
      'registerPage_registerTeacherAccountSubmit';
  static const paymentIndividual_payIndCartTitle =
      'paymentIndividual_payIndCartTitle';
  static const paymentIndividual_payIndInfoTitle =
      'paymentIndividual_payIndInfoTitle';
  static const paymentIndividual_payIndBillingTitle =
      'paymentIndividual_payIndBillingTitle';
  static const paymentIndividual_payIndRightTitle =
      'paymentIndividual_payIndRightTitle';
  static const paymentIndividual_payIndRightBtn1 =
      'paymentIndividual_payIndRightBtn1';
  static const paymentIndividual_payIndRightBtn2 =
      'paymentIndividual_payIndRightBtn2';
  static const paymentIndividual_payIndRightBtn3 =
      'paymentIndividual_payIndRightBtn3';
  static const paymentIndividual_payIndRightBtn4 =
      'paymentIndividual_payIndRightBtn4';
  static const paymentIndividual_payIndRightBtnWarning =
      'paymentIndividual_payIndRightBtnWarning';
  static const paymentIndividual_payIndRightTableH1 =
      'paymentIndividual_payIndRightTableH1';
  static const paymentIndividual_payIndRightTableH2 =
      'paymentIndividual_payIndRightTableH2';
  static const paymentIndividual_payIndRightTableH3 =
      'paymentIndividual_payIndRightTableH3';
  static const paymentIndividual_payIndRightTableH4 =
      'paymentIndividual_payIndRightTableH4';
  static const paymentIndividual_payIndRightTableSubTotal =
      'paymentIndividual_payIndRightTableSubTotal';
  static const paymentIndividual_payIndRightTableTax =
      'paymentIndividual_payIndRightTableTax';
  static const paymentIndividual_payIndRightTableDiscount =
      'paymentIndividual_payIndRightTableDiscount';
  static const paymentIndividual_payIndRightTableTotal =
      'paymentIndividual_payIndRightTableTotal';
  static const teacherBillPage_payTeacherMethod1 =
      'teacherBillPage_payTeacherMethod1';
  static const teacherBillPage_payTeacherMethod2 =
      'teacherBillPage_payTeacherMethod2';
  static const teacherBillPage_payTeacherMethod3 =
      'teacherBillPage_payTeacherMethod3';
  static const teacherBillPage_payTeacherPoNumber =
      'teacherBillPage_payTeacherPoNumber';
  static const teacherBillPage_payTeacherPoNumberPlaceholder =
      'teacherBillPage_payTeacherPoNumberPlaceholder';
  static const teacherBillPage_payTeacherPoFile =
      'teacherBillPage_payTeacherPoFile';
  static const teacherBillPage_payTeacherPoFilePlaceholder =
      'teacherBillPage_payTeacherPoFilePlaceholder';
  static const teacherBillPage_payTeacherBankName =
      'teacherBillPage_payTeacherBankName';
  static const teacherBillPage_payTeacherBankNamePlaceholder =
      'teacherBillPage_payTeacherBankNamePlaceholder';
  static const teacherBillPage_payTeacherTransactionFile =
      'teacherBillPage_payTeacherTransactionFile';
  static const teacherBillPage_payTeacherTransactionFilePlaceholder =
      'teacherBillPage_payTeacherTransactionFilePlaceholder';
  static const teacherBillPage_payTeacherBankInfo =
      'teacherBillPage_payTeacherBankInfo';
  static const teacherBillPage_payTeacherRightTableH1 =
      'teacherBillPage_payTeacherRightTableH1';
  static const teacherBillPage_payTeacherRightTableH2 =
      'teacherBillPage_payTeacherRightTableH2';
  static const teacherBillPage_payTeacherRightTableH3 =
      'teacherBillPage_payTeacherRightTableH3';
  static const teacherBillPage_payTeacherRightTableH4 =
      'teacherBillPage_payTeacherRightTableH4';
  static const teacherBillPage_payTeacherAccountHeader =
      'teacherBillPage_payTeacherAccountHeader';
  static const successPage_paySuccessTitle = 'successPage_paySuccessTitle';
  static const successPage_paySuccessSubtitle =
      'successPage_paySuccessSubtitle';
  static const successPage_paySuccessUsername =
      'successPage_paySuccessUsername';
  static const successPage_paySuccessDescription1 =
      'successPage_paySuccessDescription1';
  static const successPage_paySuccessDescription2 =
      'successPage_paySuccessDescription2';
  static const successPage_paySuccessLogin = 'successPage_paySuccessLogin';
  static const successPage_paySuccessFooter = 'successPage_paySuccessFooter';
  static const breadcrumb_breadcrumbSearchPlaceholder =
      'breadcrumb_breadcrumbSearchPlaceholder';
  static const teacherHeader_teacherHeaderProfile =
      'teacherHeader_teacherHeaderProfile';
  static const teacherHeader_teacherHeaderChangeLanguage =
      'teacherHeader_teacherHeaderChangeLanguage';
  static const teacherHeader_teacherHeaderLogout =
      'teacherHeader_teacherHeaderLogout';
  static const teacherMenuList_teacherMenu = 'teacherMenuList_teacherMenu';
  static const teacherMenuList_teacherMenuClassChosen =
      'teacherMenuList_teacherMenuClassChosen';
  static const teacherMenuList_teacherMenuClassChosenManagement =
      'teacherMenuList_teacherMenuClassChosenManagement';
  static const teacherMenuList_teacherMenuClassChosenAssignment =
      'teacherMenuList_teacherMenuClassChosenAssignment';
  static const teacherMenuList_teacherMenuClassChosenReport =
      'teacherMenuList_teacherMenuClassChosenReport';
  static const teacherMenuList_teacherMenuMockexam =
      'teacherMenuList_teacherMenuMockexam';
  static const teacherMenuList_teacherMenuTestbank =
      'teacherMenuList_teacherMenuTestbank';
  static const teacherMenuList_teacherMenu1 = 'teacherMenuList_teacherMenu1';
  static const teacherMenuList_teacherMenu2 = 'teacherMenuList_teacherMenu2';
  static const teacherMenuList_teacherMenu3 = 'teacherMenuList_teacherMenu3';
  static const teacherMenuList_teacherMenu4 = 'teacherMenuList_teacherMenu4';
  static const teacherMenuList_teacherMenu5 = 'teacherMenuList_teacherMenu5';
  static const teacherMenuList_teacherMenu6 = 'teacherMenuList_teacherMenu6';
  static const teacherMenuList_teacherMenuLicense =
      'teacherMenuList_teacherMenuLicense';
  static const teacherHome_teacherHomeStudentNo =
      'teacherHome_teacherHomeStudentNo';
  static const teacherHome_teacherHomeClassNo =
      'teacherHome_teacherHomeClassNo';
  static const teacherHome_teahcerHomeUnactivatedKey =
      'teacherHome_teahcerHomeUnactivatedKey';
  static const teacherHome_teahcerHomeUngradedTests =
      'teacherHome_teahcerHomeUngradedTests';
  static const teacherHome_teacherHomeGreetingsMorning =
      'teacherHome_teacherHomeGreetingsMorning';
  static const teacherHome_teacherHomeGreetingsAfternoon =
      'teacherHome_teacherHomeGreetingsAfternoon';
  static const teacherHome_teacherHomeGreetingsEvening =
      'teacherHome_teacherHomeGreetingsEvening';
  static const teacherHome_teacherHomeClassSelectionLabel =
      'teacherHome_teacherHomeClassSelectionLabel';
  static const teacherHome_teacherHomeClassSelectionLabelWarning =
      'teacherHome_teacherHomeClassSelectionLabelWarning';
  static const teacherHome_teacherHomeOverview_1 =
      'teacherHome_teacherHomeOverview_1';
  static const teacherHome_teacherHomeOverview_1Btn =
      'teacherHome_teacherHomeOverview_1Btn';
  static const teacherHome_teacherHomeOverview_2 =
      'teacherHome_teacherHomeOverview_2';
  static const teacherHome_teacherHomeOverview_2Btn =
      'teacherHome_teacherHomeOverview_2Btn';
  static const teacherHome_teacherHomeOverview_3 =
      'teacherHome_teacherHomeOverview_3';
  static const teacherHome_teacherHomeOverview_4 =
      'teacherHome_teacherHomeOverview_4';
  static const teacherHome_teacherHomeOverview_5 =
      'teacherHome_teacherHomeOverview_5';
  static const teacherHome_teacherHomeShortcutTitle =
      'teacherHome_teacherHomeShortcutTitle';
  static const teacherHome_teacherHomeShortcutOp_1 =
      'teacherHome_teacherHomeShortcutOp_1';
  static const teacherHome_teacherHomeShortcutOp_2 =
      'teacherHome_teacherHomeShortcutOp_2';
  static const teacherHome_teacherHomeShortcutOp_3 =
      'teacherHome_teacherHomeShortcutOp_3';
  static const teacherHome_teacherHomeShortcutOp_4 =
      'teacherHome_teacherHomeShortcutOp_4';
  static const teacherHome_teacherHomeShortcutOp_5 =
      'teacherHome_teacherHomeShortcutOp_5';
  static const teacherHome_teacherHomeOngingTitle =
      'teacherHome_teacherHomeOngingTitle';
  static const teacherHome_teacherHomeOngingData_1 =
      'teacherHome_teacherHomeOngingData_1';
  static const teacherHome_teacherHomeOngingData_2 =
      'teacherHome_teacherHomeOngingData_2';
  static const teacherTestListPage_teacherTestListCategoryTitle =
      'teacherTestListPage_teacherTestListCategoryTitle';
  static const teacherTestListPage_teacherTestListCategorySearchPlaceholder =
      'teacherTestListPage_teacherTestListCategorySearchPlaceholder';
  static const teacherTestListPage_teacherTestListCategorySortMethod =
      'teacherTestListPage_teacherTestListCategorySortMethod';
  static const teacherTestListPage_teacherTestListCategorySortMethod_1 =
      'teacherTestListPage_teacherTestListCategorySortMethod_1';
  static const teacherTestListPage_teacherTestListCategorySortMethod_1Desc =
      'teacherTestListPage_teacherTestListCategorySortMethod_1Desc';
  static const teacherTestListPage_teacherTestListCategorySortMethod_2 =
      'teacherTestListPage_teacherTestListCategorySortMethod_2';
  static const teacherTestListPage_teacherTestListCategorySortMethod_2Desc =
      'teacherTestListPage_teacherTestListCategorySortMethod_2Desc';
  static const teacherTestListPage_teacherTestListAllTitle =
      'teacherTestListPage_teacherTestListAllTitle';
  static const teacherTestListPage_teacherTestListAllBtnAssign =
      'teacherTestListPage_teacherTestListAllBtnAssign';
  static const teacherTestListPage_teacherTestListAllBtnReview =
      'teacherTestListPage_teacherTestListAllBtnReview';
  static const teacherOpenQueuedPage_teacherOpenQueuedBtnCreate =
      'teacherOpenQueuedPage_teacherOpenQueuedBtnCreate';
  static const teacherOpenQueuedPage_teacherOpenQueuedBtnDelete =
      'teacherOpenQueuedPage_teacherOpenQueuedBtnDelete';
  static const teacherOpenQueuedPage_teacherAssessmentDeleteWarning =
      'teacherOpenQueuedPage_teacherAssessmentDeleteWarning';
  static const teacherOpenQueuedPage_teacherAssessmentDeleteTitle =
      'teacherOpenQueuedPage_teacherAssessmentDeleteTitle';
  static const teacherOpenQueuedPage_teacherAssessmentDeleteDescription =
      'teacherOpenQueuedPage_teacherAssessmentDeleteDescription';
  static const teacherManagementPage_teacherManagementAllStudentsTitle =
      'teacherManagementPage_teacherManagementAllStudentsTitle';
  static const teacherManagementPage_teacherManagementClassesTitle =
      'teacherManagementPage_teacherManagementClassesTitle';
  static const teacherManagementPage_teacherManagementAllClassesTitle =
      'teacherManagementPage_teacherManagementAllClassesTitle';
  static const teacherManagementPage_teacherManagementWarningNocontent =
      'teacherManagementPage_teacherManagementWarningNocontent';
  static const teacherManagementPage_teacherManagementWarningNoStudent =
      'teacherManagementPage_teacherManagementWarningNoStudent';
  static const teacherManagementPage_teacherManagementIconBtnDeleteClass =
      'teacherManagementPage_teacherManagementIconBtnDeleteClass';
  static const teacherManagementPage_teacherManagementIconBtnEditTitle =
      'teacherManagementPage_teacherManagementIconBtnEditTitle';
  static const teacherManagementPage_teacherManagementIconBtnEditStudents =
      'teacherManagementPage_teacherManagementIconBtnEditStudents';
  static const teacherManagementPage_teacherManagementIconBtnChangePassword =
      'teacherManagementPage_teacherManagementIconBtnChangePassword';
  static const teacherManagementPage_teacherManagementIconBtnDeleteForever =
      'teacherManagementPage_teacherManagementIconBtnDeleteForever';
  static const teacherManagementPage_teacherManagementIconBtnCreateStudent =
      'teacherManagementPage_teacherManagementIconBtnCreateStudent';
  static const teacherManagementPage_teacherManagementIconBtnDelete =
      'teacherManagementPage_teacherManagementIconBtnDelete';
  static const teacherManagementPage_teacherManagementIconBtnCreateClass =
      'teacherManagementPage_teacherManagementIconBtnCreateClass';
  static const teacherManagementPage_teacherClassManagementCreateClass =
      'teacherManagementPage_teacherClassManagementCreateClass';
  static const teacherManagementPage_teacherClassManagementEdit =
      'teacherManagementPage_teacherClassManagementEdit';
  static const teacherManagementPage_teacherClassManagementDelete =
      'teacherManagementPage_teacherClassManagementDelete';
  static const teacherManagementPage_teacherClassManagementChangePassword =
      'teacherManagementPage_teacherClassManagementChangePassword';
  static const teacherManagementPage_teacherStudentManagementCreateStudent =
      'teacherManagementPage_teacherStudentManagementCreateStudent';
  static const teacherCreateAssessmentPage_teacherCreateAssStepperStep1 =
      'teacherCreateAssessmentPage_teacherCreateAssStepperStep1';
  static const teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub =
      'teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub';
  static const teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub2 =
      'teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub2';
  static const teacherCreateAssessmentPage_teacherCreateAss_TestTypeMockexam =
      'teacherCreateAssessmentPage_teacherCreateAss_TestTypeMockexam';
  static const teacherCreateAssessmentPage_teacherCreateAss_TestTypeTestbank =
      'teacherCreateAssessmentPage_teacherCreateAss_TestTypeTestbank';
  static const teacherCreateAssessmentPage_teacherCreateAssStepperStep2 =
      'teacherCreateAssessmentPage_teacherCreateAssStepperStep2';
  static const teacherCreateAssessmentPage_teacherCreateAssStepperStep3 =
      'teacherCreateAssessmentPage_teacherCreateAssStepperStep3';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentName =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentName';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentNamePlaceholder =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentNamePlaceholder';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsName =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsName';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNamePlaceholder =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNamePlaceholder';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarning =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarning';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameError =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameError';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStartDate =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDate';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStartDatePlaceholder =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDatePlaceholder';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooEarly =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooEarly';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooLate =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooLate';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsDueDate =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDate';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsDueDateTooEarly =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDateTooEarly';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsDueDatePlaceholder =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDatePlaceholder';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsInstruction =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsInstruction';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsInstructionPlaceholder =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsInstructionPlaceholder';
  static const teacherCreateAssessmentPage_teacherCreateAssFeaturesTextmode =
      'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextmode';
  static const teacherCreateAssessmentPage_teacherCreateAssTextModeExercise =
      'teacherCreateAssessmentPage_teacherCreateAssTextModeExercise';
  static const teacherCreateAssessmentPage_teacherCreateAssTextModeTest =
      'teacherCreateAssessmentPage_teacherCreateAssTextModeTest';
  static const teacherCreateAssessmentPage_teacherCreateAssFeaturesText =
      'teacherCreateAssessmentPage_teacherCreateAssFeaturesText';
  static const teacherCreateAssessmentPage_teacherCreateAssFeaturesTextSimplified =
      'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextSimplified';
  static const teacherCreateAssessmentPage_teacherCreateAssFeaturesTextTraditional =
      'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextTraditional';
  static const teacherCreateAssessmentPage_teacherCreateAssFeaturesAudio =
      'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudio';
  static const teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioMandarin =
      'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioMandarin';
  static const teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioCantonese =
      'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioCantonese';
  static const teacherCreateAssessmentPage_teacherCreateAssTestsLeftTitle =
      'teacherCreateAssessmentPage_teacherCreateAssTestsLeftTitle';
  static const teacherCreateAssessmentPage_teacherCreateAssTestsRightWarning =
      'teacherCreateAssessmentPage_teacherCreateAssTestsRightWarning';
  static const teacherCreateAssessmentPage_teacherCreateAssTestsError =
      'teacherCreateAssessmentPage_teacherCreateAssTestsError';
  static const teacherCreateAssessmentPage_teacherCreateAssReviewEdit =
      'teacherCreateAssessmentPage_teacherCreateAssReviewEdit';
  static const teacherCreateAssessmentPage_teacherReviewTestQuit =
      'teacherCreateAssessmentPage_teacherReviewTestQuit';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSkill =
      'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSkill';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONS_All =
      'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONS_All';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSListening =
      'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSListening';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSpeaking =
      'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSpeaking';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSReading =
      'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSReading';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSWriting =
      'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSWriting';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Theme =
      'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Theme';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_All =
      'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_All';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Global =
      'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Global';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Life =
      'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Life';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Interdisciplinary =
      'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Interdisciplinary';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Families =
      'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Families';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Identities =
      'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Identities';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Beauty =
      'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Beauty';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_All =
      'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_All';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Identities =
      'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Identities';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Experiences =
      'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Experiences';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Human =
      'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Human';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Social =
      'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Social';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Sharing =
      'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Sharing';
  static const teacherGradePage_teacherGradeTestQuestionIndex =
      'teacherGradePage_teacherGradeTestQuestionIndex';
  static const teacherGradePage_teacherGradeTestScore =
      'teacherGradePage_teacherGradeTestScore';
  static const teacherGradePage_teacherGradeTestComment =
      'teacherGradePage_teacherGradeTestComment';
  static const teacherGradePage_teacherGradeTestSpeakingWarning =
      'teacherGradePage_teacherGradeTestSpeakingWarning';
  static const teacherReportPage_teacherReportTaskName =
      'teacherReportPage_teacherReportTaskName';
  static const teacherProfile_teacherProfileChangePassword =
      'teacherProfile_teacherProfileChangePassword';
  static const teacherProfile_teacherProfileUpdate =
      'teacherProfile_teacherProfileUpdate';
  static const teacherProfile_teacherProfileUpdateSuccess =
      'teacherProfile_teacherProfileUpdateSuccess';
  static const teacherProfile_teacherProfileUpdateWarning =
      'teacherProfile_teacherProfileUpdateWarning';
  static const teacherProfile_teacherProfileMaxStudent =
      'teacherProfile_teacherProfileMaxStudent';
  static const individualPage_individualHome = 'individualPage_individualHome';
  static const individualPage_individualHomeTitle =
      'individualPage_individualHomeTitle';
  static const individualPage_individualHomeAplibTitle =
      'individualPage_individualHomeAplibTitle';
  static const individualPage_individualHomeAprecordTitle =
      'individualPage_individualHomeAprecordTitle';
  static const individualPage_individualHomeReportTitle =
      'individualPage_individualHomeReportTitle';
  static const individualPage_individualHomeProfileTitle =
      'individualPage_individualHomeProfileTitle';
  static const individualPage_individualHomeOverview_1 =
      'individualPage_individualHomeOverview_1';
  static const individualPage_individualHomeOverview_1Btn =
      'individualPage_individualHomeOverview_1Btn';
  static const individualPage_individualHomeOverview_2 =
      'individualPage_individualHomeOverview_2';
  static const individualPage_individualHomeOverview_2Btn =
      'individualPage_individualHomeOverview_2Btn';
  static const individualPage_individualHomeOverview_3 =
      'individualPage_individualHomeOverview_3';
  static const individualPage_individualHomeOverview_4 =
      'individualPage_individualHomeOverview_4';
  static const apTest_apLibpageContinue = 'apTest_apLibpageContinue';
  static const apTest_apLibpageStart = 'apTest_apLibpageStart';
  static const apTest_apLibpageRestart = 'apTest_apLibpageRestart';
  static const apTest_apLibpageReview = 'apTest_apLibpageReview';
  static const apTest_apLibpageNullAssessmentList =
      'apTest_apLibpageNullAssessmentList';
  static const apTest_apLibpageNullList = 'apTest_apLibpageNullList';
  static const apTest_apModalTestmodalexercise =
      'apTest_apModalTestmodalexercise';
  static const apTest_apModalTestmodaltest = 'apTest_apModalTestmodaltest';
  static const apTest_apModalTestModalSet = 'apTest_apModalTestModalSet';
  static const apTest_apModalTextLangSet = 'apTest_apModalTextLangSet';
  static const apTest_apModalTextSimplify = 'apTest_apModalTextSimplify';
  static const apTest_apModalTextTraditional = 'apTest_apModalTextTraditional';
  static const apTest_apModalAudioLangSet = 'apTest_apModalAudioLangSet';
  static const apTest_apModalAudioMandarin = 'apTest_apModalAudioMandarin';
  static const apTest_apModalAudioCatonese = 'apTest_apModalAudioCatonese';
  static const apTest_apModalButtonStart = 'apTest_apModalButtonStart';
  static const apTest_apQListTitle = 'apTest_apQListTitle';
  static const apTest_apTestButtonSave = 'apTest_apTestButtonSave';
  static const apTest_apTestButtonPrev = 'apTest_apTestButtonPrev';
  static const apTest_apTestButtonNext = 'apTest_apTestButtonNext';
  static const apTest_apTestButtonSubmit = 'apTest_apTestButtonSubmit';
  static const apTest_apLibpageQuit = 'apTest_apLibpageQuit';
  static const apTest_apRecordFinalPageSentence =
      'apTest_apRecordFinalPageSentence';
  static const apTest_apTestFinalPageSentence =
      'apTest_apTestFinalPageSentence';
  static const apBank_apTestBankStart = 'apBank_apTestBankStart';
  static const apBank_apTestBankTitle = 'apBank_apTestBankTitle';
  static const apBank_apTestBankDescription = 'apBank_apTestBankDescription';
  static const apRecording_apAudioTooltipsStart =
      'apRecording_apAudioTooltipsStart';
  static const apRecording_apAudioTooltipsStop =
      'apRecording_apAudioTooltipsStop';
  static const apRecording_apAudioTooltipsSave =
      'apRecording_apAudioTooltipsSave';
  static const apRecording_apAudioTooltipsDelete =
      'apRecording_apAudioTooltipsDelete';
  static const apWriting_apWriteWordCount = 'apWriting_apWriteWordCount';
  static const studentPage_studentHome = 'studentPage_studentHome';
  static const studentPage_studentHomeTitle = 'studentPage_studentHomeTitle';
  static const studentPage_studentHomeAssessmentTitle =
      'studentPage_studentHomeAssessmentTitle';
  static const studentPage_studentHomeRecordTitle =
      'studentPage_studentHomeRecordTitle';
  static const studentPage_studentHomeReportTitle =
      'studentPage_studentHomeReportTitle';
  static const studentPage_studentPersonReportTitle =
      'studentPage_studentPersonReportTitle';
  static const studentPage_studentClassReportTitle =
      'studentPage_studentClassReportTitle';
  static const studentPage_studentHomeProfileTitle =
      'studentPage_studentHomeProfileTitle';
  static const studentPage_studentTestInstructionReminder =
      'studentPage_studentTestInstructionReminder';
  static const studentPage_studentTestInstruction =
      'studentPage_studentTestInstruction';
  static const modals_modalClose = 'modals_modalClose';
  static const forgotPassword_modalForgotPasswordTitle =
      'forgotPassword_modalForgotPasswordTitle';
  static const forgotPassword_modalForgotPasswordSuccess =
      'forgotPassword_modalForgotPasswordSuccess';
  static const questionIndexModal_modalQuestionIndexTitle =
      'questionIndexModal_modalQuestionIndexTitle';
  static const questionIndexModal_modalQuestionIndexClose =
      'questionIndexModal_modalQuestionIndexClose';
  static const questionIndexModal_modalBankInfoTitle =
      'questionIndexModal_modalBankInfoTitle';
  static const questionIndexModal_modalWaitTitle =
      'questionIndexModal_modalWaitTitle';
  static const questionIndexModal_modalWaitContent =
      'questionIndexModal_modalWaitContent';
  static const createClass_modalCreateClassTitle =
      'createClass_modalCreateClassTitle';
  static const createClass_modalCreateClassStudents =
      'createClass_modalCreateClassStudents';
  static const createClass_modalCreateClassStudentsPlaceholder =
      'createClass_modalCreateClassStudentsPlaceholder';
  static const createClass_modalCreateClassNoOptions =
      'createClass_modalCreateClassNoOptions';
  static const createClass_modalCreateClassSuccess =
      'createClass_modalCreateClassSuccess';
  static const createClass_modalCreateClassKeys =
      'createClass_modalCreateClassKeys';
  static const createClass_modalCreateClassNoKeys =
      'createClass_modalCreateClassNoKeys';
  static const add_StudentToClass_modalAddStudentTitle =
      'add_StudentToClass_modalAddStudentTitle';
  static const add_StudentToClass_modalAddStudentStudents =
      'add_StudentToClass_modalAddStudentStudents';
  static const add_StudentToClass_modalAddStudentNoOptions =
      'add_StudentToClass_modalAddStudentNoOptions';
  static const add_StudentToClass_modalAddStudentTooShort =
      'add_StudentToClass_modalAddStudentTooShort';
  static const add_StudentToClass_modalAddStudentSuccess =
      'add_StudentToClass_modalAddStudentSuccess';
  static const createStudent_modalCreateStudentTitle =
      'createStudent_modalCreateStudentTitle';
  static const createStudent_modalCreateStudentClassIdsLabel =
      'createStudent_modalCreateStudentClassIdsLabel';
  static const createStudent_modalCreateStudentClassIdsPlaceholder =
      'createStudent_modalCreateStudentClassIdsPlaceholder';
  static const createStudent_modalCreateStudentClassIdsWarning =
      'createStudent_modalCreateStudentClassIdsWarning';
  static const createStudent_modalCreateStudentSuccess =
      'createStudent_modalCreateStudentSuccess';
  static const createStudent_modalCreateStudentLicenseLabel =
      'createStudent_modalCreateStudentLicenseLabel';
  static const createStudent_modalCreateStudentLicenseWarning =
      'createStudent_modalCreateStudentLicenseWarning';
  static const createStudent_modalCreateStudentLicensePlaceholder =
      'createStudent_modalCreateStudentLicensePlaceholder';
  static const createStudent_modalCreateStudentDescription =
      'createStudent_modalCreateStudentDescription';
  static const updateStudent_modalUpdateStudentTitle =
      'updateStudent_modalUpdateStudentTitle';
  static const updateStudent_modalUpdateStudentSuccess =
      'updateStudent_modalUpdateStudentSuccess';
  static const updateStudent_modalUpdateStudentLicenseLabel =
      'updateStudent_modalUpdateStudentLicenseLabel';
  static const updateStudent_modalUpdateStudentLicensePlaceholder =
      'updateStudent_modalUpdateStudentLicensePlaceholder';
  static const updateStudent_modalUpdateStudentLicenseWarning =
      'updateStudent_modalUpdateStudentLicenseWarning';
  static const changePassword_modalChangePasswordTitle =
      'changePassword_modalChangePasswordTitle';
  static const changePassword_modalChangePasswordSuccess =
      'changePassword_modalChangePasswordSuccess';
  static const alertDialog_modalDeleteClassTitle =
      'alertDialog_modalDeleteClassTitle';
  static const alertDialog_modalDeleteClassDescription =
      'alertDialog_modalDeleteClassDescription';
  static const alertDialog_modalDeleteClassSuccess =
      'alertDialog_modalDeleteClassSuccess';
  static const alertDialog_modalDeleteStudentTitle =
      'alertDialog_modalDeleteStudentTitle';
  static const alertDialog_modalDeleteStudentDescription =
      'alertDialog_modalDeleteStudentDescription';
  static const alertDialog_modalDeleteStudentSuccess =
      'alertDialog_modalDeleteStudentSuccess';
  static const gradeExplian_modalGradeExplainTitle =
      'gradeExplian_modalGradeExplainTitle';
  static const apTest_studentTestInstructionReminder =
      'apTest_studentTestInstructionReminder';
  static const apTest_studentTestInstruction = 'apTest_studentTestInstruction';
  static const icaNavigation_studyRecords = 'icaNavigation_studyRecords';
  static const icaNavigation_assignments = 'icaNavigation_assignments';
  static const icaNavigation_profileAndSettings =
      'icaNavigation_profileAndSettings';
  static const exceptions_defaultMessage = 'exceptions_defaultMessage';
  static const exceptions_internetMessage = 'exceptions_internetMessage';
  static const exceptions_invalidTokenMessage =
      'exceptions_invalidTokenMessage';
  static const exceptions_timeOutMessage = 'exceptions_timeOutMessage';
  static const exceptions_userNotFoundMessage =
      'exceptions_userNotFoundMessage';
  static const util_success = 'util_success';
  static const util_warning = 'util_warning';
  static const util_error = 'util_error';
  static const services_auth_userNotFoundMessage =
      'services_auth_userNotFoundMessage';
  static const initializer_unauthorizedAccess =
      'initializer_unauthorizedAccess';
  static const initializer_dueToInactivity = 'initializer_dueToInactivity';
  static const message_msgForm_InvalidUsername =
      'message_msgForm_InvalidUsername';
  static const formInfo_formLicense = 'formInfo_formLicense';
  static const formInfo_formLicensePlaceholder =
      'formInfo_formLicensePlaceholder';
  static const loginPage_loginCheckQuote = 'loginPage_loginCheckQuote';
  static const registerPage_registerTeacherIndexQ2Cancel =
      'registerPage_registerTeacherIndexQ2Cancel';
  static const teacherTestListPage_teacherTestListCategoryFilterMethod =
      'teacherTestListPage_teacherTestListCategoryFilterMethod';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentClassPlaceholder =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentClassPlaceholder';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentClass =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentClass';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsClassId =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsClassId';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsClassIdPlaceholder =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsClassIdPlaceholder';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsClassIdWarning =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsClassIdWarning';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarningMOCK_EXAM =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarningMOCK_EXAM';
  static const teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarningTEST_BANK =
      'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarningTEST_BANK';
  static const teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_WhoWeAre =
      'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_WhoWeAre';
  static const individualPage_individualHomeAptestTitle =
      'individualPage_individualHomeAptestTitle';
  static const individualPage_individualHomeApmockTitle =
      'individualPage_individualHomeApmockTitle';
  static const individualPage_individualHomeIbmockTitle =
      'individualPage_individualHomeIbmockTitle';
  static const apTest_apModalTestBankName = 'apTest_apModalTestBankName';
  static const addedAfter_apAudioTooltipsSpeed =
      'addedAfter_apAudioTooltipsSpeed';
  static const addedAfter_apSubmitionTestText =
      'addedAfter_apSubmitionTestText';
  static const addedAfter_apSubmitionText = 'addedAfter_apSubmitionText';
  static const addedAfter_apAudioStart = 'addedAfter_apAudioStart';
}

class Locales {
  static const zh_HK = {
    'message_default': 'error',
    'message_msgFormRequired': '必填',
    'message_msgFormInvalidUsername': '這個用戶名已經有人使用了!',
    'message_msgFormInvalidEmail': '這個郵箱位址無效',
    'message_msgFormUsedEmail': '這個郵箱已經有人使用了!',
    'message_msgFormNotSameEmail': '請確保兩次電子郵箱輸入一致',
    'message_msgFormLongPassword': '這個密碼太長了!',
    'message_msgFormNotSamePassword': '請確保兩次密碼輸入一致',
    'message_msgFormShortPassword': '密碼最少要有6個字元',
    'message_msgFormSpecifyPassword': '密碼必須包括最少1個數字號碼和1個特殊符號',
    'message_msgFormNotSupportedFormat': '對不起，我們不支持這種檔案格式',
    'formInfo_formClassName': '班級名稱',
    'formInfo_formClassNamePlaceholder': '請輸入班級名稱',
    'formInfo_formFirstName': '名字',
    'formInfo_formFirstNamePlaceholder': '請輸入您的名字',
    'formInfo_formLastName': '姓',
    'formInfo_formLastNamePlaceholder': '請輸入您的姓',
    'formInfo_formTeacherEmail': '老師郵箱',
    'formInfo_formTeacherEmailPlaceholder': '如果您已經有老師，輸入老師的電子郵件信箱',
    'formInfo_formEmail': '郵箱',
    'formInfo_formEmailPlaceholder': '請輸入您的電子郵箱',
    'formInfo_formConfirmEmail': '確認電子郵箱',
    'formInfo_formConfirmEmailPlaceholder': '請重新輸入您的電子郵箱',
    'formInfo_formPassword': '密碼',
    'formInfo_formCurpassword': '原有密碼',
    'formInfo_formNewpassword': '新的密碼',
    'formInfo_formPasswordPlaceholder': '請輸入密碼',
    'formInfo_formCurpasswordPlaceholder': '請輸入原有密碼',
    'formInfo_formNewpasswordPlaceholder': '請輸入新的密碼',
    'formInfo_formConfirmPassword': '確認密碼',
    'formInfo_formConfirmPasswordPlaceholder': '請再一次輸入密碼',
    'formInfo_formUserName': '用戶名',
    'formInfo_formUserNamePlaceholder': '請輸入您的姓名',
    'formInfo_formPhone': '電話號碼',
    'formInfo_formPhonePlaceholder': '請輸入您的電話號碼',
    'formInfo_formCountry': '國家',
    'formInfo_formCountryPlaceholder': '請選擇您的國家',
    'formInfo_formRegion': '區域',
    'formInfo_formRegionPlaceholder': '請選擇您所在的地區',
    'formInfo_formZipCode': '郵遞區號',
    'formInfo_formZipCodePlaceholder': '請輸入您的郵遞區號',
    'formInfo_formCancel': '取消',
    'formInfo_formSubmit': '提交',
    'formInfo_formNext': '下一步',
    'formInfo_formBack': '返回',
    'formInfo_formOk': '完成',
    'formInfo_formLoading': '載入中...',
    'formInfo_tableNoContent': '沒有資料',
    'modal_modalAgree': '同意',
    'modal_modalDisagree': '不同意',
    'modal_modalSubmit': '提交',
    'modal_modalCancel': '取消',
    'headerInfo_headerHome': '主頁',
    'headerInfo_headerSimulations': '模擬題集',
    'headerInfo_headerSimulationsIndividual': '個人',
    'headerInfo_headerSimulationsStudents': '班級',
    'headerInfo_headerSimulationsOverview': '概覽',
    'headerInfo_headerQuestionBank': '題庫',
    'headerInfo_headerQuestionBankIndividual': '個人',
    'headerInfo_headerQuestionBankStudents': '班級',
    'headerInfo_headerQuestionBankOverview': '概覽',
    'headerInfo_headerPrice': '價格',
    'headerInfo_headerPriceIndividual': '個人',
    'headerInfo_headerPriceSchool': '班級',
    'headerInfo_headerComments': '評論',
    'headerInfo_headerQuestions': '問題',
    'headerInfo_headerBuy': '購買',
    'headerInfo_headerBuyIndividual': '個人',
    'headerInfo_headerBuySchool': '班級',
    'headerInfo_headerContact': '聯繫我們',
    'headerInfo_headerLogin': '登錄',
    'headerInfo_headerLogout': '退出',
    'footerInfo_footerTerms': '隱私與條款',
    'loginPage_loginLoginName': '用戶名',
    'loginPage_loginLoginNamePlaceholder': '請輸入用戶名',
    'loginPage_loginLogin': '登錄',
    'loginPage_loginSignUp': '註冊',
    'loginPage_loginForgetPassword': '忘記密碼',
    'resetPassword_resetPasswordSuccess': '密碼已重置！',
    'registerPage_registerTitle': '您是什麼身份？',
    'registerPage_registerIndividual': '個人',
    'registerPage_registerTeacher': '老師',
    'registerPage_registerTeacherIndexQ1': '您有報價單號嗎?',
    'registerPage_registerTeacherIndexA1': '有',
    'registerPage_registerTeacherIndexA2': '沒有',
    'registerPage_registerTeacherIndexQ2QuoteNumber': '報價單號',
    'registerPage_registerTeacherIndexQ2QuoteNumberPlaceholder': '請輸入您的報價單號',
    'registerPage_registerTeacherIndexQ2Email': '電子郵箱',
    'registerPage_registerTeacherIndexQ2EmailPlaceholder': '請輸入電子郵箱',
    'registerPage_registerTeacherIndexQ2Submit': '查看狀態',
    'registerPage_registerTeacherIndexQ2Pending': '您的付款已在處理當中，通常需要24小時的處理時間。',
    'registerPage_registerTeacherIndexQ2Success': '恭喜您! 您已經完成了購買！',
    'registerPage_registerTeacherQuoteNumber': '您的報價單號是:',
    'registerPage_registerTeacherQuoteNumberSubtitle': '請記住這組號碼',
    'registerPage_registerTeacherQuoteNumberCopy': '複製',
    'registerPage_registerTeacherQuoteNumberCopied': '已經複製',
    'registerPage_registerTeacherQuoteTitle1': '索取報價',
    'registerPage_registerTeacherQuoteTitle2': '關於你',
    'registerPage_registerTeacherQuoteTitle3': '訂單資訊',
    'registerPage_registerTeacherQuoteNoOfStudent': '學生數量',
    'registerPage_registerTeacherQuoteNoOfStudentSubText': '請輸入您要購買的學生帳號數量',
    'registerPage_registerTeacherQuoteDuration': '使用期限',
    'registerPage_registerTeacherQuoteDurationOption1': '一年',
    'registerPage_registerTeacherQuoteDurationOption2': '二年',
    'registerPage_registerTeacherQuoteDurationOption3': '三年',
    'registerPage_registerTeacherQuoteSubtotal': '小計',
    'registerPage_registerTeacherQuoteUnitPrice': '單價',
    'registerPage_registerTeacherQuoteSubmit': '索取報價',
    'registerPage_registerTeacherQuoteBillFailed':
        '對不起，您的資料有錯誤，麻煩您重新提交，或者使用其他的付款方式。',
    'registerPage_registerTeacherQuoteBillPending': '您的支付正在審理中',
    'registerPage_registerTeacherQuoteBillSuccess': '恭喜您！ 您已經完成了帳號申請！',
    'registerPage_registerTeacherAccountSubmit': '創建帳號',
    'paymentIndividual_payIndCartTitle': '您的購物車',
    'paymentIndividual_payIndInfoTitle': '個人資訊',
    'paymentIndividual_payIndBillingTitle': '信用卡持有人資訊',
    'paymentIndividual_payIndRightTitle': '訂單資訊',
    'paymentIndividual_payIndRightBtn1': '選擇這項產品',
    'paymentIndividual_payIndRightBtn2': '存儲這條資訊',
    'paymentIndividual_payIndRightBtn3': '使用此付款方式',
    'paymentIndividual_payIndRightBtn4': '創建一個帳號',
    'paymentIndividual_payIndRightBtnWarning': '請先存儲付款方式',
    'paymentIndividual_payIndRightTableH1': '產品',
    'paymentIndividual_payIndRightTableH2': '從',
    'paymentIndividual_payIndRightTableH3': '到',
    'paymentIndividual_payIndRightTableH4': '價格',
    'paymentIndividual_payIndRightTableSubTotal': '稅前小計',
    'paymentIndividual_payIndRightTableTax': '預估稅款',
    'paymentIndividual_payIndRightTableDiscount': '折扣碼',
    'paymentIndividual_payIndRightTableTotal': '總計',
    'teacherBillPage_payTeacherMethod1': '信用卡',
    'teacherBillPage_payTeacherMethod2': '下訂單',
    'teacherBillPage_payTeacherMethod3': '轉帳',
    'teacherBillPage_payTeacherPoNumber': 'PO 號碼',
    'teacherBillPage_payTeacherPoNumberPlaceholder': '請輸入 PO 號碼',
    'teacherBillPage_payTeacherPoFile': 'PO 文件',
    'teacherBillPage_payTeacherPoFilePlaceholder': '請上傳您的 PO 文件',
    'teacherBillPage_payTeacherBankName': '銀行名',
    'teacherBillPage_payTeacherBankNamePlaceholder': '請輸入銀行名',
    'teacherBillPage_payTeacherTransactionFile': '交易檔',
    'teacherBillPage_payTeacherTransactionFilePlaceholder': '請上傳交易檔',
    'teacherBillPage_payTeacherBankInfo': '我們的銀行資訊',
    'teacherBillPage_payTeacherRightTableH1': '產品',
    'teacherBillPage_payTeacherRightTableH2': '學生數量',
    'teacherBillPage_payTeacherRightTableH3': '使用期限',
    'teacherBillPage_payTeacherRightTableH4': '單價',
    'teacherBillPage_payTeacherAccountHeader': '創建您的帳戶',
    'successPage_paySuccessTitle': '恭喜您!',
    'successPage_paySuccessSubtitle': '您的帳號已經建立。',
    'successPage_paySuccessUsername': '用戶名',
    'successPage_paySuccessDescription1': '帳號從 {from} 到 {to} 有效.',
    'successPage_paySuccessDescription2': '請檢查您的郵件信箱，啟動您的帳戶',
    'successPage_paySuccessLogin': '登錄',
    'successPage_paySuccessFooter': '您若有任何問題，歡迎隨時電郵至 {email}',
    'breadcrumb_breadcrumbSearchPlaceholder': '搜索',
    'teacherHeader_teacherHeaderProfile': '個人資料',
    'teacherHeader_teacherHeaderChangeLanguage': '語言切換',
    'teacherHeader_teacherHeaderLogout': '退出',
    'teacherMenuList_teacherMenu': '主頁',
    'teacherMenuList_teacherMenuClassChosen': '選擇班級',
    'teacherMenuList_teacherMenuClassChosenManagement': '管理',
    'teacherMenuList_teacherMenuClassChosenAssignment': '作業',
    'teacherMenuList_teacherMenuClassChosenReport': '報告',
    'teacherMenuList_teacherMenuMockexam': '模擬試題',
    'teacherMenuList_teacherMenuTestbank': '題庫',
    'teacherMenuList_teacherMenu1': '模擬題集',
    'teacherMenuList_teacherMenu2': '作業考試',
    'teacherMenuList_teacherMenu3': '學習報告',
    'teacherMenuList_teacherMenu4': '班級管理',
    'teacherMenuList_teacherMenu5': '學生管理',
    'teacherMenuList_teacherMenu6': '帳戶設置',
    'teacherMenuList_teacherMenuLicense': '教師許可證',
    'teacherHome_teacherHomeStudentNo': '學生',
    'teacherHome_teacherHomeClassNo': '班級',
    'teacherHome_teahcerHomeUnactivatedKey': '未啟動帳號',
    'teacherHome_teahcerHomeUngradedTests': '等待評分',
    'teacherHome_teacherHomeGreetingsMorning': '早上好',
    'teacherHome_teacherHomeGreetingsAfternoon': '下午好',
    'teacherHome_teacherHomeGreetingsEvening': '晚上好',
    'teacherHome_teacherHomeClassSelectionLabel': '請選擇您要查看的班級',
    'teacherHome_teacherHomeClassSelectionLabelWarning': '沒有任何班級, 請先創建一個班級',
    'teacherHome_teacherHomeOverview_1': '正在進行中的任務',
    'teacherHome_teacherHomeOverview_1Btn': '查看今天的任務',
    'teacherHome_teacherHomeOverview_2': '等待評分的任務',
    'teacherHome_teacherHomeOverview_2Btn': '去批改作業',
    'teacherHome_teacherHomeOverview_3': '所有任務',
    'teacherHome_teacherHomeOverview_4': '所有學生',
    'teacherHome_teacherHomeOverview_5': '最多能容納的學生數量',
    'teacherHome_teacherHomeShortcutTitle': '快捷方式',
    'teacherHome_teacherHomeShortcutOp_1': '創建任務',
    'teacherHome_teacherHomeShortcutOp_2': '添加學生',
    'teacherHome_teacherHomeShortcutOp_3': '任務詳情',
    'teacherHome_teacherHomeShortcutOp_4': '個性化設置',
    'teacherHome_teacherHomeShortcutOp_5': '創建班級',
    'teacherHome_teacherHomeOngingTitle': '即時任務',
    'teacherHome_teacherHomeOngingData_1': '正在進行中的任務',
    'teacherHome_teacherHomeOngingData_2': '進行中',
    'teacherTestListPage_teacherTestListCategoryTitle': '類別',
    'teacherTestListPage_teacherTestListCategorySearchPlaceholder': '搜索',
    'teacherTestListPage_teacherTestListCategorySortMethod': '分類',
    'teacherTestListPage_teacherTestListCategorySortMethod_1': '標題從最低開始',
    'teacherTestListPage_teacherTestListCategorySortMethod_1Desc': '標題從最高開始',
    'teacherTestListPage_teacherTestListCategorySortMethod_2': '從最低階開始',
    'teacherTestListPage_teacherTestListCategorySortMethod_2Desc': '從最高階開始',
    'teacherTestListPage_teacherTestListAllTitle': '測驗',
    'teacherTestListPage_teacherTestListAllBtnAssign': '佈置任務',
    'teacherTestListPage_teacherTestListAllBtnReview': '檢查',
    'teacherOpenQueuedPage_teacherOpenQueuedBtnCreate': '新建',
    'teacherOpenQueuedPage_teacherOpenQueuedBtnDelete': '刪除',
    'teacherOpenQueuedPage_teacherAssessmentDeleteWarning': '請選擇最少一項測驗。',
    'teacherOpenQueuedPage_teacherAssessmentDeleteTitle': '您確定要刪除選擇的測驗嗎?',
    'teacherOpenQueuedPage_teacherAssessmentDeleteDescription':
        '選擇的測驗將被永久刪除， 學生們將無法使用這些作業',
    'teacherManagementPage_teacherManagementAllStudentsTitle': '所有學生',
    'teacherManagementPage_teacherManagementClassesTitle': '學生在 {label}',
    'teacherManagementPage_teacherManagementAllClassesTitle': '所有班級',
    'teacherManagementPage_teacherManagementWarningNocontent': '這個班級沒有任何學生。',
    'teacherManagementPage_teacherManagementWarningNoStudent':
        '沒有學生, 請先創建一個學生帳號。',
    'teacherManagementPage_teacherManagementIconBtnDeleteClass': '刪除班級',
    'teacherManagementPage_teacherManagementIconBtnEditTitle': '編輯班級名稱',
    'teacherManagementPage_teacherManagementIconBtnEditStudents':
        '增加或刪除這個班級裡的學生',
    'teacherManagementPage_teacherManagementIconBtnChangePassword': '更改密碼',
    'teacherManagementPage_teacherManagementIconBtnDeleteForever': '永久刪除此學生帳號',
    'teacherManagementPage_teacherManagementIconBtnCreateStudent': '建立一個新學生帳號',
    'teacherManagementPage_teacherManagementIconBtnDelete': '刪除班級',
    'teacherManagementPage_teacherManagementIconBtnCreateClass': '建立一個新的班級',
    'teacherManagementPage_teacherClassManagementCreateClass': '新建班級',
    'teacherManagementPage_teacherClassManagementEdit': '修改',
    'teacherManagementPage_teacherClassManagementDelete': '删除',
    'teacherManagementPage_teacherClassManagementChangePassword': '更改密碼',
    'teacherManagementPage_teacherStudentManagementCreateStudent': '添加學生',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep1': '設置',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub': '特徵',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub2': '任務類型',
    'teacherCreateAssessmentPage_teacherCreateAss_TestTypeMockexam': '模擬考試',
    'teacherCreateAssessmentPage_teacherCreateAss_TestTypeTestbank': '題庫',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep2': '添加任務',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep3': '新建',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentName':
        '任務名稱',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentNamePlaceholder':
        '請輸入任務名稱',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsName': '選擇學生',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNamePlaceholder':
        '請選擇至少一位學生',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarning':
        '沒有任何學生',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameError':
        '請選擇至少一位學生',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDate': '開始日期',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDatePlaceholder':
        '請輸入作業開始時間',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooEarly':
        '開始時間要在現在的時間之後',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooLate':
        '開始日期不能在截止日期之後',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDate': '截止日期',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDateTooEarly':
        '截止日期必須在開始日期之後',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDatePlaceholder':
        '請輸入作業截止日期',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsInstruction':
        '任務說明（選填）',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsInstructionPlaceholder':
        '請輸入任務說明',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextmode': '任務模式',
    'teacherCreateAssessmentPage_teacherCreateAssTextModeExercise': '練習模式',
    'teacherCreateAssessmentPage_teacherCreateAssTextModeTest': '考試模式',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesText': '文字顯示',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextSimplified':
        '簡體中文',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextTraditional':
        '繁體中文',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudio': '音訊選項',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioMandarin': '中文',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioCantonese': '粵語',
    'teacherCreateAssessmentPage_teacherCreateAssTestsLeftTitle': '佈置任務',
    'teacherCreateAssessmentPage_teacherCreateAssTestsRightWarning': '沒有任何任務',
    'teacherCreateAssessmentPage_teacherCreateAssTestsError': '您沒有選擇任何習題!',
    'teacherCreateAssessmentPage_teacherCreateAssReviewEdit': '編輯',
    'teacherCreateAssessmentPage_teacherReviewTestQuit': '退出',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSkill': '技能',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONS_All': '全部',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSListening': '聽',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSpeaking': '說',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSReading': '讀',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSWriting': '寫',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Theme': '主題',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_All': '全部',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Global': '全球性挑戰',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Life': '現代生活',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Interdisciplinary':
        '科學技術',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Families': '家庭與社區',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Identities':
        '個人與社會認同',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Beauty': '美與美學',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_All': '全部',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Identities':
        '身份認同',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Experiences': '體驗',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Human': '人類發明與創造',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Social': '社會組織',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Sharing': '共享地球',
    'teacherGradePage_teacherGradeTestQuestionIndex': '主頁',
    'teacherGradePage_teacherGradeTestScore': '成績',
    'teacherGradePage_teacherGradeTestComment': '評論',
    'teacherGradePage_teacherGradeTestSpeakingWarning': '您的學生沒有提交答案!',
    'teacherReportPage_teacherReportTaskName': '任務名稱',
    'teacherProfile_teacherProfileChangePassword': '修改密碼',
    'teacherProfile_teacherProfileUpdate': '更改個人資料',
    'teacherProfile_teacherProfileUpdateSuccess': '個人資料更改成功',
    'teacherProfile_teacherProfileUpdateWarning': '不做任何更改嗎？',
    'teacherProfile_teacherProfileMaxStudent': '最多能容納的學生數量: {maxStudent}',
    'individualPage_individualHome': '主頁',
    'individualPage_individualHomeTitle': '主頁',
    'individualPage_individualHomeAplibTitle': '類別',
    'individualPage_individualHomeAprecordTitle': '學習記錄',
    'individualPage_individualHomeReportTitle': '學習報告',
    'individualPage_individualHomeProfileTitle': '帳戶設置',
    'individualPage_individualHomeOverview_1': '正在進行中的任務',
    'individualPage_individualHomeOverview_1Btn': '查看今天的任務',
    'individualPage_individualHomeOverview_2': '等待批改',
    'individualPage_individualHomeOverview_2Btn': '去批改作業',
    'individualPage_individualHomeOverview_3': '試題總覽',
    'individualPage_individualHomeOverview_4': '學生總數',
    'apTest_apLibpageContinue': '繼續',
    'apTest_apLibpageStart': '開始',
    'apTest_apLibpageRestart': '重新開始',
    'apTest_apLibpageReview': '檢查',
    'apTest_apLibpageNullAssessmentList': '您沒有任何任務!',
    'apTest_apLibpageNullList': '您沒有任何學習記錄!',
    'apTest_apModalTestmodalexercise': '練習',
    'apTest_apModalTestmodaltest': '考試',
    'apTest_apModalTestModalSet': '任務模式設定',
    'apTest_apModalTextLangSet': '文本語言設定',
    'apTest_apModalTextSimplify': '簡體中文',
    'apTest_apModalTextTraditional': '繁體中文',
    'apTest_apModalAudioLangSet': '音訊語言設置',
    'apTest_apModalAudioMandarin': '普通話',
    'apTest_apModalAudioCatonese': '廣東話',
    'apTest_apModalButtonStart': '開始任務',
    'apTest_apQListTitle': '列表',
    'apTest_apTestButtonSave': '存儲',
    'apTest_apTestButtonPrev': '上一步',
    'apTest_apTestButtonNext': '下一步',
    'apTest_apTestButtonSubmit': '提交',
    'apTest_apLibpageQuit': '退出',
    'apTest_apRecordFinalPageSentence': '最後一題!',
    'apTest_apTestFinalPageSentence': '恭喜您! 您做完了!',
    'apBank_apTestBankStart': '開始做題',
    'apBank_apTestBankTitle': 'AP 題庫',
    'apBank_apTestBankDescription': '您有一個尚未完成的任務。',
    'apRecording_apAudioTooltipsStart': '請開始錄製您的答案',
    'apRecording_apAudioTooltipsStop': '停止',
    'apRecording_apAudioTooltipsSave': '請按"存儲"鍵, 否則，您的錄音將會被刪除',
    'apRecording_apAudioTooltipsDelete': '您可以刪除，然後重新錄製',
    'apWriting_apWriteWordCount': '字數統計',
    'studentPage_studentHome': '主頁',
    'studentPage_studentHomeTitle': '主頁',
    'studentPage_studentHomeAssessmentTitle': '作業考試',
    'studentPage_studentHomeRecordTitle': '學習記錄',
    'studentPage_studentHomeReportTitle': '學習報告',
    'studentPage_studentPersonReportTitle': '學生報告',
    'studentPage_studentClassReportTitle': '班級報告',
    'studentPage_studentHomeProfileTitle': '帳戶設置',
    'studentPage_studentTestInstructionReminder': '老師的指示',
    'studentPage_studentTestInstruction': '任務說明',
    'modals_modalClose': '關閉',
    'forgotPassword_modalForgotPasswordTitle': '忘記密碼',
    'forgotPassword_modalForgotPasswordSuccess': '重置密碼的連結已發送至您的電子郵箱中.',
    'questionIndexModal_modalQuestionIndexTitle': '索引',
    'questionIndexModal_modalQuestionIndexClose': '關閉',
    'questionIndexModal_modalBankInfoTitle': '銀行資訊',
    'questionIndexModal_modalWaitTitle': '您的購買已經提交',
    'questionIndexModal_modalWaitContent':
        '付款審核完成後(通常需要24小時), 請使用您的報價單號和您的電子郵箱繼續完成帳號申請。',
    'createClass_modalCreateClassTitle': '創建班級',
    'createClass_modalCreateClassStudents': '學生',
    'createClass_modalCreateClassStudentsPlaceholder': '選擇學生',
    'createClass_modalCreateClassNoOptions': '沒有任何學生',
    'createClass_modalCreateClassSuccess': '班級已創建',
    'createClass_modalCreateClassKeys': '選擇鑰匙',
    'createClass_modalCreateClassNoKeys': '沒有鑰匙可用',
    'add_StudentToClass_modalAddStudentTitle': '編輯班級學生',
    'add_StudentToClass_modalAddStudentStudents': '學生',
    'add_StudentToClass_modalAddStudentNoOptions': '沒有任何學生',
    'add_StudentToClass_modalAddStudentTooShort': '最少選擇一位學生',
    'add_StudentToClass_modalAddStudentSuccess': '班級學生已修改',
    'createStudent_modalCreateStudentTitle': '創建學生',
    'createStudent_modalCreateStudentClassIdsLabel': '班級',
    'createStudent_modalCreateStudentClassIdsPlaceholder': '選擇班級',
    'createStudent_modalCreateStudentClassIdsWarning': '沒有班級被創建',
    'createStudent_modalCreateStudentSuccess': '學生帳號創建成功！',
    'createStudent_modalCreateStudentLicenseLabel': '帳號許可',
    'createStudent_modalCreateStudentLicenseWarning': '沒有剩餘的帳號許可可用',
    'createStudent_modalCreateStudentLicensePlaceholder': '分配一個帳號許可',
    'createStudent_modalCreateStudentDescription':
        '一個帳號許可一旦被分配，就不能再被修改。請點擊“確認”創建學生帳號。',
    'updateStudent_modalUpdateStudentTitle': '更新學生資料資訊',
    'updateStudent_modalUpdateStudentSuccess': '學生資料資訊更新成功！',
    'updateStudent_modalUpdateStudentLicenseLabel': '添加帳號許可',
    'updateStudent_modalUpdateStudentLicensePlaceholder': '添加帳號許可類型',
    'updateStudent_modalUpdateStudentLicenseWarning': '沒有剩餘的帳號許可可用',
    'changePassword_modalChangePasswordTitle': '更改密碼',
    'changePassword_modalChangePasswordSuccess': '密碼更改成功！',
    'alertDialog_modalDeleteClassTitle': '您確定要刪除班級嗎?',
    'alertDialog_modalDeleteClassDescription': '這個班級已不存在。 但是，學生不會被刪除。',
    'alertDialog_modalDeleteClassSuccess': '班級已被刪除',
    'alertDialog_modalDeleteStudentTitle': '您要刪除這位學生嗎?',
    'alertDialog_modalDeleteStudentDescription': '這位學生將永遠被刪除。',
    'alertDialog_modalDeleteStudentSuccess': '這位學生已經被刪除',
    'gradeExplian_modalGradeExplainTitle': '評分標準',
  };
  static const zh_CN = {
    'message_default': 'error',
    'message_msgFormRequired': '必填',
    'message_msgFormInvalidUsername': '这个用户名已经有人使用了!',
    'message_msgFormInvalidEmail': '这个邮箱地址无效',
    'message_msgFormUsedEmail': '这个邮箱已经有人使用了!',
    'message_msgFormNotSameEmail': '请确保两次电子邮箱输入一致',
    'message_msgFormLongPassword': '这个密码太长了!',
    'message_msgFormNotSamePassword': '请确保两次密码输入一致',
    'message_msgFormShortPassword': '密码最少要有6个字符',
    'message_msgFormSpecifyPassword': '密码必须包括最少1个数字号码和1个特殊符号',
    'message_msgFormNotSupportedFormat': '对不起，我们不支持这种文件格式',
    'formInfo_formClassName': '班级名称',
    'formInfo_formClassNamePlaceholder': '请输入班级名称',
    'formInfo_formFirstName': '名字',
    'formInfo_formFirstNamePlaceholder': '请输入您的名字',
    'formInfo_formLastName': '姓',
    'formInfo_formLastNamePlaceholder': '请输入您的姓',
    'formInfo_formTeacherEmail': '老师邮箱',
    'formInfo_formTeacherEmailPlaceholder': '如果您已经有老师，输入老师的电子邮件信箱',
    'formInfo_formEmail': '邮箱',
    'formInfo_formEmailPlaceholder': '请输入您的电子邮箱',
    'formInfo_formConfirmEmail': '确认电子邮箱',
    'formInfo_formConfirmEmailPlaceholder': '请重新输入您的电子邮箱',
    'formInfo_formPassword': '密码',
    'formInfo_formCurpassword': '原有密码',
    'formInfo_formNewpassword': '新的密码',
    'formInfo_formPasswordPlaceholder': '请输入密码',
    'formInfo_formCurpasswordPlaceholder': '请输入原有密码',
    'formInfo_formNewpasswordPlaceholder': '请输入新的密码',
    'formInfo_formConfirmPassword': '确认密码',
    'formInfo_formConfirmPasswordPlaceholder': '请再一次输入密码',
    'formInfo_formUserName': '用户名',
    'formInfo_formUserNamePlaceholder': '请输入您的姓名',
    'formInfo_formPhone': '电话号码',
    'formInfo_formPhonePlaceholder': '请输入您的电话号码',
    'formInfo_formCountry': '国家',
    'formInfo_formCountryPlaceholder': '请选择您的国家',
    'formInfo_formRegion': '区域',
    'formInfo_formRegionPlaceholder': '请选择您所在的地区',
    'formInfo_formZipCode': '邮递区号',
    'formInfo_formZipCodePlaceholder': '请输入您的邮递区号',
    'formInfo_formCancel': '取消',
    'formInfo_formSubmit': '提交',
    'formInfo_formNext': '下一步',
    'formInfo_formBack': '返回',
    'formInfo_formOk': '完成',
    'formInfo_formLoading': '载入中...',
    'formInfo_tableNoContent': '没有数据',
    'modal_modalAgree': '同意',
    'modal_modalDisagree': '不同意',
    'modal_modalSubmit': '提交',
    'modal_modalCancel': '取消',
    'headerInfo_headerHome': '主页',
    'headerInfo_headerSimulations': '模拟题集',
    'headerInfo_headerSimulationsIndividual': '个人',
    'headerInfo_headerSimulationsStudents': '班级',
    'headerInfo_headerSimulationsOverview': '概览',
    'headerInfo_headerQuestionBank': '题库',
    'headerInfo_headerQuestionBankIndividual': '个人',
    'headerInfo_headerQuestionBankStudents': '班级',
    'headerInfo_headerQuestionBankOverview': '概览',
    'headerInfo_headerPrice': '价格',
    'headerInfo_headerPriceIndividual': '个人',
    'headerInfo_headerPriceSchool': '班级',
    'headerInfo_headerComments': '评论',
    'headerInfo_headerQuestions': '问题',
    'headerInfo_headerBuy': '购买',
    'headerInfo_headerBuyIndividual': '个人',
    'headerInfo_headerBuySchool': '班级',
    'headerInfo_headerContact': '联系我们',
    'headerInfo_headerLogin': '登录',
    'headerInfo_headerLogout': '退出',
    'footerInfo_footerTerms': '隐私与条款',
    'loginPage_loginLoginName': '用户名',
    'loginPage_loginLoginNamePlaceholder': '请输入用户名',
    'loginPage_loginLogin': '登录',
    'loginPage_loginSignUp': '注册',
    'loginPage_loginForgetPassword': '忘记密码',
    'resetPassword_resetPasswordSuccess': '密码已重置!',
    'registerPage_registerTitle': '您是什么身份？',
    'registerPage_registerIndividual': '个人',
    'registerPage_registerTeacher': '老师',
    'registerPage_registerTeacherIndexQ1': '您有报价单号吗?',
    'registerPage_registerTeacherIndexA1': '有',
    'registerPage_registerTeacherIndexA2': '没有',
    'registerPage_registerTeacherIndexQ2QuoteNumber': '报价单号',
    'registerPage_registerTeacherIndexQ2QuoteNumberPlaceholder': '请输入您的报价单号',
    'registerPage_registerTeacherIndexQ2Email': '电子邮箱',
    'registerPage_registerTeacherIndexQ2EmailPlaceholder': '请输入电子邮箱',
    'registerPage_registerTeacherIndexQ2Submit': '查看状态',
    'registerPage_registerTeacherIndexQ2Pending': '您的付款已在处理当中，通常需要24小时的处理时间。',
    'registerPage_registerTeacherIndexQ2Success': '恭喜您! 您已经完成了购买！',
    'registerPage_registerTeacherQuoteNumber': '您的报价单号是:',
    'registerPage_registerTeacherQuoteNumberSubtitle': '请记住这组号码',
    'registerPage_registerTeacherQuoteNumberCopy': '复制',
    'registerPage_registerTeacherQuoteNumberCopied': '已经复制',
    'registerPage_registerTeacherQuoteTitle1': '索取报价',
    'registerPage_registerTeacherQuoteTitle2': '关于你',
    'registerPage_registerTeacherQuoteTitle3': '订单信息',
    'registerPage_registerTeacherQuoteNoOfStudent': '学生数量',
    'registerPage_registerTeacherQuoteNoOfStudentSubText': '请输入您要购买的学生账号数量',
    'registerPage_registerTeacherQuoteDuration': '使用期限',
    'registerPage_registerTeacherQuoteDurationOption1': '一年',
    'registerPage_registerTeacherQuoteDurationOption2': '二年',
    'registerPage_registerTeacherQuoteDurationOption3': '三年',
    'registerPage_registerTeacherQuoteSubtotal': '小计',
    'registerPage_registerTeacherQuoteUnitPrice': '单价',
    'registerPage_registerTeacherQuoteSubmit': '索取报价',
    'registerPage_registerTeacherQuoteBillFailed':
        '对不起，您的资料有错误，麻烦您重新提交，或者使用其他的付款方式。',
    'registerPage_registerTeacherQuoteBillPending': '您的支付正在审理中',
    'registerPage_registerTeacherQuoteBillSuccess': '恭喜您！ 您已经完成了账号申请！',
    'registerPage_registerTeacherAccountSubmit': '创建账号',
    'paymentIndividual_payIndCartTitle': '您的购物车',
    'paymentIndividual_payIndInfoTitle': '个人信息',
    'paymentIndividual_payIndBillingTitle': '信用卡持有人信息',
    'paymentIndividual_payIndRightTitle': '订单信息',
    'paymentIndividual_payIndRightBtn1': '选择这项产品',
    'paymentIndividual_payIndRightBtn2': '存储这条信息',
    'paymentIndividual_payIndRightBtn3': '使用此付款方式',
    'paymentIndividual_payIndRightBtn4': '创建一个账号',
    'paymentIndividual_payIndRightBtnWarning': '请先存储付款方式',
    'paymentIndividual_payIndRightTableH1': '产品',
    'paymentIndividual_payIndRightTableH2': '从',
    'paymentIndividual_payIndRightTableH3': '到',
    'paymentIndividual_payIndRightTableH4': '价格',
    'paymentIndividual_payIndRightTableSubTotal': '税前小计',
    'paymentIndividual_payIndRightTableTax': '预估税款',
    'paymentIndividual_payIndRightTableDiscount': '折扣码',
    'paymentIndividual_payIndRightTableTotal': '总计',
    'teacherBillPage_payTeacherMethod1': '信用卡',
    'teacherBillPage_payTeacherMethod2': '下订单',
    'teacherBillPage_payTeacherMethod3': '转账',
    'teacherBillPage_payTeacherPoNumber': 'PO 号码',
    'teacherBillPage_payTeacherPoNumberPlaceholder': '请输入 PO 号码',
    'teacherBillPage_payTeacherPoFile': 'PO 文件',
    'teacherBillPage_payTeacherPoFilePlaceholder': '请上传您的 PO 文件',
    'teacherBillPage_payTeacherBankName': '银行名',
    'teacherBillPage_payTeacherBankNamePlaceholder': '请输入银行名',
    'teacherBillPage_payTeacherTransactionFile': '交易文件',
    'teacherBillPage_payTeacherTransactionFilePlaceholder': '请上传交易文件',
    'teacherBillPage_payTeacherBankInfo': '我们的银行信息',
    'teacherBillPage_payTeacherRightTableH1': '产品',
    'teacherBillPage_payTeacherRightTableH2': '学生数量',
    'teacherBillPage_payTeacherRightTableH3': '使用期限',
    'teacherBillPage_payTeacherRightTableH4': '单价',
    'teacherBillPage_payTeacherAccountHeader': '创建您的账户',
    'successPage_paySuccessTitle': '恭喜您!',
    'successPage_paySuccessSubtitle': '您的账号已经建立。',
    'successPage_paySuccessUsername': '用户名',
    'successPage_paySuccessDescription1': '账号从 {from} 到 {to} 有效.',
    'successPage_paySuccessDescription2': '请检查您的邮件信箱，激活您的账户',
    'successPage_paySuccessLogin': '登录',
    'successPage_paySuccessFooter': '您若有任何问题，欢迎随时电邮至 {email}',
    'breadcrumb_breadcrumbSearchPlaceholder': '搜索',
    'teacherHeader_teacherHeaderProfile': '个人资料',
    'teacherHeader_teacherHeaderChangeLanguage': '语言切换',
    'teacherHeader_teacherHeaderLogout': '退出',
    'teacherMenuList_teacherMenu': '主页',
    'teacherMenuList_teacherMenuClassChosen': '选择班级',
    'teacherMenuList_teacherMenuClassChosenManagement': '管理',
    'teacherMenuList_teacherMenuClassChosenAssignment': '作业',
    'teacherMenuList_teacherMenuClassChosenReport': '报告',
    'teacherMenuList_teacherMenuMockexam': '模拟试题',
    'teacherMenuList_teacherMenuTestbank': '题库',
    'teacherMenuList_teacherMenu1': '模拟题集',
    'teacherMenuList_teacherMenu2': '作业考试',
    'teacherMenuList_teacherMenu3': '学习报告',
    'teacherMenuList_teacherMenu4': '班级管理',
    'teacherMenuList_teacherMenu5': '学生管理',
    'teacherMenuList_teacherMenu6': '账户设置',
    'teacherMenuList_teacherMenuLicense': '教师许可证',
    'teacherHome_teacherHomeStudentNo': '学生',
    'teacherHome_teacherHomeClassNo': '班级',
    'teacherHome_teahcerHomeUnactivatedKey': '未激活账号',
    'teacherHome_teahcerHomeUngradedTests': '等待评分',
    'teacherHome_teacherHomeGreetingsMorning': '早上好',
    'teacherHome_teacherHomeGreetingsAfternoon': '下午好',
    'teacherHome_teacherHomeGreetingsEvening': '晚上好',
    'teacherHome_teacherHomeClassSelectionLabel': '请选择您要查看的班级',
    'teacherHome_teacherHomeClassSelectionLabelWarning': '没有任何班级, 请先创建一个班级',
    'teacherHome_teacherHomeOverview_1': '正在进行中的任务',
    'teacherHome_teacherHomeOverview_1Btn': '查看今天的任务',
    'teacherHome_teacherHomeOverview_2': '等待评分的任务',
    'teacherHome_teacherHomeOverview_2Btn': '去批改作业',
    'teacherHome_teacherHomeOverview_3': '所有任务',
    'teacherHome_teacherHomeOverview_4': '所有学生',
    'teacherHome_teacherHomeOverview_5': '最多能容纳的学生数量',
    'teacherHome_teacherHomeShortcutTitle': '快捷方式',
    'teacherHome_teacherHomeShortcutOp_1': '创建任务',
    'teacherHome_teacherHomeShortcutOp_2': '添加学生',
    'teacherHome_teacherHomeShortcutOp_3': '任务详情',
    'teacherHome_teacherHomeShortcutOp_4': '个性化设置',
    'teacherHome_teacherHomeShortcutOp_5': '创建班级',
    'teacherHome_teacherHomeOngingTitle': '实时任务',
    'teacherHome_teacherHomeOngingData_1': '正在进行中的任务',
    'teacherHome_teacherHomeOngingData_2': '进行中',
    'teacherTestListPage_teacherTestListCategoryTitle': '类别',
    'teacherTestListPage_teacherTestListCategorySearchPlaceholder': '搜索',
    'teacherTestListPage_teacherTestListCategorySortMethod': '分类',
    'teacherTestListPage_teacherTestListCategorySortMethod_1': '标题从最低开始',
    'teacherTestListPage_teacherTestListCategorySortMethod_1Desc': '标题从最高开始',
    'teacherTestListPage_teacherTestListCategorySortMethod_2': '从最低阶开始',
    'teacherTestListPage_teacherTestListCategorySortMethod_2Desc': '从最高阶开始',
    'teacherTestListPage_teacherTestListAllTitle': '测验',
    'teacherTestListPage_teacherTestListAllBtnAssign': '布置任务',
    'teacherTestListPage_teacherTestListAllBtnReview': '检查',
    'teacherOpenQueuedPage_teacherOpenQueuedBtnCreate': '新建',
    'teacherOpenQueuedPage_teacherOpenQueuedBtnDelete': '删除',
    'teacherOpenQueuedPage_teacherAssessmentDeleteWarning': '请选择最少一项测验。',
    'teacherOpenQueuedPage_teacherAssessmentDeleteTitle': '您确定要删除选择的测验吗?',
    'teacherOpenQueuedPage_teacherAssessmentDeleteDescription':
        '选择的测验将被永久删除， 学生们将无法使用这些作业',
    'teacherManagementPage_teacherManagementAllStudentsTitle': '所有学生',
    'teacherManagementPage_teacherManagementClassesTitle': '学生在 {label}',
    'teacherManagementPage_teacherManagementAllClassesTitle': '所有班级',
    'teacherManagementPage_teacherManagementWarningNocontent': '这个班级没有任何学生。',
    'teacherManagementPage_teacherManagementWarningNoStudent':
        '没有学生, 请先创建一个学生账号。',
    'teacherManagementPage_teacherManagementIconBtnDeleteClass': '删除班级',
    'teacherManagementPage_teacherManagementIconBtnEditTitle': '编辑班级名称',
    'teacherManagementPage_teacherManagementIconBtnEditStudents':
        '增加或删除这个班级里的学生',
    'teacherManagementPage_teacherManagementIconBtnChangePassword': '更改密码',
    'teacherManagementPage_teacherManagementIconBtnDeleteForever': '永久删除此学生账号',
    'teacherManagementPage_teacherManagementIconBtnCreateStudent': '建立一个新学生账号',
    'teacherManagementPage_teacherManagementIconBtnDelete': '删除班级',
    'teacherManagementPage_teacherManagementIconBtnCreateClass': '建立一个新的班级',
    'teacherManagementPage_teacherClassManagementCreateClass': '新建班级',
    'teacherManagementPage_teacherClassManagementEdit': '修改',
    'teacherManagementPage_teacherClassManagementDelete': '删除',
    'teacherManagementPage_teacherClassManagementChangePassword': '更改密码',
    'teacherManagementPage_teacherStudentManagementCreateStudent': '添加学生',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep1': '设置',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub': '特征',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub2': '任务类型',
    'teacherCreateAssessmentPage_teacherCreateAss_TestTypeMockexam': '模拟考试',
    'teacherCreateAssessmentPage_teacherCreateAss_TestTypeTestbank': '题库',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep2': '添加任务',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep3': '新建',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentName':
        '任务名称',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentNamePlaceholder':
        '请输入任务名称',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsName': '选择学生',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNamePlaceholder':
        '请选择至少一位学生',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarning':
        '没有任何学生',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameError':
        '请选择至少一位学生',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDate': '开始日期',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDatePlaceholder':
        '请输入作业开始时间',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooEarly':
        '开始时间要在现在的时间之后',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooLate':
        '开始日期不能在截止日期之后',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDate': '截止日期',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDateTooEarly':
        '截止日期必须在开始日期之后',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDatePlaceholder':
        '请输入作业截止日期',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsInstruction':
        '任务说明（选填）',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsInstructionPlaceholder':
        '请输入任务说明',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextmode': '任务模式',
    'teacherCreateAssessmentPage_teacherCreateAssTextModeExercise': '练习模式',
    'teacherCreateAssessmentPage_teacherCreateAssTextModeTest': '考试模式',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesText': '文字显示',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextSimplified':
        '简体中文',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextTraditional':
        '繁体中文',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudio': '音频选项',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioMandarin': '中文',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioCantonese': '粤语',
    'teacherCreateAssessmentPage_teacherCreateAssTestsLeftTitle': '布置任务',
    'teacherCreateAssessmentPage_teacherCreateAssTestsRightWarning': '没有任何任务',
    'teacherCreateAssessmentPage_teacherCreateAssTestsError': '您没有选择任何习题!',
    'teacherCreateAssessmentPage_teacherCreateAssReviewEdit': '编辑',
    'teacherCreateAssessmentPage_teacherReviewTestQuit': '退出',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSkill': '技能',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONS_All': '全部',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSListening': '听',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSpeaking': '说',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSReading': '读',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSWriting': '写',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Theme': '主题',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_All': '全部',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Global': '全球性挑战',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Life': '现代生活',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Interdisciplinary':
        '科学技术',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Families': '家庭与社区',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Identities':
        '个人与社会认同',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Beauty': '美与美学',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_All': '全部',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Identities':
        '身份认同',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Experiences': '体验',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Human': '人类发明与创造',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Social': '社会组织',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Sharing': '共享地球',
    'teacherGradePage_teacherGradeTestQuestionIndex': '主页',
    'teacherGradePage_teacherGradeTestScore': '成绩',
    'teacherGradePage_teacherGradeTestComment': '评论',
    'teacherGradePage_teacherGradeTestSpeakingWarning': '您的学生没有提交答案!',
    'teacherReportPage_teacherReportTaskName': '任务名称',
    'teacherProfile_teacherProfileChangePassword': '修改密码',
    'teacherProfile_teacherProfileUpdate': '更改个人资料',
    'teacherProfile_teacherProfileUpdateSuccess': '个人资料更改成功',
    'teacherProfile_teacherProfileUpdateWarning': '不做任何更改吗？',
    'teacherProfile_teacherProfileMaxStudent': '最多能容纳的学生数量: {maxStudent}',
    'individualPage_individualHome': '主页',
    'individualPage_individualHomeTitle': '主页',
    'individualPage_individualHomeAplibTitle': '类别',
    'individualPage_individualHomeAprecordTitle': '学习记录',
    'individualPage_individualHomeReportTitle': '学习报告',
    'individualPage_individualHomeProfileTitle': '账户设置',
    'individualPage_individualHomeOverview_1': '正在进行中的任务',
    'individualPage_individualHomeOverview_1Btn': '查看今天的任务',
    'individualPage_individualHomeOverview_2': '等待批改',
    'individualPage_individualHomeOverview_2Btn': '去批改作业',
    'individualPage_individualHomeOverview_3': '试题总览',
    'individualPage_individualHomeOverview_4': '学生总数',
    'apTest_apLibpageContinue': '继续',
    'apTest_apLibpageStart': '开始',
    'apTest_apLibpageRestart': '重新开始',
    'apTest_apLibpageReview': '检查',
    'apTest_apLibpageNullAssessmentList': '您没有任何任务!',
    'apTest_apLibpageNullList': '您没有任何学习记录!',
    'apTest_apModalTestmodalexercise': '练习',
    'apTest_apModalTestmodaltest': '考试',
    'apTest_apModalTestModalSet': '任务模式设定',
    'apTest_apModalTextLangSet': '文本语言设定',
    'apTest_apModalTextSimplify': '简体中文',
    'apTest_apModalTextTraditional': '繁体中文',
    'apTest_apModalAudioLangSet': '音频语言设置',
    'apTest_apModalAudioMandarin': '普通话',
    'apTest_apModalAudioCatonese': '广东话',
    'apTest_apModalButtonStart': '开始任务',
    'apTest_apQListTitle': '列表',
    'apTest_apTestButtonSave': '存储',
    'apTest_apTestButtonPrev': '上一步',
    'apTest_apTestButtonNext': '下一步',
    'apTest_apTestButtonSubmit': '提交',
    'apTest_apLibpageQuit': '退出',
    'apTest_apRecordFinalPageSentence': '最后一题!',
    'apTest_apTestFinalPageSentence': '恭喜您! 您做完了!',
    'apTest_studentTestInstructionReminder': '教学建议',
    'apTest_studentTestInstruction': '建议',
    'apBank_apTestBankStart': '开始做题',
    'apBank_apTestBankTitle': 'AP 题库',
    'apBank_apTestBankDescription': '您有一个尚未完成的任务。',
    'apRecording_apAudioTooltipsStart': '请开始录制您的答案',
    'apRecording_apAudioTooltipsStop': '停止',
    'apRecording_apAudioTooltipsSave': '请按"存储"键, 否则，您的录音将会被删除',
    'apRecording_apAudioTooltipsDelete': '您可以删除，然后重新录制',
    'apWriting_apWriteWordCount': '字数统计',
    'studentPage_studentHome': '主页',
    'studentPage_studentHomeTitle': '主页',
    'studentPage_studentHomeAssessmentTitle': '作业考试',
    'studentPage_studentHomeRecordTitle': '学习记录',
    'studentPage_studentHomeReportTitle': '学习报告',
    'studentPage_studentPersonReportTitle': '学生报告',
    'studentPage_studentClassReportTitle': '班级报告',
    'studentPage_studentHomeProfileTitle': '账户设置',
    'studentPage_studentTestInstructionReminder': '老师的指示',
    'studentPage_studentTestInstruction': '任务说明',
    'modals_modalClose': '关闭',
    'forgotPassword_modalForgotPasswordTitle': '忘记密码',
    'forgotPassword_modalForgotPasswordSuccess': '重置密码的链接已发送至您的电子邮箱中.',
    'questionIndexModal_modalQuestionIndexTitle': '索引',
    'questionIndexModal_modalQuestionIndexClose': '关闭',
    'questionIndexModal_modalBankInfoTitle': '银行信息',
    'questionIndexModal_modalWaitTitle': '您的购买已经提交',
    'questionIndexModal_modalWaitContent':
        '付款审核完成后(通常需要24小时), 请使用您的报价单号和您的电子邮箱继续完成账号申请。',
    'createClass_modalCreateClassTitle': '创建班级',
    'createClass_modalCreateClassStudents': '学生',
    'createClass_modalCreateClassStudentsPlaceholder': '选择学生',
    'createClass_modalCreateClassNoOptions': '没有任何学生',
    'createClass_modalCreateClassSuccess': '班级已创建',
    'createClass_modalCreateClassKeys': '选择钥匙',
    'createClass_modalCreateClassNoKeys': '没有钥匙可用',
    'add_StudentToClass_modalAddStudentTitle': '编辑班级学生',
    'add_StudentToClass_modalAddStudentStudents': '学生',
    'add_StudentToClass_modalAddStudentNoOptions': '没有任何学生',
    'add_StudentToClass_modalAddStudentTooShort': '最少选择一位学生',
    'add_StudentToClass_modalAddStudentSuccess': '班级学生已修改',
    'createStudent_modalCreateStudentTitle': '创建学生',
    'createStudent_modalCreateStudentClassIdsLabel': '班级',
    'createStudent_modalCreateStudentClassIdsPlaceholder': '选择班级',
    'createStudent_modalCreateStudentClassIdsWarning': '没有班级被创建',
    'createStudent_modalCreateStudentSuccess': '学生账号创建成功！',
    'createStudent_modalCreateStudentLicenseLabel': '账号许可',
    'createStudent_modalCreateStudentLicenseWarning': '没有剩余的账号许可可用',
    'createStudent_modalCreateStudentLicensePlaceholder': '分配一个账号许可',
    'createStudent_modalCreateStudentDescription':
        '一个账号许可一旦被分配，就不能再被修改。请点击“确认”创建学生账号。',
    'updateStudent_modalUpdateStudentTitle': '更新学生资料信息',
    'updateStudent_modalUpdateStudentSuccess': '学生资料信息更新成功！',
    'updateStudent_modalUpdateStudentLicenseLabel': '添加账号许可',
    'updateStudent_modalUpdateStudentLicensePlaceholder': '添加账号许可类型',
    'updateStudent_modalUpdateStudentLicenseWarning': '没有剩余的账号许可可用',
    'changePassword_modalChangePasswordTitle': '更改密码',
    'changePassword_modalChangePasswordSuccess': '密码更改成功！',
    'alertDialog_modalDeleteClassTitle': '您确定要删除班级吗?',
    'alertDialog_modalDeleteClassDescription': '这个班级已不存在。 但是，学生不会被删除。',
    'alertDialog_modalDeleteClassSuccess': '班级已被删除',
    'alertDialog_modalDeleteStudentTitle': '您要删除这位学生吗?',
    'alertDialog_modalDeleteStudentDescription': '这位学生将永远被删除。',
    'alertDialog_modalDeleteStudentSuccess': '这位学生已经被删除',
    'gradeExplian_modalGradeExplainTitle': '评分标准',
    'icaNavigation_studyRecords': '学习回顾',
    'icaNavigation_assignments': '分配',
    'icaNavigation_profileAndSettings': '信息设置',
  };
  static const en_US = {
    'exceptions_defaultMessage': 'Error loading data, verify your internet!',
    'exceptions_internetMessage': 'Failed to connect to the server!',
    'exceptions_invalidTokenMessage': 'Invalid token!',
    'exceptions_timeOutMessage': 'Time out when trying to connect server!',
    'exceptions_userNotFoundMessage': 'User not found!',
    'util_success': 'Success',
    'util_warning': 'Warning',
    'util_error': 'Error',
    'services_auth_userNotFoundMessage': 'User and/or password wrongs!!',
    'initializer_unauthorizedAccess': 'Unauthorized access',
    'initializer_dueToInactivity':
        'Logged out due to inactivity, please login again!',
    'message_default': 'error',
    'message_msgFormRequired': 'Required.',
    'message_msgForm_InvalidUsername': 'Username exists.',
    'message_msgFormInvalidEmail': 'Invalid Email.',
    'message_msgFormUsedEmail': 'Email has been used.',
    'message_msgFormNotSameEmail': 'Email must be same.',
    'message_msgFormLongPassword': 'Password is too long.',
    'message_msgFormNotSamePassword': 'Password must be same.',
    'message_msgFormShortPassword': 'Password must have at least 6 characters.',
    'message_msgFormSpecifyPassword':
        'Password must contain at least 1 Number and 1 Special character.',
    'message_msgFormNotSupportedFormat': 'Unsupported File Format',
    'formInfo_formClassName': 'Class Name',
    'formInfo_formClassNamePlaceholder': 'Enter your class name',
    'formInfo_formFirstName': 'First Name',
    'formInfo_formFirstNamePlaceholder': 'Enter your first name',
    'formInfo_formLastName': 'Last Name',
    'formInfo_formLastNamePlaceholder': 'Enter your last name',
    'formInfo_formTeacherEmail': 'Teacher email',
    'formInfo_formTeacherEmailPlaceholder':
        'Enter your teacher email if you has teacher',
    'formInfo_formEmail': 'Email',
    'formInfo_formEmailPlaceholder': 'Enter your email',
    'formInfo_formConfirmEmail': 'Confirm Email',
    'formInfo_formConfirmEmailPlaceholder': 'Re-enter your email',
    'formInfo_formPassword': 'Password',
    'formInfo_formCurpassword': 'Current Password',
    'formInfo_formNewpassword': 'New Password',
    'formInfo_formPasswordPlaceholder': 'Enter your password',
    'formInfo_formCurpasswordPlaceholder': 'Enter your current password',
    'formInfo_formNewpasswordPlaceholder': 'Enter your new password',
    'formInfo_formConfirmPassword': 'Confirm Password',
    'formInfo_formConfirmPasswordPlaceholder': 'Re-enter password',
    'formInfo_formUserName': 'Username',
    'formInfo_formUserNamePlaceholder': 'Enter your username',
    'formInfo_formPhone': 'Phone',
    'formInfo_formPhonePlaceholder': 'Enter your phone number',
    'formInfo_formCountry': 'Country',
    'formInfo_formCountryPlaceholder': 'Select country',
    'formInfo_formRegion': 'Region',
    'formInfo_formRegionPlaceholder': 'Select region',
    'formInfo_formZipCode': 'Zip Code',
    'formInfo_formZipCodePlaceholder': 'Enter your zip code',
    'formInfo_formLicense': 'Current License',
    'formInfo_formLicensePlaceholder': 'Select license',
    'formInfo_formCancel': 'Cancel',
    'formInfo_formSubmit': 'Submit',
    'formInfo_formNext': 'Next',
    'formInfo_formBack': 'Back',
    'formInfo_formOk': 'Ok',
    'formInfo_formLoading': 'Loading...',
    'formInfo_tableNoContent': 'No data',
    'modal_modalAgree': 'Agree',
    'modal_modalDisagree': 'Disagree',
    'modal_modalSubmit': 'Submit',
    'modal_modalCancel': 'Cancel',
    'headerInfo_headerHome': 'Home',
    'headerInfo_headerSimulations': 'Simulations',
    'headerInfo_headerSimulationsIndividual': 'For Individual',
    'headerInfo_headerSimulationsStudents': 'For Students',
    'headerInfo_headerSimulationsOverview': 'Overview',
    'headerInfo_headerQuestionBank': 'Question Bank',
    'headerInfo_headerQuestionBankIndividual': 'For Individual',
    'headerInfo_headerQuestionBankStudents': 'For Students',
    'headerInfo_headerQuestionBankOverview': 'Overview',
    'headerInfo_headerPrice': 'Price',
    'headerInfo_headerPriceIndividual': 'Individual',
    'headerInfo_headerPriceSchool': 'School',
    'headerInfo_headerComments': 'Comments',
    'headerInfo_headerQuestions': 'Questions',
    'headerInfo_headerBuy': 'Buy',
    'headerInfo_headerBuyIndividual': 'Individual',
    'headerInfo_headerBuySchool': 'School',
    'headerInfo_headerContact': 'Contact Us',
    'headerInfo_headerLogin': 'Login',
    'headerInfo_headerLogout': 'Sign Out',
    'footerInfo_footerTerms': 'Terms of use and privacy policy',
    'loginPage_loginLoginName': 'Login Name',
    'loginPage_loginLoginNamePlaceholder': 'Enter login name',
    'loginPage_loginLogin': 'Login',
    'loginPage_loginSignUp': 'Sign Up',
    'loginPage_loginCheckQuote': 'Check Quote',
    'loginPage_loginForgetPassword': 'Forget Password',
    'resetPassword_resetPasswordSuccess':
        'Password has been successfully reset.',
    'registerPage_registerTitle': 'What is your role?',
    'registerPage_registerIndividual': 'Individual',
    'registerPage_registerTeacher': 'Teacher',
    'registerPage_registerTeacherIndexQ1': 'Do you have quote number?',
    'registerPage_registerTeacherIndexA1': 'Yes',
    'registerPage_registerTeacherIndexA2': 'No',
    'registerPage_registerTeacherIndexQ2QuoteNumber': 'Quote number',
    'registerPage_registerTeacherIndexQ2QuoteNumberPlaceholder':
        'Type your quote number',
    'registerPage_registerTeacherIndexQ2Email': 'Email',
    'registerPage_registerTeacherIndexQ2EmailPlaceholder':
        'Enter the email address',
    'registerPage_registerTeacherIndexQ2Submit': 'Check status',
    'registerPage_registerTeacherIndexQ2Cancel': 'Cancel',
    'registerPage_registerTeacherIndexQ2Pending':
        'Your payment is pending.It usually takes 24 hours to process.',
    'registerPage_registerTeacherIndexQ2Success':
        'Congratulations, you have already finished the applicaton process.',
    'registerPage_registerTeacherQuoteNumber': 'Your quote number is:',
    'registerPage_registerTeacherQuoteNumberSubtitle':
        'Please save this number',
    'registerPage_registerTeacherQuoteNumberCopy': 'Copy',
    'registerPage_registerTeacherQuoteNumberCopied': 'Copied',
    'registerPage_registerTeacherQuoteTitle1': 'Get a quote',
    'registerPage_registerTeacherQuoteTitle2': 'More about you',
    'registerPage_registerTeacherQuoteTitle3': 'Order summary',
    'registerPage_registerTeacherQuoteNoOfStudent': 'No. of Students',
    'registerPage_registerTeacherQuoteNoOfStudentSubText':
        'Type how many students account you want to purchase.',
    'registerPage_registerTeacherQuoteDuration': 'Duration',
    'registerPage_registerTeacherQuoteDurationOption1': '1 Year',
    'registerPage_registerTeacherQuoteDurationOption2': '2 Year',
    'registerPage_registerTeacherQuoteDurationOption3': '3 Year',
    'registerPage_registerTeacherQuoteSubtotal': 'Subtotal Price',
    'registerPage_registerTeacherQuoteUnitPrice': 'Unit Price',
    'registerPage_registerTeacherQuoteSubmit': 'GET QUOTE & EMAIL',
    'registerPage_registerTeacherQuoteBillFailed':
        'Your documents are declined. Please check and resubmit or you can chooose other payment methods.',
    'registerPage_registerTeacherQuoteBillPending':
        'Your payment is currently under review.',
    'registerPage_registerTeacherQuoteBillSuccess':
        'Congratulations, you have already finished the applicaton process.',
    'registerPage_registerTeacherAccountSubmit': 'Create an account',
    'paymentIndividual_payIndCartTitle': 'Your Cart',
    'paymentIndividual_payIndInfoTitle': 'Personal Info',
    'paymentIndividual_payIndBillingTitle': 'Billing Info',
    'paymentIndividual_payIndRightTitle': 'Order Summary',
    'paymentIndividual_payIndRightBtn1': 'Choose this product',
    'paymentIndividual_payIndRightBtn2': 'Confirm',
    'paymentIndividual_payIndRightBtn3': 'Confirm',
    'paymentIndividual_payIndRightBtn4': 'Place My Order ',
    'paymentIndividual_payIndRightBtnWarning': 'Save payment method first',
    'paymentIndividual_payIndRightTableH1': 'Product',
    'paymentIndividual_payIndRightTableH2': 'From',
    'paymentIndividual_payIndRightTableH3': 'To',
    'paymentIndividual_payIndRightTableH4': 'Price',
    'paymentIndividual_payIndRightTableSubTotal': 'Total Before Tax',
    'paymentIndividual_payIndRightTableTax': 'Estimated Tax To Be Collected',
    'paymentIndividual_payIndRightTableDiscount': 'Discount',
    'paymentIndividual_payIndRightTableTotal': 'Total',
    'teacherBillPage_payTeacherMethod1': 'Credit Card',
    'teacherBillPage_payTeacherMethod2': 'Purchase Order',
    'teacherBillPage_payTeacherMethod3': 'Wire Transfer',
    'teacherBillPage_payTeacherPoNumber': 'PO Number',
    'teacherBillPage_payTeacherPoNumberPlaceholder': 'Enter PO number',
    'teacherBillPage_payTeacherPoFile': 'PO File',
    'teacherBillPage_payTeacherPoFilePlaceholder': 'Upload your PO file',
    'teacherBillPage_payTeacherBankName': 'Bank Name',
    'teacherBillPage_payTeacherBankNamePlaceholder': 'Enter bank name',
    'teacherBillPage_payTeacherTransactionFile': 'Transaction File',
    'teacherBillPage_payTeacherTransactionFilePlaceholder':
        'Upload transaction file',
    'teacherBillPage_payTeacherBankInfo': 'OUR BANK INFO',
    'teacherBillPage_payTeacherRightTableH1': 'Product',
    'teacherBillPage_payTeacherRightTableH2': 'No. of Students',
    'teacherBillPage_payTeacherRightTableH3': 'Duration',
    'teacherBillPage_payTeacherRightTableH4': 'Unit Price',
    'teacherBillPage_payTeacherAccountHeader': 'Create your account',
    'successPage_paySuccessTitle': 'Congratulations!',
    'successPage_paySuccessSubtitle': 'Your account has been created.',
    'successPage_paySuccessUsername': 'Username',
    'successPage_paySuccessDescription1':
        'Account is valid from {from} to {to}.',
    'successPage_paySuccessDescription2':
        'Please check email to activate your account.',
    'successPage_paySuccessLogin': 'Login',
    'successPage_paySuccessFooter':
        'If you have any questions, please feel free to email {email},',
    'breadcrumb_breadcrumbSearchPlaceholder': 'Search',
    'teacherHeader_teacherHeaderProfile': 'Profile',
    'teacherHeader_teacherHeaderChangeLanguage': 'Change Language',
    'teacherHeader_teacherHeaderLogout': 'Logout',
    'teacherMenuList_teacherMenu': 'Home',
    'teacherMenuList_teacherMenuClassChosen': 'Choose Class',
    'teacherMenuList_teacherMenuClassChosenManagement': 'Management',
    'teacherMenuList_teacherMenuClassChosenAssignment': 'Assignments',
    'teacherMenuList_teacherMenuClassChosenReport': 'Report',
    'teacherMenuList_teacherMenuMockexam': 'Mock Exam',
    'teacherMenuList_teacherMenuTestbank': 'Test Bank',
    'teacherMenuList_teacherMenu1': 'Tests',
    'teacherMenuList_teacherMenu2': 'Assessment',
    'teacherMenuList_teacherMenu3': 'Report',
    'teacherMenuList_teacherMenu4': 'Class Management',
    'teacherMenuList_teacherMenu5': 'Student Management',
    'teacherMenuList_teacherMenu6': 'Profile and Settings',
    'teacherMenuList_teacherMenuLicense': 'License',
    'teacherHome_teacherHomeStudentNo': 'Students',
    'teacherHome_teacherHomeClassNo': 'Classes',
    'teacherHome_teahcerHomeUnactivatedKey': 'Unactivated Key',
    'teacherHome_teahcerHomeUngradedTests': 'Ungraded Tests',
    'teacherHome_teacherHomeGreetingsMorning': 'Good morning',
    'teacherHome_teacherHomeGreetingsAfternoon': 'Good afternoon',
    'teacherHome_teacherHomeGreetingsEvening': 'Good evening',
    'teacherHome_teacherHomeClassSelectionLabel': 'Select the class',
    'teacherHome_teacherHomeClassSelectionLabelWarning':
        'No classes, please create class first',
    'teacherHome_teacherHomeOverview_1': 'Live Tests',
    'teacherHome_teacherHomeOverview_1Btn': 'Check today\'s test',
    'teacherHome_teacherHomeOverview_2': 'Ungraded Tests',
    'teacherHome_teacherHomeOverview_2Btn': 'Go to correct',
    'teacherHome_teacherHomeOverview_3': 'Total tests',
    'teacherHome_teacherHomeOverview_4': 'Total students',
    'teacherHome_teacherHomeOverview_5': 'Max students',
    'teacherHome_teacherHomeShortcutTitle': 'Shortcuts',
    'teacherHome_teacherHomeShortcutOp_1': 'Create Test',
    'teacherHome_teacherHomeShortcutOp_2': 'Add Student',
    'teacherHome_teacherHomeShortcutOp_3': 'Test Details',
    'teacherHome_teacherHomeShortcutOp_4': 'Custom',
    'teacherHome_teacherHomeShortcutOp_5': 'Create Class',
    'teacherHome_teacherHomeOngingTitle': 'Onging test',
    'teacherHome_teacherHomeOngingData_1': 'Ongoing tests',
    'teacherHome_teacherHomeOngingData_2': 'In progress',
    'teacherTestListPage_teacherTestListCategoryTitle': 'Category',
    'teacherTestListPage_teacherTestListCategorySearchPlaceholder': 'Search',
    'teacherTestListPage_teacherTestListCategoryFilterMethod': 'Filter',
    'teacherTestListPage_teacherTestListCategorySortMethod': 'Sort',
    'teacherTestListPage_teacherTestListCategorySortMethod_1': 'Title from low',
    'teacherTestListPage_teacherTestListCategorySortMethod_1Desc':
        'Title from high',
    'teacherTestListPage_teacherTestListCategorySortMethod_2': 'Level from low',
    'teacherTestListPage_teacherTestListCategorySortMethod_2Desc':
        'Level from high',
    'teacherTestListPage_teacherTestListAllTitle': 'Tests',
    'teacherTestListPage_teacherTestListAllBtnAssign': 'Assign',
    'teacherTestListPage_teacherTestListAllBtnReview': 'Review',
    'teacherOpenQueuedPage_teacherOpenQueuedBtnCreate': 'create',
    'teacherOpenQueuedPage_teacherOpenQueuedBtnDelete': 'delete',
    'teacherOpenQueuedPage_teacherAssessmentDeleteWarning':
        'Select at least one assessment.',
    'teacherOpenQueuedPage_teacherAssessmentDeleteTitle':
        'Do you want to delete selected assessments?',
    'teacherOpenQueuedPage_teacherAssessmentDeleteDescription':
        'The selected assessments will be premanently deleted, students will no longer have access to these assessments.',
    'teacherManagementPage_teacherManagementAllStudentsTitle': 'All Students',
    'teacherManagementPage_teacherManagementClassesTitle':
        'Students in {label},',
    'teacherManagementPage_teacherManagementAllClassesTitle': 'All classes',
    'teacherManagementPage_teacherManagementWarningNocontent':
        'No students in this class',
    'teacherManagementPage_teacherManagementWarningNoStudent':
        'No students, please create one',
    'teacherManagementPage_teacherManagementIconBtnDeleteClass': 'Delete Class',
    'teacherManagementPage_teacherManagementIconBtnEditTitle':
        'Edit Class Name',
    'teacherManagementPage_teacherManagementIconBtnEditStudents':
        'Add or Remove Students In This Class',
    'teacherManagementPage_teacherManagementIconBtnChangePassword':
        'Change Password',
    'teacherManagementPage_teacherManagementIconBtnDeleteForever':
        'Permanently Delete Student',
    'teacherManagementPage_teacherManagementIconBtnCreateStudent':
        'Create A New Student',
    'teacherManagementPage_teacherManagementIconBtnDelete': 'Delete Class',
    'teacherManagementPage_teacherManagementIconBtnCreateClass':
        'Create A New Class',
    'teacherManagementPage_teacherClassManagementCreateClass': 'Create Class',
    'teacherManagementPage_teacherClassManagementEdit': 'Edit',
    'teacherManagementPage_teacherClassManagementDelete': 'Delete',
    'teacherManagementPage_teacherClassManagementChangePassword':
        'Change Password',
    'teacherManagementPage_teacherStudentManagementCreateStudent':
        'Create Student',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep1': 'Settings',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub': 'Features',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep1Sub2': 'Test Type',
    'teacherCreateAssessmentPage_teacherCreateAss_TestTypeMockexam':
        'Mock Exam',
    'teacherCreateAssessmentPage_teacherCreateAss_TestTypeTestbank':
        'Test Bank',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep2': 'Add tests',
    'teacherCreateAssessmentPage_teacherCreateAssStepperStep3': 'Create',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentName':
        'Assessment name',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentClassPlaceholder':
        'Enter class name',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentClass':
        'Class name',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsAssessmentNamePlaceholder':
        'Enter assessment name',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsClassId':
        'Select Class',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsClassIdPlaceholder':
        'Select Class',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsClassIdWarning':
        'No class available',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsName':
        'Select Students',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNamePlaceholder':
        'Select at least one student',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarning':
        'No students available',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarningMOCK_EXAM':
        'No students available for Mock Exam test',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameWarningTEST_BANK':
        'No students available for Test Bank test',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStudentsNameError':
        'Select at least one student',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDate':
        'Start Date',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDatePlaceholder':
        'Enter assignment start time',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooEarly':
        'No later than now',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsStartDateTooLate':
        'Start date is greater than due date',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDate': 'Due Date',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDateTooEarly':
        'Due date should be greater than start date',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsDueDatePlaceholder':
        'Enter assignment due time',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsInstruction':
        'Introduction',
    'teacherCreateAssessmentPage_teacherCreateAssSettingsInstructionPlaceholder':
        'Enter instruction',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextmode': 'Test Mode',
    'teacherCreateAssessmentPage_teacherCreateAssTextModeExercise': 'Practice',
    'teacherCreateAssessmentPage_teacherCreateAssTextModeTest': 'Test',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesText': 'Text',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextSimplified':
        'Simplified',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesTextTraditional':
        'Traditional',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudio': 'Audio',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioMandarin':
        'Mandarin',
    'teacherCreateAssessmentPage_teacherCreateAssFeaturesAudioCantonese':
        'Cantonese',
    'teacherCreateAssessmentPage_teacherCreateAssTestsLeftTitle':
        'Assigned tests',
    'teacherCreateAssessmentPage_teacherCreateAssTestsRightWarning':
        'No test found.',
    'teacherCreateAssessmentPage_teacherCreateAssTestsError':
        'No test is selected!',
    'teacherCreateAssessmentPage_teacherCreateAssReviewEdit': 'Edit',
    'teacherCreateAssessmentPage_teacherReviewTestQuit': 'Quit',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSkill': 'Skill',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONS_All': 'All',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSListening':
        'Listening',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSSpeaking':
        'Speaking',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSReading':
        'Reading',
    'teacherCreateAssessmentPage_teacher_TESTBANK_SKILL_OPTIONSWriting':
        'Writing',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Theme': 'Theme',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_All': 'All',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Global':
        'Global Issues and Challenges',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Life':
        'Contemporary Life',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Interdisciplinary':
        'Interdisciplinary/STEM',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Families':
        'Families and Communities/Social Relationships',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Identities':
        'Personal and Public Identities',
    'teacherCreateAssessmentPage_teacher_TESTBANK_AP_THEME_Beauty':
        'Beauty and Aesthetics',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_All': 'All',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Identities':
        'Identities',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Experiences':
        'Experiences',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Human':
        'Human Ingenuity',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Social':
        'Social Organization',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_Sharing':
        'Sharing the Planet',
    'teacherCreateAssessmentPage_teacher_TESTBANK_IBDP_THEME_WhoWeAre':
        'Who We Are',
    'teacherGradePage_teacherGradeTestQuestionIndex': 'Sheet',
    'teacherGradePage_teacherGradeTestScore': 'Score',
    'teacherGradePage_teacherGradeTestComment': 'Comment',
    'teacherGradePage_teacherGradeTestSpeakingWarning':
        'No answer was submitted!',
    'teacherReportPage_teacherReportTaskName': 'Task Name',
    'teacherProfile_teacherProfileChangePassword': 'Change Password',
    'teacherProfile_teacherProfileUpdate': 'Update Profile',
    'teacherProfile_teacherProfileUpdateSuccess': 'Profile updated',
    'teacherProfile_teacherProfileUpdateWarning': 'Nothing changed',
    'teacherProfile_teacherProfileMaxStudent':
        'The maximum student capacity: {maxStudent},',
    'individualPage_individualHome': 'Home',
    'individualPage_individualHomeTitle': 'Home',
    'individualPage_individualHomeAplibTitle': 'Tests',
    'individualPage_individualHomeAptestTitle': 'AP Question Bank',
    'individualPage_individualHomeApmockTitle': 'AP Simulated Tests',
    'individualPage_individualHomeIbmockTitle': 'IB Simulated Tests',
    'individualPage_individualHomeAprecordTitle': 'History',
    'individualPage_individualHomeReportTitle': 'Reports',
    'individualPage_individualHomeProfileTitle': 'Profile and Settings',
    'individualPage_individualHomeOverview_1': 'In progress tests',
    'individualPage_individualHomeOverview_1Btn': 'Check today\'s test',
    'individualPage_individualHomeOverview_2': 'Wait for correct tests',
    'individualPage_individualHomeOverview_2Btn': 'Go to correct',
    'individualPage_individualHomeOverview_3': 'Total tests',
    'individualPage_individualHomeOverview_4': 'Total students',
    'apTest_apLibpageContinue': 'Continue',
    'apTest_apLibpageStart': 'Start',
    'apTest_apLibpageRestart': 'Restart',
    'apTest_apLibpageReview': 'Review',
    'apTest_apLibpageNullAssessmentList': 'Oops... There is no any assessment!',
    'apTest_apLibpageNullList': 'Oops... There is no any test records!',
    'apTest_apModalTestmodalexercise': 'Practice',
    'apTest_apModalTestBankName': 'Title',
    'apTest_apModalTestmodaltest': 'Test',
    'apTest_apModalTestModalSet': 'Test Mode',
    'apTest_apModalTextLangSet': 'Test Language Setting',
    'apTest_apModalTextSimplify': 'Simplified',
    'apTest_apModalTextTraditional': 'Traditional',
    'apTest_apModalAudioLangSet': 'Audio Language Setting',
    'apTest_apModalAudioMandarin': 'Mandarin',
    'apTest_apModalAudioCatonese': 'Catonese',
    'apTest_apModalButtonStart': 'Start test',
    'apTest_apQListTitle': 'Sheet',
    'apTest_apTestButtonSave': 'Save',
    'apTest_apTestButtonPrev': 'Prev',
    'apTest_apTestButtonNext': 'Next',
    'apTest_apTestButtonSubmit': 'Submit',
    'apTest_apLibpageQuit': 'Quit',
    'apTest_apRecordFinalPageSentence': 'End of the test!',
    'apTest_apTestFinalPageSentence': 'Congratulation! You finish the test!',
    'apBank_apTestBankStart': 'Start Test',
    'apBank_apTestBankTitle': 'AP Test Bank',
    'apBank_apTestBankDescription': 'You have a unfinished test',
    'apRecording_apAudioTooltipsStart': 'start recording your response',
    'apRecording_apAudioTooltipsStop': 'stop',
    'apRecording_apAudioTooltipsSave':
        'Please click "save", otherwise your recording will be deleted',
    'apRecording_apAudioTooltipsDelete': 'you can delete it and record again',
    'apWriting_apWriteWordCount': 'Writing Words Count',
    'studentPage_studentHome': 'Home',
    'studentPage_studentHomeTitle': 'Home page',
    'studentPage_studentHomeAssessmentTitle': 'Assessment',
    'studentPage_studentHomeRecordTitle': 'History',
    'studentPage_studentHomeReportTitle': 'Reports',
    'studentPage_studentPersonReportTitle': 'Student reports',
    'studentPage_studentClassReportTitle': 'Class reports',
    'studentPage_studentHomeProfileTitle': 'Profile and Settings',
    'studentPage_studentTestInstructionReminder': 'Teacher\'s note',
    'studentPage_studentTestInstruction': 'Instruction',
    'modals_modalClose': 'Close',
    'forgotPassword_modalForgotPasswordTitle': 'Forgot pasword',
    'forgotPassword_modalForgotPasswordSuccess':
        'Reset password link has been sent to your email.',
    'questionIndexModal_modalQuestionIndexTitle': 'Index',
    'questionIndexModal_modalQuestionIndexClose': 'Close',
    'questionIndexModal_modalBankInfoTitle': 'Bank Info',
    'questionIndexModal_modalWaitTitle':
        'Your transaction file has been submitted',
    'questionIndexModal_modalWaitContent':
        'When the transaction is verified(usually takes 24 hours), please use your quote number and email to proceed the application.',
    'createClass_modalCreateClassTitle': 'Create Class',
    'createClass_modalCreateClassStudents': 'Students',
    'createClass_modalCreateClassStudentsPlaceholder': 'Select students',
    'createClass_modalCreateClassNoOptions': 'No students available',
    'createClass_modalCreateClassSuccess': 'Class created',
    'createClass_modalCreateClassKeys': 'Select keys',
    'createClass_modalCreateClassNoKeys': 'No keys available',
    'add_StudentToClass_modalAddStudentTitle': 'Edit Students In Class',
    'add_StudentToClass_modalAddStudentStudents': 'Students',
    'add_StudentToClass_modalAddStudentNoOptions': 'No students available',
    'add_StudentToClass_modalAddStudentTooShort': 'Select at leat one student',
    'add_StudentToClass_modalAddStudentSuccess':
        'Students in class is modified',
    'createStudent_modalCreateStudentTitle': 'Create Student',
    'createStudent_modalCreateStudentClassIdsLabel': 'Class',
    'createStudent_modalCreateStudentClassIdsPlaceholder': 'Select classes',
    'createStudent_modalCreateStudentClassIdsWarning': 'No class created',
    'createStudent_modalCreateStudentSuccess': 'Student created',
    'createStudent_modalCreateStudentLicenseLabel': 'License',
    'createStudent_modalCreateStudentLicenseWarning': 'No license remaining',
    'createStudent_modalCreateStudentLicensePlaceholder': 'Assign a license',
    'createStudent_modalCreateStudentDescription':
        'Once the licnese is assinged, it cannot be revised. Click confirm to create student.',
    'updateStudent_modalUpdateStudentTitle': 'Update Student',
    'updateStudent_modalUpdateStudentSuccess': 'Student\'s info updated.',
    'updateStudent_modalUpdateStudentLicenseLabel': 'Add License',
    'updateStudent_modalUpdateStudentLicensePlaceholder': 'Add a license type',
    'updateStudent_modalUpdateStudentLicenseWarning': 'No license remaining',
    'changePassword_modalChangePasswordTitle': 'Change Password',
    'changePassword_modalChangePasswordSuccess': 'Password updated.',
    'alertDialog_modalDeleteClassTitle': 'Do you want to delete class?',
    'alertDialog_modalDeleteClassDescription':
        'The class wil no logner exists, but students won\'t be deleted.',
    'alertDialog_modalDeleteClassSuccess': 'Class deleted',
    'alertDialog_modalDeleteStudentTitle':
        'Do you want to delete this student?',
    'alertDialog_modalDeleteStudentDescription':
        'The studenet will be permanently deleted.',
    'alertDialog_modalDeleteStudentSuccess': 'Student deleted',
    'gradeExplian_modalGradeExplainTitle': 'Score Guidlines',
    'addedAfter_apAudioTooltipsSpeed': 'Audio speed ',
    'addedAfter_apSubmitionTestText':
        'You still have some time to review or edit your recording before the test ends. Are you sure you want to submit now? Otherwise, this exam will be automatically submitted when the time is up.',
    'addedAfter_apSubmitionText':
        'Are you sure you want to submit the test now?',
    'addedAfter_apAudioStart': 'Click to start record',
    'icaNavigation_studyRecords': 'Study Records',
    'icaNavigation_assignments': 'Assignments',
    'icaNavigation_profileAndSettings': 'Profile and Settings',
  };
}
