import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/navigation/bindings/domains/assessments.repository.binding.dart';
import 'package:ichineseaplus/presentation/assessments/assessment_query_model.dart';
import 'package:ichineseaplus/presentation/assessments/mine_assessments_model.dart';

class AssessmentsController extends GetxController with StateMixin<dynamic> {
  final repository = AssessmentRepositoryBinding().repository;

  final List<Assessment> _assessments = <Assessment>[].obs;
  List<Assessment> get assessments => _assessments;

  void getMineAssessments(AssessmentQueryDto dto) async {
    var response = await repository.getAssessment(dto);
    var data = MineAssessments.fromJson(response);

    if (data.result != null) {
      _assessments.assignAll(data.result!.list as List<Assessment>);
      change(_assessments, status: RxStatus.success());
    } else {
      change(null, status: RxStatus.empty());
    }
  }

  @override
  void onClose() {}
}
