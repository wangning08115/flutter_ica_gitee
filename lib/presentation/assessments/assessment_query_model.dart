
import 'dart:convert';

AssessmentQueryDto assessmentQueryDtoFromJson(String str) => AssessmentQueryDto.fromJson(json.decode(str));

String assessmentQueryDtoToJson(AssessmentQueryDto data) => json.encode(data.toJson());

class AssessmentQueryDto {
    AssessmentQueryDto({
        this.query,
        this.status,
        this.type,
        this.kind,
    });

    String? query;
    List<String>? status;
    List<dynamic>? type;
    List<dynamic>? kind;

    factory AssessmentQueryDto.fromJson(Map<String, dynamic> json) => AssessmentQueryDto(
        query: json["query"],
        status: List<String>.from(json["status"].map((x) => x)),
        type: List<dynamic>.from(json["type"].map((x) => x)),
        kind: List<dynamic>.from(json["kind"].map((x) => x)),
    );

    Map<String, dynamic> toJson() => {
        "query": query,
        "status":status!=null ? List<dynamic>.from(status!.map((x) => x)): null,
        "type": type!=null ? List<dynamic>.from(type!.map((x) => x)): null,
        "kind":kind!=null ? List<dynamic>.from(kind!.map((x) => x)): null,
    };
}
