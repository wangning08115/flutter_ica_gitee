class MineAssessments {
  int? status;
  String? message;
  Result? result;

  MineAssessments({this.status, this.message, this.result});

  MineAssessments.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    result = json['result'] != null ? Result?.fromJson(json['result']) : null;
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['status'] = status;
    data['message'] = message;
    if (result != null) {
      data['result'] = result?.toJson();
    }
    return data;
  }
}

class Result {
  int? limit;
  int? count;
  int? page;
  List<Assessment>? list;

  Result({this.limit, this.count, this.page, this.list});

  Result.fromJson(Map<String, dynamic> json) {
    limit = json['limit'];
    count = json['count'];
    page = json['page'];
    if (json['list'] != null) {
      list = <Assessment>[];
      json['list'].forEach((v) {
        list?.add(Assessment.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['limit'] = limit;
    data['count'] = count;
    data['page'] = page;
    if (list != null) {
      data['list'] = list?.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Assessment {
  int? submittedTime;
  String? kind;
  String? audioType;
  int? progressRate;
  String? textType;
  String? className;
  String? type;
  int? questionNumber;
  String? mode;
  dynamic instruction;
  String? name;
  int? startTime;
  QuestionGroup? questionGroup;
  String? id;
  String? questionRecordId;
  int? answerNumber;
  int? endTime;
  int? correctRate;
  String? status;

  Assessment(
      {this.submittedTime,
      this.kind,
      this.audioType,
      this.progressRate,
      this.textType,
      this.className,
      this.type,
      this.questionNumber,
      this.mode,
      this.instruction,
      this.name,
      this.startTime,
      this.questionGroup,
      this.id,
      this.questionRecordId,
      this.answerNumber,
      this.endTime,
      this.correctRate,
      this.status});

  Assessment.fromJson(Map<String, dynamic> json) {
    submittedTime = json['submittedTime'];
    kind = json['kind'];
    audioType = json['audioType'];
    progressRate = json['progressRate'];
    textType = json['textType'];
    className = json['className'];
    type = json['type'];
    questionNumber = json['questionNumber'];
    mode = json['mode'];
    instruction = json['instruction'];
    name = json['name'];
    startTime = json['startTime'];
    questionGroup = json['questionGroup'] != null
        ? QuestionGroup?.fromJson(json['questionGroup'])
        : null;
    id = json['id'];
    questionRecordId = json['questionRecordId'];
    answerNumber = json['answerNumber'];
    endTime = json['endTime'];
    correctRate = json['correctRate'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['submittedTime'] = submittedTime;
    data['kind'] = kind;
    data['audioType'] = audioType;
    data['progressRate'] = progressRate;
    data['textType'] = textType;
    data['className'] = className;
    data['type'] = type;
    data['questionNumber'] = questionNumber;
    data['mode'] = mode;
    data['instruction'] = instruction;
    data['name'] = name;
    data['startTime'] = startTime;
    if (questionGroup != null) {
      data['questionGroup'] = questionGroup?.toJson();
    }
    data['id'] = id;
    data['questionRecordId'] = questionRecordId;
    data['answerNumber'] = answerNumber;
    data['endTime'] = endTime;
    data['correctRate'] = correctRate;
    data['status'] = status;
    return data;
  }
}

class QuestionGroup {
  dynamic imageLink;
  String? id;
  String? title;
  dynamic bookId;

  QuestionGroup({this.imageLink, this.id, this.title, this.bookId});

  QuestionGroup.fromJson(Map<String, dynamic> json) {
    imageLink = json['imageLink'];
    id = json['id'];
    title = json['title'];
    bookId = json['bookId'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['imageLink'] = imageLink;
    data['id'] = id;
    data['title'] = title;
    data['bookId'] = bookId;
    return data;
  }
}
