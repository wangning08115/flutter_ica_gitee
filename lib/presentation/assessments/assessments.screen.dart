import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichineseaplus/presentation/assessments/assessment_query_model.dart';
import 'package:ichineseaplus/presentation/shared/grids/assessmentsGrid.dart';
import 'controllers/assessments.controller.dart';

class AssessmentsScreen extends GetView<AssessmentsController> {
  const AssessmentsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final AssessmentsController controller = Get.put(AssessmentsController());
    AssessmentQueryDto dto =  AssessmentQueryDto();
    dto.status = ["NEW", "UNDONE"];
    controller.getMineAssessments(dto);

    return Scaffold(
      appBar: AppBar(
        title: const Text('My Assessments'),
        centerTitle: true,
      ),
      body: 
      Column(
        children: [
          controller.obx(
            (data) => Expanded(
                child: AssessmentsDataGrids(assesssments: controller.assessments, isStudyRecord: false,)),
            onEmpty: Center(child: Text('No assessment'.tr)),
            onLoading: const CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }
}
