export 'package:ichineseaplus/presentation/assessments/assessments.screen.dart';
export 'package:ichineseaplus/presentation/home/home.screen.dart';
export 'package:ichineseaplus/presentation/ica/ica.screen.dart';
export 'package:ichineseaplus/presentation/ica/login/login.screen.dart';
export 'package:ichineseaplus/presentation/ica/student/student.screen.dart';
export 'package:ichineseaplus/presentation/studyrecords/studyrecords.screen.dart';
export 'package:ichineseaplus/presentation/ica/student/assignment/assignment.screen.dart';