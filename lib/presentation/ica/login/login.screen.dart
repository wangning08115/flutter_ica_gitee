import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/presentation/shared/loading/base.widget.dart';
import 'package:logger/logger.dart';
import 'package:reactive_forms/reactive_forms.dart';

import 'controllers/login.controller.dart';

class LoginScreen extends GetView<LoginController> {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      child: Scaffold(
        appBar: AppBar(
          title: const Text('LoginScreen'),
          centerTitle: true,
        ),
        body: Center(
          child: ReactiveForm(
            formGroup: controller.loginForm.value,
            child: Column(
              children: [
                const Text("Login"),
                ReactiveTextField(
                  formControlName: 'loginName',
                ),
                ReactiveTextField(
                  formControlName: 'password',
                  obscureText: true,
                ),
                OutlinedButton(
                  child: const Text('Submit'),
                  onPressed: () async {
                    Logger().v(controller.loginForm.value.value);
                    await controller.login();
                    Logger().v("logined");
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
