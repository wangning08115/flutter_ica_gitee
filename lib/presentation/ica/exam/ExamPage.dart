import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/navigation/routes.dart';
import 'package:ichineseaplus/presentation/ica/exam/ExamPartPage.dart';
import 'package:ichineseaplus/presentation/ica/exam/ExamPartStartPromptPage.dart';
import 'package:ichineseaplus/presentation/ica/exam/ExamSectionPage.dart';
import 'package:ichineseaplus/presentation/ica/exam/util/ExamLayoutUtil.dart';

import 'controllers/ExamPageController.dart';

class ExamPage extends GetView<ExamPageController> {
  const ExamPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Exam'),
        centerTitle: true,
      ),
      body: _buildExam(),
    );
  }

  Widget _buildExam() {
    return Obx(() {
      int count = controller.count.value;
      return Container(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [_buildExamTop(), _buildExamMain(), _buildExamBottom()],
        ),
      );
    });
  }

  Widget _buildExamTop() {
    return Row(
      children: [
        Text('Question 1 of 15'),
        Expanded(child: SizedBox()),
        OutlinedButton(onPressed: () async {}, child: Text('INDEX'))
      ],
    );
  }

  Widget _buildExamMain() {
    Widget? examMain = null;
    examMain = ExamPartPage();

    var childExpanded = Container(
      padding: const EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 10),
      child: examMain,
    );
    var borderContainer = Container(
      decoration: ExamLayoutUtil.getBoxDecoration(),
      child: childExpanded,
      width: double.infinity, //代表百分百宽度
    );
    return Expanded(child: borderContainer);
  }

  Widget _buildExamBottom() {
    return Row(
      children: [
        // Text('第一题'),
        Expanded(child: SizedBox()),
        Row(
          children: [
            OutlinedButton(onPressed: () async {}, child: Text('PREV')),
            SizedBox(width: 10),
            OutlinedButton(onPressed: () async {}, child: Text('NEXT')),
          ],
        ),
      ],
    );
  }
}
