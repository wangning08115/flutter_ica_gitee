import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/navigation/routes.dart';
import 'package:ichineseaplus/presentation/ica/exam/ExamPartStartPromptPage.dart';
import 'package:ichineseaplus/presentation/ica/exam/ExamSectionPage.dart';

import 'controllers/ExamPageController.dart';

/**
 * Part 试卷章节， 最高级别的 章节
 */
class ExamPartPage extends GetView<ExamPageController> {
  const ExamPartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(0),
      child: _buildExamPartContent(),
    );
  }

  Widget _buildExamPartContent() {
    Widget partContent = ExamPartStartPromptPage();
    partContent = ExamSectionPage();
    return partContent;
  }
}
