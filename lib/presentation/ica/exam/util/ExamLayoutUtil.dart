
import 'package:flutter/material.dart';

class ExamLayoutUtil {
  static BoxDecoration getBoxDecoration() {
    return BoxDecoration(
      border: new Border.all(
        color: Colors.grey.withOpacity(0.2), //边框颜色
        width: 1, //边框宽度
      ), // 边色与边宽度
      color: Colors.white, // 底色
      boxShadow: [
        BoxShadow(
          blurRadius: 10, //阴影范围
          spreadRadius: 0.1, //阴影浓度
          color: Colors.grey.withOpacity(0.2), //阴影颜色
        ),
      ],
      borderRadius: BorderRadius.circular(10),
    );
  }
}
