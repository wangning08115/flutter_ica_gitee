import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/navigation/routes.dart';
import 'package:ichineseaplus/presentation/ica/exam/ExamQuestionPage.dart';
import 'package:ichineseaplus/presentation/ica/exam/util/ExamLayoutUtil.dart';

import 'controllers/ExamPageController.dart';

/**
 * Part 试卷章节， 最高级别的 章节
 */
class ExamTopicPage extends GetView<ExamPageController> {
  const ExamTopicPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: _buildExamTopicContent(),
    );
  }

  Widget _buildExamTopicContent() {
    Widget examQuestion = Column(
      children: [
        Text(
            '        京杭大运河是世界上最长的一条人工开凿的运河，北起北京，南到杭州，经北京、天津两直辖市及河北、山东、江苏、浙江四省，贯通海河、黄河、淮河、长江、钱塘江五大水系。运河全长约1750公里，是苏伊士运河长度的16倍，巴拿马运河的33倍。')
      ],
    );
    return examQuestion;
  }
}
//   @override
//   Widget build(BuildContext context) {
//     Widget mainContent = Container(
//       padding: const EdgeInsets.all(20),
//       child: Column(
//         children: [
//           Text(
//             '''Listening Part Directions:
//
// Listening Selections (15%, 10 minutes)
//
// You will listen to several selections in Chinese. For each selection, you will be told whether it will be played once or twice. You may take notes as you listen. Your notes will not be graded. After listening to each selection, you will see questions in English. For each question, choose the response that is best according to the selection. You will have 12 seconds to answer each question. ''',
//             style: TextStyle(fontSize: 20),
//             textAlign: TextAlign.left,
//           )
//         ],
//       ),
//     );
//
//   }
// }
