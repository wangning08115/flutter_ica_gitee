import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/navigation/routes.dart';

import 'controllers/ExamPageController.dart';

/**
 * Part 试卷章节， 最高级别的 章节
 */
class ExamQuestionPage extends GetView<ExamPageController> {
  ExamQuestionPage({Key? key}) : super(key: key);
  var answerRadioEdgeInsetsGeometry = EdgeInsets.symmetric(horizontal: 0);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: _buildExamQuestionContent(),
    );
  }

  Widget _buildExamQuestionContent() {
    int _radioGroupA = 1;
    Widget partContent = SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '1. How many provinces does the Great Jing-Hang Canal pass through?',
          ),
          SizedBox(height: 30),
          RadioListTile(
              value: 0,
              groupValue: _radioGroupA,
              onChanged: onOptionChanged,
              title: Text('A) Hong Kong University'),
              // subtitle: Text(''),
              //右侧图标
              selected: _radioGroupA == 0,
              contentPadding: answerRadioEdgeInsetsGeometry),
          RadioListTile(
              value: 1,
              groupValue: _radioGroupA,
              onChanged: onOptionChanged,
              title: Text('B) Chinese Soccer Association'),
              // subtitle: Text('Description'),
              //右侧图标
              selected: _radioGroupA == 1,
              contentPadding: answerRadioEdgeInsetsGeometry),
          RadioListTile(
              value: 2,
              groupValue: _radioGroupA,
              onChanged: onOptionChanged,
              title: Text('C) Chinese Soccer Team'),
              // subtitle: Text('Description'),
              //右侧图标
              selected: _radioGroupA == 2,
              contentPadding: answerRadioEdgeInsetsGeometry),
          RadioListTile(
              value: 3,
              groupValue: _radioGroupA,
              onChanged: onOptionChanged,
              title: Text('D) The School Student Union'),
              // subtitle: Text('Description'),
              //右侧图标
              selected: _radioGroupA == 3,
              contentPadding: answerRadioEdgeInsetsGeometry),
        ],
      ),
    );

    return partContent;
  }

  void onOptionChanged(Object? value) {}
}
