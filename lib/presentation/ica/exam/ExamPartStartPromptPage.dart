import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/navigation/routes.dart';
import 'package:ichineseaplus/presentation/ica/exam/util/ExamLayoutUtil.dart';

import 'controllers/ExamPageController.dart';

/**
 * Part 试卷章节， 最高级别的 章节
 */
class ExamPartStartPromptPage extends GetView<ExamPageController> {
  const ExamPartStartPromptPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget mainContent = Container(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Text(
            '''Listening Part Directions:
          
Listening Selections (15%, 10 minutes)

You will listen to several selections in Chinese. For each selection, you will be told whether it will be played once or twice. You may take notes as you listen. Your notes will not be graded. After listening to each selection, you will see questions in English. For each question, choose the response that is best according to the selection. You will have 12 seconds to answer each question. ''',
            style: TextStyle(fontSize: 20),
            textAlign: TextAlign.left,
          )
        ],
      ),
    );
    var borderContainer = Container(
      decoration: ExamLayoutUtil.getBoxDecoration(),
      child: mainContent,
      width: double.infinity, //代表百分百宽度
    );

    return borderContainer;
  }
}
