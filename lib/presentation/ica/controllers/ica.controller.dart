import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/navigation/bindings/domains/auth.repository.binding.dart';
import 'package:ichineseaplus/infrastructure/navigation/routes.dart';
import 'package:logger/logger.dart';

class IcaController extends GetxController {
  @override
  Future<void> onReady() async {
    final authRepositoryBinding = AuthRepositoryBinding();
    var repository = authRepositoryBinding.repository;
    var isAuthenticated = false; // await repository.isAuthenticated();

    if (isAuthenticated) {
      Logger().v("is Authenticated");
      Get.offNamed([Routes.ICA, Routes.STUDENT].join());
    } else {
      Logger().v("is not Authenticated");
      Get.offNamed([Routes.ICA, Routes.LOGIN].join());
    }
    Get.put(authRepositoryBinding);
    super.onReady();
  }
}
