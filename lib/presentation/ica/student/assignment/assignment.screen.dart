import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/domain/core/utils/Log.dart';
import 'package:ichineseaplus/presentation/ica/exam/ExamPage.dart';
import 'package:ichineseaplus/presentation/ica/exam/controllers/ExamPageController.dart';
import 'package:ichineseaplus/presentation/shared/loading/base.widget.dart';
import 'controllers/assignment.controller.dart';

class AssignmentScreen extends GetView<AssignmentController> {
  const AssignmentScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return BaseWidget(
      child: Obx(
        () {
          if (controller.loadingController.isLoading) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }

          var result = controller.assessmentMineList.value!;

          return ListView.builder(
            itemCount: result.count,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text(
                  result.list[index].questionGroup?.title ?? 'no questionGroup',
                ),
                onTap: () {
                  final data = result.list[index];
                  Log.debug('id=${data.id}');
                  Get.put(ExamPageController());
                  Get.to(() => ExamPage());
                },
              );
            },
          );
        },
      ),
    );
  }
}
