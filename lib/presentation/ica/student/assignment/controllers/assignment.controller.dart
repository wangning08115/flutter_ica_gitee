import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/dal/services/assessment/assessment.service.dart';
import 'package:ichineseaplus/infrastructure/dal/services/assessment/dto/assessment_mine_list.body.dart';
import 'package:ichineseaplus/infrastructure/dal/services/assessment/dto/assessment_mine_list.response.dart';
import 'package:ichineseaplus/presentation/shared/loading/loading.controller.dart';
import 'package:logger/logger.dart';

class AssignmentController extends GetxController {
  final AssessmentService _service;
  Rx<Result?> assessmentMineList =
      Rx<Result?>(Result(limit: 0, count: 0, page: 0, list: []));
  final loadingController = Get.find<LoadingController>();

  AssignmentController(this._service);

  @override
  Future<void> onInit() async {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    getAssessmentMineList();
  }

  Future<void> getAssessmentMineList() async {
    loadingController.isLoading = true;
    try {
      assessmentMineList.value = await _service.getAssessmentMineList(
        AssessmentMineListBody(
          limit: 8,
          assessmentViewForm: [],
          page: 0,
          sortField: 'endTime',
        ),
      );
    } catch (e) {
      Logger().e(e);
    } finally {
      loadingController.isLoading = false;
    }
  }
}
