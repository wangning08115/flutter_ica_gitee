import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichineseaplus/generated/locales.g.dart';

class StudentController extends GetxController {
  final navItemLabels = [
    LocaleKeys.icaNavigation_assignments,
    LocaleKeys.icaNavigation_studyRecords,
    LocaleKeys.icaNavigation_profileAndSettings,
  ];
  final navItemIcons = [Icons.abc, Icons.ssid_chart, Icons.ac_unit];
  int get navItemLength => navItemLabels.length;

  // final selectIndex = 0.obs;

  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
}
