import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'controllers/records.controller.dart';

class RecordsScreen extends GetView<RecordsController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('RecordsScreen'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'RecordsScreen is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
