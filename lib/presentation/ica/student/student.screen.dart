import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/navigation/routes.dart';
import 'package:ichineseaplus/presentation/ica/student/controllers/student.controller.dart';
import 'package:logger/logger.dart';


List<String> _routeNames = [
  [Routes.ICA, Routes.STUDENT].join(),
  [Routes.ICA, Routes.STUDENT, Routes.RECORDS].join(),
  [Routes.ICA, Routes.STUDENT, Routes.PROFILE].join(),
  // this is only using GetRouterOutlet.initialRoute
  [Routes.ICA, Routes.STUDENT, Routes.ASSIGNMENT].join(),
];

class StudentScreen extends GetView {
  const StudentScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetRouterOutlet.builder(
      builder: (context, delegate, currentRoute) {
        final currentLocation = currentRoute?.location;
        var currentIndex = 0;
        if (currentLocation != null &&
            currentLocation.startsWith(_routeNames[0])) {
          if (currentLocation.startsWith(_routeNames[1])) {
            currentIndex = 1;
          }
          if (currentLocation.startsWith(_routeNames[2])) {
            currentIndex = 2;
          }
        }
        return StudentView(currentIndex: currentIndex, delegate: delegate);
      },
      routerDelegate: GetDelegate(),
    );
  }
}

class StudentView extends GetResponsiveView<StudentController> {
  StudentView({
    Key? key,
    required this.currentIndex,
    required this.delegate,
  }) : super(key: key);

  final int currentIndex;
  final GetDelegate delegate;

  @override
  Widget? desktop() {
    return Scaffold(
      appBar: AppBar(
        title: const Text('StudentScreen'),
        centerTitle: true,
        actions: const [
          _LanguageSwitchButton(),
        ],
      ),
      body: Row(
        children: [
          SizedBox(
            width: 360.0,
            child: ListView(
              children: [
                for (var i = 0; i < controller.navItemLength; i++)
                  ListTile(
                    leading: Icon(controller.navItemIcons[i]),
                    title: Text(controller.navItemLabels[i].tr),
                    selected: currentIndex == i,
                    onTap: () {
                      delegate.toNamed(_routeNames[i]);
                    },
                  ),
              ],
            ),
          ),
          const VerticalDivider(width: 1, thickness: 1),
          Expanded(
            child: _StudentScreenChild(anchorRoute: _routeNames[currentIndex]),
          ),
        ],
      ),
    );
  }

  @override
  Widget? tablet() {
    return phone();
  }

  @override
  Widget? phone() {
    return Scaffold(
      appBar: AppBar(
        title: const Text('StudentScreen'),
        centerTitle: true,
        actions: const [
          _LanguageSwitchButton(),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            const DrawerHeader(child: Text('LOGO')),
            for (var i = 0; i < controller.navItemLength; i++)
              ListTile(
                leading: Icon(controller.navItemIcons[i]),
                title: Text(controller.navItemLabels[i].tr),
                selected: currentIndex == i,
                onTap: () {
                  delegate.toNamed(_routeNames[i]);
                },
              ),
          ],
        ),
      ),
      body: GetRouterOutlet(
        initialRoute: _routeNames[3],
        anchorRoute: _routeNames[currentIndex],
        key: Get.nestedKey(_routeNames[currentIndex]),
      ),
    );
  }
}

class _LanguageSwitchButton extends StatelessWidget {
  const _LanguageSwitchButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      itemBuilder: (BuildContext context) => [
        for (var item in Get.translations.entries)
          PopupMenuItem(
            child: Text(item.key.replaceAll('_', '-')),
            onTap: () async {
              await Get.updateLocale(
                Locale(
                  item.key.split('_')[0],
                  item.key.split('_')[1],
                ),
              );
              Logger().d("Locale is ${Get.locale}");
            },
          )
      ],
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(Icons.language),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              Get.locale!.toLanguageTag(),
              style: Get.textTheme.button,
            ),
          ),
        ],
      ),
    );
  }
}

class _StudentScreenChild extends StatelessWidget {
  const _StudentScreenChild({
    Key? key,
    required this.anchorRoute,
  }) : super(key: key);

  final String anchorRoute;

  @override
  Widget build(BuildContext context) {
    return GetRouterOutlet(
      initialRoute: _routeNames[3],
      anchorRoute: anchorRoute,
    );
  }
}
