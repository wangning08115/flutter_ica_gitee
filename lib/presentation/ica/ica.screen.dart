import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'controllers/ica.controller.dart';

class IcaScreen extends GetView<IcaController> {
  const IcaScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('IcaScreen'),
        centerTitle: true,
      ),
      body: Center(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: const [
            CircularProgressIndicator(),
            Text(
              'Page jumping...',
              style: TextStyle(fontSize: 20),
            ),
          ],
        ),
      ),
    );
  }
}
