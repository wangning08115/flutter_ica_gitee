import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/navigation/routes.dart';

import 'controllers/home.controller.dart';

class HomeScreen extends GetView<HomeController> {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('HomeScreen'),
        centerTitle: true,
        actions: [
          for (var actionName in ["Home", "Programs", "Help Center", "Pricing"])
            Center(
              child: TextButton(
                onPressed: () {},
                child: Text(actionName),
              ),
            ),
          Center(
            child: ElevatedButton(
              onPressed: () {
                Get.toNamed([Routes.ICA, Routes.LOGIN].join());
              },
              child: const Text("Login"),
            ),
          ),
          const SizedBox(width: 16.0),
        ],
      ),
      body: const Center(
        child: 
        Text(
          'HomeScreen is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
