import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichineseaplus/presentation/assessments/assessment_query_model.dart';
import 'package:ichineseaplus/presentation/shared/grids/assessmentsGrid.dart';
import 'package:ichineseaplus/presentation/studyrecords/controllers/studyrecords.controller.dart';

class StudyrecordsScreen extends GetView<StudyrecordsController> {
  const StudyrecordsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final StudyrecordsController controller = Get.put(StudyrecordsController());

    AssessmentQueryDto dto = AssessmentQueryDto();
    dto.status = ["SUBMITTED", "PROCESSING", "COMPLETED"];
    controller.getMineAssessmentRecords(dto);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Study Records'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          controller.obx(
            (data) => Expanded(
                child: AssessmentsDataGrids(
              assesssments: controller.assessments,
              isStudyRecord: true,
            )),
            onEmpty: Center(child: Text('No assessment'.tr)),
            onLoading: const CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }
}
