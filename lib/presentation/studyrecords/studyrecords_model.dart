import 'dart:convert';

Studyrecords studyrecordsFromJson(String str) =>
    Studyrecords.fromJson(json.decode(str));
String studyrecordsToJson(Studyrecords data) => json.encode(data.toJson());

class Studyrecords {
  Studyrecords({
    this.status,
    this.message,
    this.result,
  });

  int? status;
  String? message;
  Result? result;

  factory Studyrecords.fromJson(Map<String, dynamic> json) => Studyrecords(
        status: json["status"],
        message: json["message"],
        result: Result.fromJson(json["result"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "message": message,
        "result": result?.toJson(),
      };
}

class Result {
  Result({
    this.mode,
    this.recordStatus,
    this.commitTime,
    this.progressStatus,
    this.list,
  });

  String? mode;
  String? recordStatus;
  int? commitTime;
  dynamic progressStatus;
  List<ListElement>? list;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        mode: json["mode"],
        recordStatus: json["recordStatus"],
        commitTime: json["commitTime"],
        progressStatus: json["progressStatus"],
        list: List<ListElement>.from(
            json["list"].map((x) => ListElement.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "mode": mode,
        "recordStatus": recordStatus,
        "commitTime": commitTime,
        "progressStatus": progressStatus,
        "list": List<dynamic>.from(list!.map((x) => x.toJson())),
      };
}

class ListElement {
  ListElement({
    this.questions,
    this.introduction,
    this.partType,
  });

  List<ListQuestion>? questions;
  String? introduction;
  String? partType;

  factory ListElement.fromJson(Map<String, dynamic> json) => ListElement(
        questions: List<ListQuestion>.from(
            json["questions"].map((x) => ListQuestion.fromJson(x))),
        introduction: json["introduction"],
        partType: json["partType"],
      );

  Map<String, dynamic> toJson() => {
        "questions": List<dynamic>.from(questions!.map((x) => x.toJson())),
        "introduction": introduction,
        "partType": partType,
      };
}

class ListQuestion {
  ListQuestion({
    this.limitTime,
    this.question,
    this.id,
    this.content,
  });

  int? limitTime;
  List<Question>? question;
  String? id;
  Content? content;

  factory ListQuestion.fromJson(Map<String, dynamic> json) => ListQuestion(
        limitTime: json["limitTime"],
        question: List<Question>.from(
            json["question"].map((x) => Question.fromJson(x))),
        id: json["id"],
        content: Content.fromJson(json["content"]),
      );

  Map<String, dynamic> toJson() => {
        "limitTime": limitTime,
        "question": List<dynamic>.from(question!.map((x) => x.toJson())),
        "id": id,
        "content": content?.toJson(),
      };
}

class Content {
  Content({
    this.show,
    this.text,
    this.audio,
  });

  String? show;
  String? text;
  String? audio;

  factory Content.fromJson(Map<String, dynamic> json) => Content(
        show: json["show"],
        text: json["text"],
        audio: json["audio"],
      );

  Map<String, dynamic> toJson() => {
        "show": show,
        "text": text,
        "audio": audio,
      };
}

class Question {
  Question({
    this.head,
    this.limitTime,
    this.reference,
    this.number,
    this.answerCount,
    this.answer,
    this.solution,
    this.type,
    this.body,
    this.point,
  });

  Content? head;
  int? limitTime;
  Content? reference;
  int? number;
  int? answerCount;
  List<String>? answer;
  List<String>? solution;
  String? type;
  List<List<Body>>? body;
  int? point;

  factory Question.fromJson(Map<String, dynamic> json) => Question(
        head: Content.fromJson(json["head"]),
        limitTime: json["limitTime"],
        reference: Content.fromJson(json["reference"]),
        number: json["number"],
        answerCount: json["answerCount"],
        answer: List<String>.from(json["answer"].map((x) => x)),
        solution: List<String>.from(json["solution"].map((x) => x)),
        type: json["type"],
        body: List<List<Body>>.from(json["body"]
            .map((x) => List<Body>.from(x.map((x) => Body.fromJson(x))))),
        point: json["point"],
      );

  Map<String, dynamic> toJson() => {
        "head": head?.toJson(),
        "limitTime": limitTime,
        "reference": reference?.toJson(),
        "number": number,
        "answerCount": answerCount,
        "answer": List<dynamic>.from(answer!.map((x) => x)),
        "solution": List<dynamic>.from(solution!.map((x) => x)),
        "type": type,
        "body": List<dynamic>.from(
            body!.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
        "point": point,
      };
}

class Body {
  Body({
    this.baseValue,
    this.show,
    this.text,
  });

  String? baseValue;
  String? show;
  String? text;

  factory Body.fromJson(Map<String, dynamic> json) => Body(
        baseValue: json["baseValue"],
        show: json["show"],
        text: json["text"],
      );

  Map<String, dynamic> toJson() => {
        "baseValue": baseValue,
        "show": show,
        "text": text,
      };
}
