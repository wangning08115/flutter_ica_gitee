import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:ichineseaplus/generated/locales.g.dart';

class SnackbarUtil {
  static void showSuccess({required String message}) {
    Get.rawSnackbar(
      title: LocaleKeys.util_success.tr,
      icon: const Icon(Icons.thumb_up, color: Colors.white),
      message: message,
      backgroundColor: Colors.green.shade600,
    );
  }

  static void showWarning({required String message}) {
    Get.rawSnackbar(
      title: LocaleKeys.util_warning,
      icon: const Icon(Icons.warning, color: Colors.white),
      message: message,
      backgroundColor: Colors.orange.shade900,
    );
  }

  static void showError({required String message}) {
    Get.rawSnackbar(
      title: LocaleKeys.util_error,
      icon: const Icon(Icons.error, color: Colors.white),
      message: message,
      backgroundColor: Colors.redAccent.shade700,
    );
  }
}
