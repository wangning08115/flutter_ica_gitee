import 'package:date_format/date_format.dart';
import 'dart:developer';

class Log {
  static void debug(Object? object, {bool showFullLog = false}) {
    String now = formatDate(new DateTime.now(), [HH, ':', nn, ':', ss]);
    if (showFullLog)
      log('[$now] $object');
    else
      print('[$now] $object');
  }

  static String timeFlag() {
    String now = formatDate(new DateTime.now(), [HH, nn, ss, SSS]);
    return '[FLAG>>$now]';
  }
}
