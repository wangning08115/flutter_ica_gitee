abstract class RequestHeaderConstants {
  /// request header name
  static const String authTokenRequestHeader = 'authtoken';
}
