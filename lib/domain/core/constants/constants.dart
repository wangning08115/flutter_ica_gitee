//////// api addresses
// https://superbe.ichinesereader.com/superadmin/swagger-ui.html
// https://api.ichinesereader.com/studentmanager/swagger-ui.html

// ignore_for_file: constant_identifier_names

class Constants {
//////////// for production
  static const String BaseAPI = 'https://api.ichineseaplus.com/';
  static const String BaseAPISuper = 'https://superbe.ichinesereader.com/';
  static const String BaseURL = 'https://ichineseaplus.com/';
  static const String BaseDomain = 'ichineseaplus.com';

// ////////// for migration
//   static const String BaseAPI = 'https://api.migration.ichineseaplus.com/';
//   static const String BaseAPISuper =
//       'https://superbe.migration.ichinesereader.com/';
//   static const String BaseURL = 'https://migration.ichineseaplus.com/';
//   static const String BaseDomain = 'migration.ichineseaplus.com';

//book resource base url
  static const String BaseResources = 'https://resources.ichinesereader.com/';

  static const Map<String, String> NanhaiRoles = {
    'student': 'STUDENT',
    'teacher': 'TEACHER',
    'individual': 'INDIVIDUAL',
  };

  static const String NanhaiRole_Student = 'STUDENT';
  static const String NanhaiRole_Teacher = 'TEACHER';
  static const String NanhaiRole_Individual = 'INDIVIDUAL';

  static const String AssessmentType_Placement = 'PRE_TEST';
  static const String AssessmentType_AP = 'AP';
  static const String AssessmentType_Running_Record = 'RUNNING_RECORD';
  static const String AssessmentType_Customize = 'CUSTOMIZE';
  static const String AssessmentType_IBSpeaking = 'IB SPEAKING';

//   static List<AssessmentBookType> assessmentTypes = [
//     AssessmentBookType(
//         displayName: 'RUNNING_RECORD',
//         orderNumber: 0,
//         assessmentType: 'RUNNING_RECORD'),
//     AssessmentBookType(
//         displayName: 'IB_SPEAKING',
//         orderNumber: 1,
//         assessmentType: 'IBDP_AB,IBDP_BSL,IBDP_BHL'),
//     AssessmentBookType(
//         displayName: 'BENCHMARK_TEST',
//         orderNumber: 2,
//         assessmentType: 'BENCHMARK_TEST'),
// // AssessmentBookType(displayName: 'PLACEMENT_TEST', orderNumber: 4, assessmentType: 'PRE_TEST'),
// // AssessmentBookType(displayName: 'BENCHMARK_BOOK', orderNumber: 3, assessmentType: 'BENCHMARK_BOOK'),
//   ];

  static List<String> assessmentKinds = [
    'LISTENING',
    'SPEAKING',
    'READING',
    'WRITING',
    'TEST',
    'CUSTOMIZE',
  ];

  // static List<NanhaiLocale> nanhaiLocales = [
  //   NanhaiLocale(
  //       order: 0, key: 'en', displayName: 'ENG', locale: Locale('en', 'US')),
  //   NanhaiLocale(
  //       order: 1, key: 'cn', displayName: '简体', locale: Locale('zh', 'CN')),
  //   NanhaiLocale(
  //       order: 2, key: 'tc', displayName: '繁体', locale: Locale('zhHant', 'TW')),
  //   NanhaiLocale(
  //       order: 3, key: 'ko', displayName: '한국인', locale: Locale('ko', 'KO'))
  // ];

  static const String Mandarin = "Mandarin";
  static const String Cantonese = "Cantonese";
  static const String Simplified = "Simplified";
  static const String Traditional = "Traditional";

  // static List<SelectOption> audioOptions = [
  //   SelectOption(value: Mandarin, label: Mandarin),
  //   SelectOption(value: Cantonese, label: Cantonese),
  //   SelectOption(value: "No_audio", label: "No Audio"),
  // ];

  // static List<SelectOption> textOptions = [
  //   SelectOption(value: Simplified, label: Simplified),
  //   SelectOption(value: Traditional, label: Traditional),
  //   SelectOption(value: "No text", label: "No Text"),
  // ];

  static const String LevelColorJson = '''
[{"value":"level1","label":"Level 1","color":"#d91c2a"},
{"value":"level2","label":"Level 2","color":"#dc3e2c"},
{"value":"level3","label":"Level 3","color":"#de5a2a"},
{"value":"level4","label":"Level 4","color":"#f5831f"},
{"value":"level5","label":"Level 5","color":"#f89c1b"},
{"value":"level6","label":"Level 6","color":"#f7ba1a"},
{"value":"level7","label":"Level 7","color":"#ffe600"},
{"value":"level8","label":"Level 8","color":"#e4e11b"},
{"value":"level9","label":"Level 9","color":"#bcd530"},
{"value":"level10","label":"Level 10","color":"#8dc43e"},
{"value":"level11","label":"Level 11","color":"#57b947"},
{"value":"level12","label":"Level 12","color":"#35a86d"},
{"value":"level13","label":"Level 13","color":"#009991"},
{"value":"level14","label":"Level 14","color":"#0089a3"},
{"value":"level15","label":"Level 15","color":"#0073ae"},
{"value":"level16","label":"Level 16","color":"#3d63ae"},
{"value":"level17","label":"Level 17","color":"#64469c"},
{"value":"level18","label":"Level 18","color":"#7f4098"},
{"value":"level19","label":"Level 19","color":"#9d3c95"},
{"value":"level20","label":"Level 20","color":"#bd3c79"},
{"value":"level0","label":"Level 0","color":"#808080"}]
''';

  ///time zones
  static const String TimeZoneJson = '''
["Eniwetok, Kwajalein(GMT -12:00) ",
"Midway Island, Samoa(GMT -11:00) ",
"Hawaii(GMT -10:00) ","Alaska(GMT -9:00) ",
"Pacific Time(GMT -8:00) ",
"Mountain Time(GMT -7:00) ",
"Central Time(GMT -6:00) ",
"Eastern Time(GMT -5:00) ",
"Atlantic Time(GMT -4:00) ",
"Newfoundland(GMT -3:30) ",
"Brazil, Buenos Aires, Georgetown(GMT -3:00) ",
"Mid-Atlantic(GMT -2:00) ",
"Azores, Cape Verde Islands(GMT -1:00) ",
"Western Europe Time, London, Lisbon, Casablanca(GMT) ",
"Brussels, Copenhagen, Madrid, Paris(GMT +1:00) ",
"Kaliningrad, South Africa(GMT +2:00) ",
"Baghdad,Riyadh,Moscow,St.Petersburg(GMT+3:00) ",
"Tehran(GMT +3:30) ",
"Abu Dhabi, Muscat, Baku, Tbilisi(GMT +4:00) ",
"Kabul(GMT +4:30) ",
"Ekaterinburg, Islamabad, Karachi, Tashkent(GMT +5:00) ",
"Bombay, Calcutta, Madras, New Delhi(GMT +5:30) ",
"Kathmandu(GMT +5:45) ",
"Almaty, Dhaka, Colombo(GMT +6:00) ",
"Bangkok, Hanoi, Jakarta(GMT +7:00) ",
"Beijing, Perth, Singapore, Hong Kong(GMT +8:00) ",
"Tokyo, Seoul, Osaka, Sapporo, Yakutsk(GMT +9:00) ",
"Adelaide, Darwin(GMT +9:30) ",
"Eastern Australia, Guam, Vladivostok(GMT +10:00) ",
"Magadan, Solomon Islands, New Caledonia(GMT +11:00) ",
"Auckland, Wellington, Fiji, Kamchatka(GMT +12:00) "]
''';
}
