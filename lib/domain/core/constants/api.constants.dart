abstract class ApiConstant {
  String get baseAPI;
  String get baseAPISuper;
  String get baseURL;
  String get baseDomain;
}

class ApiConstantsProduction implements ApiConstant {
  factory ApiConstantsProduction() => ApiConstantsProduction._();
  ApiConstantsProduction._();

  @override
  String get baseAPI => 'https://api.ichineseaplus.com';
  @override
  String get baseAPISuper => 'https://superbe.ichinesereader.com';
  @override
  String get baseURL => 'https://ichineseaplus.com';
  @override
  String get baseDomain => 'ichineseaplus.com';
}

class ApiConstantMigration implements ApiConstant {
  factory ApiConstantMigration() => ApiConstantMigration._();
  ApiConstantMigration._();

  /**
   * 这是测试环境的 api信息
   * const DOMAIN_MIGRATION = "https://test.ichineseaplus.com";
      const DOMAIN_PRODUCT = "https://www.ichineseaplus.com";
      const API_DOMAIN_MIGRATION = "https://testapi.ichineseaplus.com";
   */
  @override
  String get baseAPI => 'https://testapi.ichineseaplus.com';
  @override
  String get baseAPISuper => 'https://test.ichineseaplus.com';
  @override
  String get baseURL => 'https://test.ichineseaplus.com';
  @override
  String get baseDomain => 'test.ichineseaplus.com';
}
