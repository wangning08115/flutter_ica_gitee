abstract class StorageConstants {
  /// user info profile key
  static const String user = 'user';

  /// local storage token name
  static const String tokenAuthorization = 'AuthToken';
}
