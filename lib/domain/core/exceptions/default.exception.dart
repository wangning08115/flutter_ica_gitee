import 'package:get/get.dart';
import 'package:ichineseaplus/generated/locales.g.dart';
import 'package:logger/logger.dart';

class DefaultException implements Exception {
  final String? message;
  final StackTrace? stackTrace;
  DefaultException({
    this.message,
    this.stackTrace,
  }) {
    Logger().e(stackTrace);
  }

  @override

  String toString() => message ?? LocaleKeys.exceptions_defaultMessage.tr;
}
