import 'package:ichineseaplus/domain/core/abstractions/response.model.dart';
import 'package:ichineseaplus/domain/core/exceptions/base_server_error.exception.dart';
import 'package:logger/logger.dart';

enum BaseResponseErrorEnum {
  // base error
  created(201, 'Created'),
  noContent(204, 'No Content'),
  unauthorized(401, 'Unauthorized'),
  forbidden(403, 'Forbidden'),
  notFound(404, 'Not Found'),

  undefined(-1, 'Undefined Error'),
  ;

  final int statusCode;
  final String reason;
  const BaseResponseErrorEnum(this.statusCode, this.reason);

  static void checkBaseError<T>(Response<T> response) {
    for (var element in values) {
      if (element.statusCode == response.statusCode) {
        Logger().v("statusCode is :${element.statusCode}");
        throw BaseServerErrorException(responseErrorEnum: element);
      }
    }
  }
}
