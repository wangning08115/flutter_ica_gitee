import 'package:ichineseaplus/domain/core/exceptions/response_error.enum.dart';
import 'package:logger/logger.dart';

class BaseServerErrorException implements Exception {
  final String message;

  BaseServerErrorException({
    required BaseResponseErrorEnum responseErrorEnum,
  }) : message = responseErrorEnum.reason{
    Logger().w(message);
  }

  @override
  String toString() {
    return message;
  }
}
