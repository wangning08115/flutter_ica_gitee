import 'package:get/get.dart';
import 'package:ichineseaplus/generated/locales.g.dart';
import 'package:logger/logger.dart';

class UserNotFoundException implements Exception {
  final String? message;
  UserNotFoundException({
    this.message,
  }) {
    Logger().w(message);
  }

  @override
  String toString() => message ?? LocaleKeys.exceptions_userNotFoundMessage.tr;}
