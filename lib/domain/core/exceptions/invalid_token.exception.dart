import 'package:get/get.dart';
import 'package:ichineseaplus/generated/locales.g.dart';
import 'package:logger/logger.dart';

class InvalidTokenException implements Exception {
  final String? message;
  InvalidTokenException({
    this.message,
  }) {
    Logger().w(message);
  }

  @override
  String toString() => message ?? LocaleKeys.exceptions_invalidTokenMessage.tr;
}
