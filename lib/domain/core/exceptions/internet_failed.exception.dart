import 'package:get/get.dart';
import 'package:ichineseaplus/generated/locales.g.dart';
import 'package:logger/logger.dart';

class InternetFailedException implements Exception {
  final String? message;

  InternetFailedException({
    this.message,
  }) {
    Logger().w(message);
  }

  @override
  String toString() => message ?? LocaleKeys.exceptions_internetMessage.tr;
}
