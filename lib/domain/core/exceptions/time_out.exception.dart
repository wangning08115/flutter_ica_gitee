import 'package:get/get.dart';
import 'package:ichineseaplus/generated/locales.g.dart';
import 'package:logger/logger.dart';

class TimeOutException implements Exception {
  final String? message;
  TimeOutException({
    this.message,
  }) {
    Logger().w(message);
  }

  @override

  String toString() => message ?? LocaleKeys.exceptions_timeOutMessage.tr;
}
