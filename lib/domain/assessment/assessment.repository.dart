import 'package:ichineseaplus/infrastructure/dal/services/assessment/assessment_service.dart';
import 'package:ichineseaplus/presentation/assessments/assessment_query_model.dart';

class AssessmentRepository {
  final AssessmentService _assessmentService;

  AssessmentRepository({
    required AssessmentService assessmentService,
  }) : _assessmentService = assessmentService;

  Future<dynamic> getAssessment(AssessmentQueryDto dto) async {
    try {
      final response = await _assessmentService.getMineAssessments(dto);
      return response;
    } catch (err) {
      rethrow;
    }
  }
}
