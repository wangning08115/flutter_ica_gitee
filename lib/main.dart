import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:ichineseaplus/generated/locales.g.dart';
import 'package:ichineseaplus/infrastructure/navigation/app_route_observer.dart';
import 'package:ichineseaplus/infrastructure/theme/app.theme.dart';
import 'package:ichineseaplus/initializer.dart';

import 'infrastructure/navigation/navigation.dart';
import 'infrastructure/navigation/routes.dart';

void main() async {
  await Initializer.init();
  var initialRoute = await Routes.initialRoute;
  runApp(Main(initialRoute));
}

class Main extends StatelessWidget {
  final String initialRoute;

  const Main(this.initialRoute, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appRouteObserver = AppRouteObserver();

    return GetMaterialApp(
      initialRoute: initialRoute,
      debugShowCheckedModeBanner: false,
      getPages: Nav.routes,
      translationsKeys: AppTranslation.translations,
      locale: const Locale('en','US'),
      theme: AppTheme.lightTheme,
      navigatorObservers: [appRouteObserver],
      darkTheme: AppTheme.darkTheme,
    );
  }
}
