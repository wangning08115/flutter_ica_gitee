// ignore_for_file: constant_identifier_names

class Routes {
  static Future<String> get initialRoute async {
    return ICA;
  }

  static const ASSIGNMENT = '/assignment';
  static const HOME = '/';
  static const ICA = '/ica';
  static const LOGIN = '/login';
  static const RECORDS = '/records';
  static const STUDENT = '/student';
  static const PROFILE = '/profile';
  static const STUDYRECORDS = '/studyrecords';
  static const ASSESSMENTS = '/assessments';
}
