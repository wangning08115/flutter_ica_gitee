import 'package:flutter/cupertino.dart';
import 'package:logger/logger.dart';

class AppRouteObserver extends NavigatorObserver {
  @override
  void didPop(Route route, Route? previousRoute) {
    Logger().v(
      "Route from ${previousRoute?.settings.name}",
      "to ${route.settings.name} "
    );
  }

  @override
  void didPush(Route route, Route? previousRoute) {
    Logger().v(
      "Route from ${previousRoute?.settings.name}",
      "to ${route.settings.name} "
    );
  }

  @override
  void didRemove(Route route, Route? previousRoute) {
    Logger().v(
      "Route from ${previousRoute?.settings.name}",
      "to ${route.settings.name} "
    );
  }

  @override
  void didReplace({Route? newRoute, Route? oldRoute}) {
    Logger().v(
      "Route from ${oldRoute?.settings.name}",
      "to ${newRoute?.settings.name} "
    );
  }

  @override
  void didStartUserGesture(Route route, Route? previousRoute) {
    Logger().v(
      "Route from ${previousRoute?.settings.name}",
      "to ${route.settings.name} "
    );
  }
}
