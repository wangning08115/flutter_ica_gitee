import 'package:get/get.dart';

import '../../presentation/screens.dart';
import 'bindings/controllers/controllers_bindings.dart';
import 'routes.dart';

class Nav {
  static List<GetPage> routes = [
    GetPage(
      name: Routes.HOME,
      page: () => const HomeScreen(),
      binding: HomeControllerBinding(),
    ),
    GetPage(
      name: Routes.ICA,
      page: () => const IcaScreen(),
      binding: IcaControllerBinding(),
      children: [
        GetPage(
          name: Routes.LOGIN,
          page: () => const LoginScreen(),
          binding: LoginControllerBinding(),
        ),
        GetPage(
          name: Routes.STUDENT,
          participatesInRootNavigator: true,
          page: () => const StudentScreen(),
          binding: StudentControllerBinding(),
          children: [
            GetPage(
              name: Routes.ASSIGNMENT,
              page: () => const AssignmentScreen(),
              binding: AssignmentControllerBinding(),
            ),
            // GetPage(
            //   name: Routes.RECORDS,
            //   page: () => RecordsScreen(),
            //   binding: RecordsControllerBinding(),
            // ),
            // GetPage(
            //   name: Routes.PROFILE,
            //   page: () => ProfileScreen(),
            //   binding: ProfileControllerBinding(),
            // ),
          ],
        ),
      ],
    ),
    GetPage(
      name: Routes.STUDYRECORDS,
      page: () => const StudyrecordsScreen(),
      binding: StudyrecordsControllerBinding(),
    ),
    GetPage(
      name: Routes.ASSESSMENTS,
      page: () => const AssessmentsScreen(),
      binding: AssessmentsControllerBinding(),
    ),
  ];
}
