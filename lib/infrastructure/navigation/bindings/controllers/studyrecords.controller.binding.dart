import 'package:get/get.dart';

import '../../../../presentation/studyrecords/controllers/studyrecords.controller.dart';

class StudyrecordsControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<StudyrecordsController>(
      () => StudyrecordsController(),
    );
  }
}
