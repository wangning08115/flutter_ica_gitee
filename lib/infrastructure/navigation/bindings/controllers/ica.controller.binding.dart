import 'package:get/get.dart';

import '../../../../presentation/ica/controllers/ica.controller.dart';

class IcaControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(IcaController());
  }
}
