export 'package:ichineseaplus/infrastructure/navigation/bindings/controllers/assessments.controller.binding.dart';
export 'package:ichineseaplus/infrastructure/navigation/bindings/controllers/assignment.controller.binding.dart';
export 'package:ichineseaplus/infrastructure/navigation/bindings/controllers/home.controller.binding.dart'; 
export 'package:ichineseaplus/infrastructure/navigation/bindings/controllers/ica.controller.binding.dart'; 
export 'package:ichineseaplus/infrastructure/navigation/bindings/controllers/login.controller.binding.dart'; 
export 'package:ichineseaplus/infrastructure/navigation/bindings/controllers/profile.controller.binding.dart'; 
export 'package:ichineseaplus/infrastructure/navigation/bindings/controllers/records.controller.binding.dart'; 
export 'package:ichineseaplus/infrastructure/navigation/bindings/controllers/student.controller.binding.dart'; 

export 'package:ichineseaplus/infrastructure/navigation/bindings/controllers/studyrecords.controller.binding.dart'; 