import 'package:get/get.dart';
import 'package:ichineseaplus/infrastructure/dal/connect.dart';
import 'package:ichineseaplus/infrastructure/dal/services/assessment/assessment.service.dart';

import '../../../../presentation/ica/student/assignment/controllers/assignment.controller.dart';

class AssignmentControllerBinding extends Bindings {
  @override
  void dependencies() {
    final getConnect = Get.find<GetConnect>();
    final connect = Connect(connect: getConnect);
    Get.lazyPut<AssignmentController>(
      () => AssignmentController(AssessmentService(connect)),
    );
  }
}
