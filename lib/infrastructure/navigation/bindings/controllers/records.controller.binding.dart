import 'package:get/get.dart';

import '../../../../presentation/ica/student/records/controllers/records.controller.dart';

class RecordsControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RecordsController>(
      () => RecordsController(),
    );
  }
}
