import 'package:get/get.dart';

import '../../../../presentation/assessments/controllers/assessments.controller.dart';

class AssessmentsControllerBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AssessmentsController>(
      () => AssessmentsController(),
    );
  }
}
