import 'package:get/get.dart';
import 'package:ichineseaplus/domain/assessment/assessment.repository.dart';
import 'package:ichineseaplus/infrastructure/dal/connect.dart';
import 'package:ichineseaplus/infrastructure/dal/services/assessment/assessment_service.dart';

class AssessmentRepositoryBinding {
  late AssessmentRepository _assessmentRepository;
  AssessmentRepository get repository => _assessmentRepository;

  AssessmentRepositoryBinding() {
    final getConnect = Get.find<GetConnect>();
    final connect = Connect(connect: getConnect);
    final assessmentService = AssessmentService(connect);
    _assessmentRepository = AssessmentRepository(
      assessmentService: assessmentService,
    );
  }
}
