// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserResponse _$UserResponseFromJson(Map<String, dynamic> json) => UserResponse(
      status: json['status'] as int,
      message: json['message'] as String,
      result: Result.fromJson(json['result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserResponseToJson(UserResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'result': instance.result,
    };

Result _$ResultFromJson(Map<String, dynamic> json) => Result(
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      zipCode: json['zipCode'],
      phone: json['phone'],
      customerId: json['customerId'],
      userName: json['userName'] as String,
      email: json['email'],
      rightList: (json['rightList'] as List<dynamic>)
          .map((e) => RightList.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ResultToJson(Result instance) => <String, dynamic>{
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'zipCode': instance.zipCode,
      'phone': instance.phone,
      'customerId': instance.customerId,
      'userName': instance.userName,
      'email': instance.email,
      'rightList': instance.rightList,
    };

RightList _$RightListFromJson(Map<String, dynamic> json) => RightList(
      scope: json['scope'] as String,
      available: json['available'] as bool,
      type: json['type'] as String,
    );

Map<String, dynamic> _$RightListToJson(RightList instance) => <String, dynamic>{
      'scope': instance.scope,
      'available': instance.available,
      'type': instance.type,
    };
