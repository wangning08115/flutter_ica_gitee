import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:ichineseaplus/domain/core/constants/storage.constants.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user.response.g.dart';

@JsonSerializable()
class UserResponse {
  final int status;
  final String message;
  final Result result;
  UserResponse({
    required this.status,
    required this.message,
    required this.result,
  });

  factory UserResponse.fromJson(Map<String, dynamic> json) =>
      _$UserResponseFromJson(json);
  Map<String, dynamic> toJson() => _$UserResponseToJson(this);

  static UserResponse? fromStorage() {
    final storage = Get.find<GetStorage>();
    final user = storage.read<UserResponse>(StorageConstants.user);
    return user;
  }

  Future<void> save() async {
    final storage = Get.find<GetStorage>();
    await storage.write(StorageConstants.user, toJson());
  }
}

@JsonSerializable()
class Result {
  final String firstName;
  final String lastName;
  final dynamic zipCode;
  final dynamic phone;
  final dynamic customerId;
  final String userName;
  final dynamic email;
  final List<RightList> rightList;
  Result({
    required this.firstName,
    required this.lastName,
    this.zipCode,
    this.phone,
    this.customerId,
    required this.userName,
    this.email,
    required this.rightList,
  });

  factory Result.fromJson(Map<String, dynamic> json) => _$ResultFromJson(json);
  Map<String, dynamic> toJson() => _$ResultToJson(this);
}

@JsonSerializable()
class RightList {
  final String scope;
  final bool available;
  final String type;
  RightList({
    required this.scope,
    required this.available,
    required this.type,
  });

  factory RightList.fromJson(Map<String, dynamic> json) =>
      _$RightListFromJson(json);
  Map<String, dynamic> toJson() => _$RightListToJson(this);
}
