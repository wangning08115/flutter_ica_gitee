import 'package:ichineseaplus/domain/core/abstractions/http_connect.interface.dart';
import 'package:ichineseaplus/domain/core/exceptions/default.exception.dart';
import 'package:ichineseaplus/infrastructure/dal/services/user/dto/user.response.dart';
import 'package:logger/logger.dart';

import '../../../../domain/core/abstractions/response.model.dart';

class UserService {
  final IHttpConnect _connect;

  const UserService(IHttpConnect connect) : _connect = connect;

  Future<UserResponse> getUserProfile() async {
    Response<UserResponse> response = await _connect.get(
      '/profile',
      decoder: (value) {
        Logger().v(value);
        return UserResponse.fromJson(value);
      },
    );

    if (response.success) {
      return response.payload!;
    } else {
      throw DefaultException(message: response.payload!.message);
    }
  }
}
