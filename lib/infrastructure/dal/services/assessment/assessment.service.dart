import 'package:ichineseaplus/domain/core/abstractions/http_connect.interface.dart';
import 'package:ichineseaplus/domain/core/exceptions/default.exception.dart';
import 'package:ichineseaplus/domain/core/exceptions/response_error.enum.dart';
import 'package:ichineseaplus/infrastructure/dal/services/assessment/dto/assessment_mine_list.body.dart';
import 'package:ichineseaplus/infrastructure/dal/services/assessment/dto/assessment_mine_list.response.dart';
import 'package:logger/logger.dart';

class AssessmentService {
  final IHttpConnect _connect;

  const AssessmentService(IHttpConnect connect) : _connect = connect;

  Future<Result> getAssessmentMineList(AssessmentMineListBody body) async {
    var response = await _connect.put(
      '/assessment/mine',
      body.toJson(),
      decoder: (data) {
        return AssessmentMineListResponse.fromJson(data);
      },
    );
    Logger().v(response);
    if (!response.success) {
      BaseResponseErrorEnum.checkBaseError(response);
    }

    switch (response.payload!.status) {
      case 1000:
        return response.payload!.result;
      default:
        throw DefaultException(message: response.payload!.message);
    }
  }
}
