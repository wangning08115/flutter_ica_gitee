import 'package:json_annotation/json_annotation.dart';

part 'assessment_mine_list.response.g.dart';

@JsonSerializable()
class AssessmentMineListResponse {
  final int status;
  final String message;
  final Result result;

  AssessmentMineListResponse({
    required this.status,
    required this.message,
    required this.result,
  });

  factory AssessmentMineListResponse.fromJson(Map<String, dynamic> json) =>
      _$AssessmentMineListResponseFromJson(json);
  Map<String, dynamic> toJson() => _$AssessmentMineListResponseToJson(this);
}

@JsonSerializable()
class Result {
  final int limit;
  final int count;
  final int page;
  final List<QuestionListItem> list;

  Result({
    required this.limit,
    required this.count,
    required this.page,
    required this.list,
  });

  factory Result.fromJson(Map<String, dynamic> json) => _$ResultFromJson(json);
  Map<String, dynamic> toJson() => _$ResultToJson(this);
}

@JsonSerializable()
class QuestionListItem {
  final int? submittedTime;
  final String kind;
  final String audioType;
  final int progressRate;
  final String textType;
  final String className;
  final String type;
  final int questionNumber;
  final String mode;
  final String? instruction;
  final String name;
  final int startTime;
  final QuestionGroup? questionGroup;
  final String id;
  final String? questionRecordId;
  final int answerNumber;
  final int endTime;
  final int? correctRate;
  final String status;

  QuestionListItem({
    required this.submittedTime,
    required this.kind,
    required this.audioType,
    required this.progressRate,
    required this.textType,
    required this.className,
    required this.type,
    required this.questionNumber,
    required this.mode,
    required this.instruction,
    required this.name,
    required this.startTime,
    required this.questionGroup,
    required this.id,
    required this.questionRecordId,
    required this.answerNumber,
    required this.endTime,
    required this.correctRate,
    required this.status,
  });

  factory QuestionListItem.fromJson(Map<String, dynamic> json) =>
      _$QuestionListItemFromJson(json);
  Map<String, dynamic> toJson() => _$QuestionListItemToJson(this);
}

@JsonSerializable()
class QuestionGroup {
  dynamic imageLink;
  final String id;
  final String title;
  dynamic bookId;

  QuestionGroup({
    required this.imageLink,
    required this.id,
    required this.title,
    required this.bookId,
  });

  factory QuestionGroup.fromJson(Map<String, dynamic> json) =>
      _$QuestionGroupFromJson(json);
  Map<String, dynamic> toJson() => _$QuestionGroupToJson(this);
}
