import 'package:json_annotation/json_annotation.dart';

part 'assessment_mine_list.body.g.dart';

@JsonSerializable()
class AssessmentMineListBody {
  final int limit;
  final List assessmentViewForm;
  final int page;
  final String sortField;

  AssessmentMineListBody({
    required this.limit,
    required this.assessmentViewForm,
    required this.page,
    required this.sortField,
  });

  factory AssessmentMineListBody.fromJson(Map<String, dynamic> json) =>
      _$AssessmentMineListBodyFromJson(json);
  Map<String, dynamic> toJson() => _$AssessmentMineListBodyToJson(this);
}
