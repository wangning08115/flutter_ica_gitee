// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'assessment_mine_list.body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AssessmentMineListBody _$AssessmentMineListBodyFromJson(
        Map<String, dynamic> json) =>
    AssessmentMineListBody(
      limit: json['limit'] as int,
      assessmentViewForm: json['assessmentViewForm'] as List<dynamic>,
      page: json['page'] as int,
      sortField: json['sortField'] as String,
    );

Map<String, dynamic> _$AssessmentMineListBodyToJson(
        AssessmentMineListBody instance) =>
    <String, dynamic>{
      'limit': instance.limit,
      'assessmentViewForm': instance.assessmentViewForm,
      'page': instance.page,
      'sortField': instance.sortField,
    };
