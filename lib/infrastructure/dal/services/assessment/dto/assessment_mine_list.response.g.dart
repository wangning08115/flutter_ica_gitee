// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'assessment_mine_list.response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AssessmentMineListResponse _$AssessmentMineListResponseFromJson(
        Map<String, dynamic> json) =>
    AssessmentMineListResponse(
      status: json['status'] as int,
      message: json['message'] as String,
      result: Result.fromJson(json['result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AssessmentMineListResponseToJson(
        AssessmentMineListResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'result': instance.result,
    };

Result _$ResultFromJson(Map<String, dynamic> json) => Result(
      limit: json['limit'] as int,
      count: json['count'] as int,
      page: json['page'] as int,
      list: (json['list'] as List<dynamic>)
          .map((e) => QuestionListItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ResultToJson(Result instance) => <String, dynamic>{
      'limit': instance.limit,
      'count': instance.count,
      'page': instance.page,
      'list': instance.list,
    };

QuestionListItem _$QuestionListItemFromJson(Map<String, dynamic> json) =>
    QuestionListItem(
      submittedTime: json['submittedTime'] as int?,
      kind: json['kind'] as String,
      audioType: json['audioType'] as String,
      progressRate: json['progressRate'] as int,
      textType: json['textType'] as String,
      className: json['className'] as String,
      type: json['type'] as String,
      questionNumber: json['questionNumber'] as int,
      mode: json['mode'] as String,
      instruction: json['instruction'] as String?,
      name: json['name'] as String,
      startTime: json['startTime'] as int,
      questionGroup: json['questionGroup'] == null
          ? null
          : QuestionGroup.fromJson(
              json['questionGroup'] as Map<String, dynamic>),
      id: json['id'] as String,
      questionRecordId: json['questionRecordId'] as String?,
      answerNumber: json['answerNumber'] as int,
      endTime: json['endTime'] as int,
      correctRate: json['correctRate'] as int?,
      status: json['status'] as String,
    );

Map<String, dynamic> _$QuestionListItemToJson(QuestionListItem instance) =>
    <String, dynamic>{
      'submittedTime': instance.submittedTime,
      'kind': instance.kind,
      'audioType': instance.audioType,
      'progressRate': instance.progressRate,
      'textType': instance.textType,
      'className': instance.className,
      'type': instance.type,
      'questionNumber': instance.questionNumber,
      'mode': instance.mode,
      'instruction': instance.instruction,
      'name': instance.name,
      'startTime': instance.startTime,
      'questionGroup': instance.questionGroup,
      'id': instance.id,
      'questionRecordId': instance.questionRecordId,
      'answerNumber': instance.answerNumber,
      'endTime': instance.endTime,
      'correctRate': instance.correctRate,
      'status': instance.status,
    };

QuestionGroup _$QuestionGroupFromJson(Map<String, dynamic> json) =>
    QuestionGroup(
      imageLink: json['imageLink'],
      id: json['id'] as String,
      title: json['title'] as String,
      bookId: json['bookId'],
    );

Map<String, dynamic> _$QuestionGroupToJson(QuestionGroup instance) =>
    <String, dynamic>{
      'imageLink': instance.imageLink,
      'id': instance.id,
      'title': instance.title,
      'bookId': instance.bookId,
    };
