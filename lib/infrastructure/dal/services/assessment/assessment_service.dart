import 'package:ichineseaplus/domain/core/abstractions/http_connect.interface.dart';
import 'package:ichineseaplus/domain/core/exceptions/default.exception.dart';
import 'package:ichineseaplus/presentation/assessments/assessment_query_model.dart';


class AssessmentService {
  final IHttpConnect _connect;
  const AssessmentService(IHttpConnect connect) : _connect = connect;

  Future<dynamic> getMineAssessments(AssessmentQueryDto dto) async {
    var response = await _connect.put(
      '/assessment/mine?&limit=0&page=0&sortField=-submittedTime',
      dto.toJson(),
    );
    if (response.success) {
      return response.payload;
    } else {
      throw DefaultException(message: response.payload!.message);
    }
  }
}
