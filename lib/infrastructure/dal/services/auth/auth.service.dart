import 'package:ichineseaplus/domain/core/abstractions/http_connect.interface.dart';
import 'package:ichineseaplus/domain/core/exceptions/default.exception.dart';
import 'package:ichineseaplus/domain/core/exceptions/internet_failed.exception.dart';
import 'package:ichineseaplus/domain/core/exceptions/user_not_found.exception.dart';
import 'package:logger/logger.dart';

import 'dto/authenticate_user.body.dart';
import 'dto/authenticate_user.response.dart';

class AuthService {
  final IHttpConnect _connect;
  const AuthService(IHttpConnect connect) : _connect = connect;

  Future<AuthenticateUserResponse> authenticateUser(
    AuthenticateUserBody body,
  ) async {

    final response = await _connect.post(
      '/login',
      body.toJson(),
      decoder: (value) {
        Logger().v(value);
        return AuthenticateUserResponse.fromJson(value);
      },
    );

    if (response.success) {
      AuthenticateUserResponse authenticateUserResponse = response.payload!;
      switch (authenticateUserResponse.status) {
        case 1000:
          return response.payload!;
        case 0:
          throw UserNotFoundException(
            message: response.payload!.message,
          );
        default:
          throw DefaultException(
              message: [response.payload!.message, response.statusCode].join());
      }
    } else {
      throw InternetFailedException(message: "${response.statusCode}");
    }
  }
}
