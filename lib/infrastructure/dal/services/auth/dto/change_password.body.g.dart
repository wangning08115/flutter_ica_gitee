// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_password.body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChangePasswordBody _$ChangePasswordBodyFromJson(Map<String, dynamic> json) =>
    ChangePasswordBody(
      newPassword: json['newPassword'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$ChangePasswordBodyToJson(ChangePasswordBody instance) =>
    <String, dynamic>{
      'newPassword': instance.newPassword,
      'password': instance.password,
    };
