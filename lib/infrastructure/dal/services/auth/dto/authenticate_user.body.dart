import 'package:json_annotation/json_annotation.dart';

part 'authenticate_user.body.g.dart';

@JsonSerializable()
class AuthenticateUserBody {
  final String loginName;
  final String password;

  AuthenticateUserBody({
    required this.loginName,
    required this.password,
  });

  factory AuthenticateUserBody.fromJson(Map<String, dynamic> json) =>
      _$AuthenticateUserBodyFromJson(json);
  Map<String, dynamic> toJson() => _$AuthenticateUserBodyToJson(this);
}
