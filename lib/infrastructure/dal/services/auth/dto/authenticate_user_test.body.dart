import 'package:json_annotation/json_annotation.dart';

part 'authenticate_user_test.body.g.dart';

@JsonSerializable()
class AuthenticateUserTestBody {
  final String loginName;
  final String password;

  AuthenticateUserTestBody({
    required this.loginName,
    required this.password,
  });

  factory AuthenticateUserTestBody.fromJson(Map<String, dynamic> json) =>
      _$AuthenticateUserTestBodyFromJson(json);
  Map<String, dynamic> toJson() => _$AuthenticateUserTestBodyToJson(this);
}
