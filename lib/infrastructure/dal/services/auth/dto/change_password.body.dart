import 'package:json_annotation/json_annotation.dart';

part 'change_password.body.g.dart';

@JsonSerializable()
class ChangePasswordBody {
  final String newPassword;
  final String password;
  ChangePasswordBody({
    required this.newPassword,
    required this.password,
  });

  factory ChangePasswordBody.fromJson(Map<String, dynamic> json) =>
      _$ChangePasswordBodyFromJson(json);
  Map<String, dynamic> toJson() => _$ChangePasswordBodyToJson(this);
}
