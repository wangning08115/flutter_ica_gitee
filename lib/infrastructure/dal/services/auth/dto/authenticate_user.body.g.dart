// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authenticate_user.body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthenticateUserBody _$AuthenticateUserBodyFromJson(
        Map<String, dynamic> json) =>
    AuthenticateUserBody(
      loginName: json['loginName'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$AuthenticateUserBodyToJson(
        AuthenticateUserBody instance) =>
    <String, dynamic>{
      'loginName': instance.loginName,
      'password': instance.password,
    };
