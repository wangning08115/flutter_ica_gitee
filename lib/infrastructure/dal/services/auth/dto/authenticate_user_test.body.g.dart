// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'authenticate_user_test.body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AuthenticateUserTestBody _$AuthenticateUserTestBodyFromJson(
        Map<String, dynamic> json) =>
    AuthenticateUserTestBody(
      loginName: json['loginName'] as String,
      password: json['password'] as String,
    );

Map<String, dynamic> _$AuthenticateUserTestBodyToJson(
        AuthenticateUserTestBody instance) =>
    <String, dynamic>{
      'loginName': instance.loginName,
      'password': instance.password,
    };
