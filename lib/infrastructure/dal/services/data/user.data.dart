import 'package:ichineseaplus/infrastructure/dal/services/user/dto/user.response.dart'
    show RightList;
import 'package:json_annotation/json_annotation.dart';

part 'user.data.g.dart';

@JsonSerializable()
class UserData {
  final String firstName;
  final String lastName;
  final dynamic zipCode;
  final dynamic phone;
  final dynamic customerId;
  final String userName;
  final dynamic email;
  final List<RightList> rightList;

  UserData({
    required this.firstName,
    required this.lastName,
    this.zipCode,
    this.phone,
    this.customerId,
    required this.userName,
    this.email,
    required this.rightList,
  });

  factory UserData.fromJson(Map<String, dynamic> json) =>
      _$UserDataFromJson(json);
  Map<String, dynamic> toJson() => _$UserDataToJson(this);
}
