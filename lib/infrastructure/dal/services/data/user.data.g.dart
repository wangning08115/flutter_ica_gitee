// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserData _$UserDataFromJson(Map<String, dynamic> json) => UserData(
      firstName: json['firstName'] as String,
      lastName: json['lastName'] as String,
      zipCode: json['zipCode'],
      phone: json['phone'],
      customerId: json['customerId'],
      userName: json['userName'] as String,
      email: json['email'],
      rightList: (json['rightList'] as List<dynamic>)
          .map((e) => RightList.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$UserDataToJson(UserData instance) => <String, dynamic>{
      'firstName': instance.firstName,
      'lastName': instance.lastName,
      'zipCode': instance.zipCode,
      'phone': instance.phone,
      'customerId': instance.customerId,
      'userName': instance.userName,
      'email': instance.email,
      'rightList': instance.rightList,
    };
