import 'package:get/get.dart' hide Response;
import 'package:ichineseaplus/domain/core/utils/Log.dart';

import '../../domain/core/abstractions/http_connect.interface.dart';
import '../../domain/core/abstractions/response.model.dart';

class Connect implements IHttpConnect {
  final GetConnect _connect;

  Connect({required GetConnect connect}) : _connect = connect;

  @override
  Future<Response<T>> get<T>(
    String url, {
    required T Function(dynamic)? decoder,
  }) async {
    String timeFlag = Log.timeFlag();
    Log.debug('$timeFlag Http Get Start Url=$url', showFullLog: true);
    final response = await _connect.get(url, decoder: decoder);
    Log.debug(
        '$timeFlag Http Get End Url=$url StatusCode=${response.statusCode} StatusText=${response.statusText} Body=${response.bodyString}',
        showFullLog: true);
    final obj = Response(
      statusCode: response.statusCode!,
      payload: response.body,
    );
    return obj;
  }

  @override
  Future<Response<T>> post<T>(
    String url,
    Map<String, dynamic> body, {
    T Function(dynamic)? decoder,
  }) async {
    String timeFlag = Log.timeFlag();
    Log.debug(
        '$timeFlag Http Post Start Url=${_connect.baseUrl}$url Body=${body.toString()}',
        showFullLog: true);
    final response = await _connect.post(url, body, decoder: decoder);
    Log.debug(
        '$timeFlag Http Post End Url=${_connect.baseUrl}$url StatusCode=${response.statusCode} StatusText=${response.statusText} Body=${response.bodyString}',
        showFullLog: true);
    final obj = Response(
      statusCode: response.statusCode!,
      payload: response.body,
    );

    return obj;
  }

  @override
  Future<Response<T>> patch<T>(
    String url,
    Map<String, dynamic> body, {
    T Function(dynamic)? decoder,
  }) async {
    String timeFlag = Log.timeFlag();
    Log.debug(
        '$timeFlag Http Patch Start Url=${_connect.baseUrl}$url Body=${body.toString()}',
        showFullLog: true);
    final response = await _connect.patch(url, body, decoder: decoder);
    Log.debug(
        '$timeFlag Http Patch End Url=${_connect.baseUrl}$url StatusCode=${response.statusCode} StatusText=${response.statusText} Body=${response.bodyString}',
        showFullLog: true);
    final obj = Response(
      statusCode: response.statusCode!,
      payload: response.body,
    );
    return obj;
  }

  @override
  Future<Response<T>> delete<T>(
    String url, {
    T Function(dynamic)? decoder,
  }) async {
    String timeFlag = Log.timeFlag();
    Log.debug('$timeFlag Http Delete Start Url=${_connect.baseUrl}$url',
        showFullLog: true);
    final response = await _connect.delete(url, decoder: decoder);
    Log.debug(
        '$timeFlag Http Delete End Url=${_connect.baseUrl}$url StatusCode=${response.statusCode} StatusText=${response.statusText} Body=${response.bodyString}',
        showFullLog: true);
    final obj = Response(
      statusCode: response.statusCode!,
      payload: response.body,
    );
    return obj;
  }

  @override
  Future<Response<T>> put<T>(
    String url,
    Map<String, dynamic> body, {
    T Function(dynamic)? decoder,
  }) async {
    String timeFlag = Log.timeFlag();
    Log.debug(
        '$timeFlag Http Put Start Url=${_connect.baseUrl}$url Body=${body.toString()}',
        showFullLog: true);
    final response = await _connect.put(url, body, decoder: decoder);
    Log.debug(
        '$timeFlag Http Put End Url=${_connect.baseUrl}$url StatusCode=${response.statusCode} StatusText=${response.statusText} Body=${response.bodyString}',
        showFullLog: true);
    final obj = Response(
      statusCode: response.statusCode!,
      payload: response.body,
    );
    return obj;
  }
}
