part of '../app.theme.dart';

AppBarTheme _appBarDarkTheme = const AppBarTheme(
  backgroundColor: Color(0xff444444),
  toolbarTextStyle: TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.bold,
  ),
  titleTextStyle: TextStyle(
    fontSize: 28,
    fontWeight: FontWeight.bold,
    fontFamily: 'Gill Sans',
    color: Color(0xffaaaaaa),
  ),
);

AppBarTheme _appBarLightTheme = const AppBarTheme(
  color: Color(0xff23c6c8),
  toolbarTextStyle: TextStyle(
    fontSize: 28,
    fontWeight: FontWeight.bold,
  ),
  titleTextStyle: TextStyle(
    fontSize: 28,
    fontWeight: FontWeight.bold,
    fontFamily: 'Gill Sans',
  ),
);
