part of '../app.theme.dart';

TextTheme _textDarkTheme = const TextTheme(
  caption: TextStyle(
    color: Color(0xffaaaaaa),
    fontSize: 25,
  ),
  subtitle1: TextStyle(
    color: Color(0xffcccccc),
    fontSize: 22,
    letterSpacing: 1,
  ),
  headline5: TextStyle(
    color: Color(0xff666666),
  ),
);

TextTheme _textLightTheme = const TextTheme(
  caption: TextStyle(
    color: Color(0xff00599b),
    fontSize: 25,
  ),
  subtitle1: TextStyle(
    color: Color(0xffd8ffff),
    fontSize: 22,
    letterSpacing: 1,
  ),
  headline5: TextStyle(
    color: Color(0xff666666),
  ),
);
