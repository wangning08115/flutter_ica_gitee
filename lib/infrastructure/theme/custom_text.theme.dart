import 'package:flutter/material.dart';

// Custom Text Styles Class For Both Dark and Light Theme
class CustomTextTheme {
  static const _textColorLight =
      Color(0xFF666666); // Light Theme Default Text Color
  static const _textColorDark =
      Color(0xFFcccccc); // Dark Theme Default Text Color

  static TextTheme get textThemeLight {
    return _textTheme(textColor: _textColorLight);
  }

  static TextTheme get textThemeDark {
    return _textTheme(textColor: _textColorDark);
  }

  // Private Method For Text Theme so that we can change the vale for Both Dark And Light Theme
  static TextTheme _textTheme({required Color textColor}) {
    const FontWeight light = FontWeight.w300;
    const FontWeight medium = FontWeight.w500;
    const FontWeight regular = FontWeight.w400;

    return TextTheme(
      subtitle1: TextStyle(
        fontSize: 21,
        color: textColor, //这样就会限制，无论在light还是dart下，subtitle1的颜色都不会改变
        fontWeight: medium,
        letterSpacing: 1.8,
      ),
      subtitle2: TextStyle(
        fontSize: 30,
        color:
            textColor, //这样就会跟着两种主题文字颜色，也就是上面定义的_textColorLight  _textColorDark
        fontWeight: medium,
        letterSpacing: 0.0,
      ),
      headline1: TextStyle(
        fontSize: 96,
        color: textColor,
        fontWeight: light,
        letterSpacing: -1.5,
      ),
      headline2: TextStyle(
        color: textColor,
        fontSize: 60,
        fontWeight: light,
        letterSpacing: -0.5,
      ),
      headline3: TextStyle(
        color: textColor,
        fontSize: 48,
        fontWeight: regular,
        letterSpacing: 0.0,
      ),
      headline4: TextStyle(
        color: textColor,
        fontSize: 34,
        fontWeight: regular,
        letterSpacing: 0.25,
      ),
      headline5: const TextStyle(
        color: Color(0xff666666),
        fontSize: 25,
        fontWeight: medium,
        letterSpacing: 2.0,
      ),
      headline6: TextStyle(
        color: textColor,
        fontSize: 20,
        fontWeight: medium,
        letterSpacing: 0.15,
      ),
      bodyText1: TextStyle(
        color: textColor,
        fontSize: 16,
        fontWeight: regular,
        letterSpacing: 0.5,
      ),
      //书本的标题使用的是这个textstyle
      bodyText2: TextStyle(
        color: textColor,
        fontSize: 18,
        fontWeight: regular,
        letterSpacing: 0.25,
      ),
      button: TextStyle(
        color: textColor,
        fontSize: 14,
        fontWeight: medium,
        letterSpacing: 1.25,
      ),
      caption: TextStyle(
        color: textColor,
        fontSize: 25,
        letterSpacing: 1,
      ),
      overline: TextStyle(
        color: textColor,
        fontSize: 10,
        fontWeight: regular,
        letterSpacing: 1.5,
      ),
    );
  }
}
