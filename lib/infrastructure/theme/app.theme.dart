import 'package:flutter/material.dart';

import 'custom_text.theme.dart';

part 'part_of_app/app_bar.theme.dart';
part 'part_of_app/text.theme.dart';
part 'part_of_app/page_transitions.theme.dart';


class AppTheme {
  static final ThemeData darkTheme = ThemeData(
    appBarTheme: _appBarDarkTheme,
    brightness: Brightness.dark,
    cardTheme: const CardTheme(color: Colors.black),
    dividerTheme: const DividerThemeData(color: Color(0xffd8ffff)),
    drawerTheme: const DrawerThemeData(
      backgroundColor: Color(0xff444444),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(
            const Color.fromARGB(255, 90, 153, 99)),
      ),
    ),
    errorColor: const Color(0xFFb00020),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: Color(0xffd8ffff),
      foregroundColor: Color(0xFF00599b),
    ),
    fontFamily: 'Gill San',
    hoverColor: const Color(0xFF6bf9fb),
    iconTheme: const IconThemeData(color: Color(0xFFaaaaaa), size: 30),
    listTileTheme: const ListTileThemeData(),
    pageTransitionsTheme: _pageTransitionsTheme,
    scaffoldBackgroundColor: const Color(0xff0a0e21),
    textTheme: _textDarkTheme.merge(CustomTextTheme.textThemeDark),
  );

  static final ThemeData lightTheme = ThemeData(
    appBarTheme: _appBarLightTheme,
    brightness: Brightness.light,
    cardTheme: const CardTheme(color: Colors.white),
    dividerTheme: const DividerThemeData(color: Color(0xffd8ffff)),
    drawerTheme: const DrawerThemeData(
      backgroundColor: Color(0xFF23c6c8),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
        backgroundColor:
            MaterialStateProperty.all<Color>(const Color(0xff1C84C6)),
      ),
    ),
    errorColor: const Color(0xFFb00020),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: Color(0xFF23c6c8),
      foregroundColor: Color(0xFFd8ffff),
    ),
    fontFamily: 'Gill Sans',
    hoverColor: const Color(0xFF5cb4ff),
    iconTheme: const IconThemeData(color: Color(0xFFd8ffff), size: 30),
    listTileTheme: const ListTileThemeData(),
    pageTransitionsTheme: _pageTransitionsTheme,
    primaryColor: const Color(0xff23c6c8),
    primarySwatch: Colors.blue,
    scaffoldBackgroundColor: const Color(0xFFfefefe),
    snackBarTheme: const SnackBarThemeData(
      //actionTextColor: Color(0xffff0000),
      backgroundColor: Color(0xFF23c6c8),
      //contentTextStyle: TextStyle(fontSize: 16),
      disabledActionTextColor: Color(0xffcccccc),
    ),
    textTheme: _textLightTheme.merge(CustomTextTheme.textThemeLight),
  );
}
