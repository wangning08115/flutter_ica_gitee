import 'package:ichineseaplus/domain/core/constants/api.constants.dart';

enum ApiEnvironments {
  production,
  migration,
}

class ConfigEnvironments {
  static ApiEnvironments get currentApiEnvironments => ApiEnvironments.migration;

  static ApiConstant getApiConstants() {
    switch (currentApiEnvironments) {
      case ApiEnvironments.production:
        return ApiConstantsProduction();
      case ApiEnvironments.migration:
        return ApiConstantMigration();
    }
  }
}
