# ichineseaplus

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## How to Build?

activate get_cli package, and add `get` environment variables

```shell
flutter pub global activate get_cli

# flutter dart
export PATH=$PATH:$HOME/snap/flutter/common/flutter/bin/cache/dart-sdk/bin/
# flutter pub packages such as get
export PATH=$PATH:$HOME/snap/flutter/common/flutter/.pub-cache/bin
```

```shell
# get packages
flutter pub get
# generate locale language file in lib/generated/locales.g.dart 
get generate locales assets/locales
# generate model to/from json by json_serializable
flutter pub run build_runner build --delete-conflicting-outputs
flutter run
```

## Module Help?

- [internalization](./assets/locales/readme.md)
- [core](./lib/domain/core/readme.md)

## get_cli

```shell
// To create a screen
// (Screens have controller, view, and binding)
// Note: you can use any name, ex: `get screen page:login`
// Nota: use this option if the chosen structure was CLEAN (by Arktekko)
get create screen:home 
```
